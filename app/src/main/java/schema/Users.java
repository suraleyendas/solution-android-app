package schema;

import android.content.ContentValues;
import android.provider.BaseColumns;

/**
 * Created by Nelson on 3/02/2017.
 */

public class Users
{
    public static abstract class UsersEntry implements BaseColumns
    {
        public static final String TABLE_NAME ="users";

        public static final String ID = "id";
        public static final String ID_USER = "iduser";
        public static final String NAME = "name";
        public static final String PICTURE = "picture";
        public static final String ROLE = "role";
        public static final String COMPANY = "company";
        public static final String CITY = "city";
        public static final String EMAIL = "correo";
        public static final String CITY_ID = "city_id";
        public static final String COMPANY_ID = "company_id";
        public static final String DOMAIN_ID = "domain_id";
    }

}
