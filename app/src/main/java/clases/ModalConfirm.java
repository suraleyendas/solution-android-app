package clases;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import adparter_class.Award;
import leyendasoy.leyenda.AwardsActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 5/04/2017.
 */

public class ModalConfirm extends DialogFragment
{
    TextView txtNombre , txtCity , txtCompany , txtQuantity , txtAddress;
    Button btnCancel , btnSend;
    static ModalConfirm ModalConfirm;
    AwardsActivity activity;
    Context context;

    String City , Company , Name;
    Award Award;
    int AwardId , Point , Cityid , CompanyId , Quantity ,  UserID ;
    String Address;

    public static ModalConfirm newInstance(AwardsActivity activity , Award award  )
    {

        ModalConfirm = new ModalConfirm();
        ModalConfirm.Award = award;
        ModalConfirm.activity = activity;
        ModalConfirm.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        return ModalConfirm;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View v = inflater.inflate(R.layout.modal_confirm, container, false);

        ModalConfirm.context = v.getContext();
        RelativeLayout popup_root = ( RelativeLayout ) v.findViewById(R.id.popup_root);
        popup_root.getBackground().setAlpha(30);

        txtNombre = (TextView) v.findViewById(R.id.txtNombre);
        txtCity = (TextView) v.findViewById(R.id.txtCity);
        txtCompany = (TextView) v.findViewById(R.id.txtCompany);
        txtQuantity = (TextView) v.findViewById(R.id.txtQuantity);
        txtAddress = (TextView) v.findViewById(R.id.txtAddress);
        btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnSend = (Button) v.findViewById(R.id.btnSend);

        txtCity.setText( ModalConfirm.City );
        txtCompany.setText( ModalConfirm.Company );
        txtCompany.setText( ModalConfirm.Name );

        v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ModalConfirm.dismiss();
            }
        });

        v.findViewById(R.id.btnSend).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ModalConfirm.Address = txtAddress.getText().toString();
                ModalConfirm.Quantity = Integer.parseInt( txtQuantity.getText().toString() );
                try
                {
                    ModalConfirm.UserID = Singleton.getInstance().getUserID();
                    ModalConfirm.Name = Singleton.getInstance().getName();
                    ModalConfirm.City = Singleton.getInstance().getCity();
                    ModalConfirm.Company = Singleton.getInstance().getCompany();
                    ModalConfirm.Point = ModalConfirm.Award.getPoint();
                    ModalConfirm.AwardId = ModalConfirm.Award.getId();
                    ModalConfirm.Cityid = Singleton.getInstance().getCityID();
                    ModalConfirm.CompanyId = Singleton.getInstance().getCompanyID();

                    Boolean error = false;
                    if( txtQuantity.getText().toString().trim().length() <= 0 )
                    {
                        txtQuantity.setError("La cantidad es obligatoria");
                        error = true;
                    }
                    else if( ModalConfirm.Quantity < 0 )
                    {
                        txtQuantity.setError("La cantidad debe ser mayor a 0");
                        error = true;
                    }
                    if( txtAddress.getText().toString().trim().length() <= 0 )
                    {
                        txtAddress.setError("La dirección es obligatoria");
                        error = true;
                    }
                    if( error == false )
                    {
                         int redimido = ModalConfirm.Quantity * Point;
                         int quedan = ModalConfirm.Award.getPointUser() - redimido;
                         if( redimido <= ModalConfirm.Award.getPointUser() )
                         {
                             ModalConfirm.activity.UpdatePoint( quedan );
                             SaveRedemption( ModalConfirm.Award.getId() , ModalConfirm.UserID , Point , ModalConfirm.Name , ModalConfirm.Cityid , ModalConfirm.CompanyId , ModalConfirm.Quantity , ModalConfirm.Address , redimido , quedan );

                         }
                         else
                             {
                             AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                             builder1.setMessage("No tienes puntos suficientes");
                             builder1.setCancelable(true);

                             builder1.setPositiveButton("Cerrar",new DialogInterface.OnClickListener()
                             {
                                 public void onClick(DialogInterface dialog, int id)
                                 {
                                     dialog.cancel();
                                 }
                             });
                             AlertDialog alert11 = builder1.create();
                             alert11.show();
                         }
                    }
                }
                catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }

            }
        });

        return v;
    }
    public void SaveRedemption(int AwardId , int UserId , int Point , String Name , int Cityid , int CompanyId , int Quantity , String Address , final int redimido , final int quedan )
    {
        final MiHelper miHelper = new MiHelper();
        final ProgressDialog progressDialog  = new ProgressDialog(context);;

        if( progressDialog == null )
        {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(context);

        Map<String, String> params = new HashMap<String, String>();
        params.put("AwardId", String.valueOf(AwardId) );
        params.put("UserId",  String.valueOf( UserId ));
        params.put("Point",  String.valueOf( Point ));
        params.put("Name", Name );
        params.put("Cityid",  String.valueOf( Cityid ) );
        params.put("CompanyId", String.valueOf( CompanyId ));
        params.put("Quantity", String.valueOf(Quantity));
        params.put("Address", Address );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "RedemptionAPI/SaveRedemption"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        ModalConfirm.dismiss();
                        ModalResume.newInstance(ModalConfirm.activity, ModalConfirm.Award, redimido, quedan).show(ModalConfirm.activity.getFragmentManager(), null);
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder( context );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {

                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);
    }

}
