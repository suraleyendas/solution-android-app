package clases;

/**
 * Created by Nelson on 28/02/2017.
 */

public class VotersCompany
{
    private String company;
    private int value;
    private String color;

    public VotersCompany(String company, int value, String color)
    {
        this.company = company;
        this.value = value;
        this.color = color;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
