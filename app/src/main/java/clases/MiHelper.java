package clases;

import android.app.DownloadManager;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
public class MiHelper
{
    private String URL = "http://test.multistrategy.co/BigSocialNetwork/Service/api/";
    private String URL_SOCKET = "http://192.168.12.162:3000";

    private String username = "PruebaAPI123";
    private String password = "PruebaAPI123";

    public  int contentText = 1;
    public  int contentVideo = 2;
    public  int contentImage = 3;
    public  int contentEvent = 4;
    public  int contentComment = 5;

    public  int userContentTypeLike = 1;
    public  int userContentTypeSave = 2;
    public  int userContentTypeReport = 3;
    public  int userContentTypeShare = 4;
    public  int userContentTypeChat = 5;

    public String paginationWall = "5";
    public String paginationContentSaved = "5";
    public String paginationContentLiked = "5";
    public String paginationNotification = "5";

    public int NotificationContentComment = 2;
    public int NotificationContentShare = 3;
    public int NotificationContentPointsChellenge = 5;
    public int NotificationContentVote = 4;
    public int NotificationContentLike = 1;
    public int NotificationContentFollow = 6;

    public  String WithOutResult = "No se encontraron más resultados";

    public  String IMAGE_DIRECTORY_NAME = "Android File Upload";

    public  String loadinMsg = "Por favor espere...";

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public  String getURL()
    {
        return URL;
    }

    public String GetUrlSocket() {
        return URL_SOCKET;
    }

    public void SetUrlSocket(String URL_SOCKET) {
        this.URL_SOCKET = URL_SOCKET;
    }
}
