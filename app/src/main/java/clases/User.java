package clases;

import android.util.Log;
import android.widget.Toast;

/**
 * Created by Nelson on 3/02/2017.
 */

public class User
{
    private int id;
    private  String name;
    private  String role;
    private  String company;
    private  String  city;
    private  String  email;
    private  String picture;
    private  int companyid;
    private  int cityid;
    private  int domainid;

    public User(int id , String name , String role , String company , String city , String email , int company_id , int city_id , int domain_id , String picture)
    {

        this.picture = picture;
        this.id = id;
        this.name = name;
        this.role = role;
        this.company = company;
        this.city = city;
        this.email = email;
        this.companyid = company_id;
        this.cityid = city_id;
        this.domainid = domain_id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCompany_id() {
        return companyid;
    }

    public void setCompany_id(int company_id) {
        this.companyid = company_id;
    }

    public int getCity_id() {
        return cityid;
    }

    public void setCity_id(int city_id) {
        this.cityid = city_id;
    }

    public int getDomain_id() {
        return domainid;
    }

    public void setDomain_id(int domain_id) {
        this.domainid = domain_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }



}
