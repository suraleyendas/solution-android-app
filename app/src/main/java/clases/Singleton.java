package clases;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import leyendasoy.leyenda.ChatActivity;
import leyendasoy.leyenda.MessageActivity;
import leyendasoy.leyenda.R;
import schema.Users;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Nelson on 30/03/2017.
 */

public class Singleton
{
    private static Singleton mInstance = null;


    private int UserID;
    private String Name;
    private String Photo;
    private String Email;
    private String City;
    private String Company;
    private int CityID;
    private int CompanyID;

    final Socket socket;
    ChatActivity chatActivity;
    MessageActivity messageActivity;
    NotificationManager notificationManager;
    Context context;

    File file = null;
    Bitmap bitmap;
    String videoPath;

    Context contextDB;
    Cursor cursor;

    private Singleton() throws URISyntaxException
    {
        MiHelper miHelper = new MiHelper();
        socket = IO.socket( miHelper.GetUrlSocket() );
        chatActivity = null;
        messageActivity = null;
    }

    public int getUserID() {
        return UserID;
    }

    public Context getContextDB() {
        return contextDB;
    }
    public void SaveUserNotification( int UserNotificated , int UserNotificater , String Message , Boolean IsViewed , int NotificationTypeId , int ContentId )
    {
        final MiHelper miHelper = new MiHelper();

        final RequestQueue queue = Volley.newRequestQueue(context);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserNotificated", String.valueOf(UserNotificated ));
        params.put("UserNotificater", String.valueOf(UserNotificater) );
        params.put("Message", Message );
        params.put("IsViewed", String.valueOf(IsViewed) );
        params.put("NotificationTypeId", String.valueOf(NotificationTypeId) );
        params.put("ContentId", String.valueOf(ContentId) );

        Log.i("SaveUser N 1: " , params.toString() );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "NotificationAPI/SaveUserNotification"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                Log.i("SaveUser N 2: " , response.toString());
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //getFollowed();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

    public void setContextDB(Context contextDB)
    {
        this.contextDB = contextDB;
        DbHelper dbHelper;
        dbHelper = new DbHelper(this.contextDB);
        cursor = dbHelper.getUser();

        Log.i("Cursor " , cursor.toString() );

        if (cursor != null)
        {
            Log.i("Count " ,""+ cursor.getCount() );
            if (cursor.getCount() > 0)
            {
                if (cursor.moveToFirst())
                {
                    UserID =  cursor.getInt(cursor.getColumnIndex( Users.UsersEntry.ID_USER ));
                    Name =  cursor.getString(cursor.getColumnIndex( Users.UsersEntry.NAME ));
                    Photo =  cursor.getString(cursor.getColumnIndex( Users.UsersEntry.PICTURE ));
                    Email =  cursor.getString(cursor.getColumnIndex( Users.UsersEntry.EMAIL ));
                    City =  cursor.getString(cursor.getColumnIndex( Users.UsersEntry.CITY ));
                    Company =  cursor.getString(cursor.getColumnIndex( Users.UsersEntry.COMPANY ));
                    CompanyID =  cursor.getInt(cursor.getColumnIndex( Users.UsersEntry.COMPANY_ID ));
                    CityID =  cursor.getInt(cursor.getColumnIndex( Users.UsersEntry.CITY_ID ));

                }
            }
        }
        cursor.close();
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    public static Singleton getInstance() throws URISyntaxException
    {
        if(mInstance == null)
        {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public MessageActivity getMessageActivity() {
        return messageActivity;
    }

    public void setMessageActivity(MessageActivity messageActivity) {
        this.messageActivity = messageActivity;
    }

    public ChatActivity getChatActivity()
    {
        return chatActivity;
    }

    public void setChatActivity(ChatActivity chatActivity)
    {
        this.chatActivity = chatActivity;
    }

    public void InitChat(final int UserId , final String picture , final String name )
    {
        try
        {
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    JSONObject obj = new JSONObject();
                    try
                    {
                        obj.put("user_id", UserId );
                        obj.put("picture", picture );
                        obj.put("name", name );
                        socket.emit("SaveOrUpdateUser", obj );
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }

            }).on("SendMessageReceive", new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {

                    JSONObject obj = (JSONObject)args[0];
                    try
                    {
                        JSONObject message = obj.getJSONObject("message");
                        String picture = obj.getString("picture");
                        String name = obj.getString("name");
                        Log.i("Respuesta server : " , obj.toString() );
                        if( chatActivity != null  )
                        {
                            if( chatActivity.StatusActivity == true  && chatActivity._userto == message.getInt("from_user") )
                            {
                                chatActivity.AddChatMessageFrom( message.getString("text") ,  message.getInt("from_user") );
                            }
                            else
                            {
                                NotificationMsg( 2, picture , R.drawable.ic_chat_bubble_outline_white_24dp, name, message.getString("text") );
                            }
                        }
                        else
                        {
                            NotificationMsg( 2, picture , R.drawable.ic_chat_bubble_outline_white_24dp, name, message.getString("text") );
                        }

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }


                }

            }).on("SendLastMessageReceive", new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {

                    JSONObject obj = (JSONObject)args[0];

                    Log.i("Obj : " , obj.toString() );

                    try
                    {

                        JSONArray messages = obj.getJSONArray("lastmessage");
                        if( messageActivity != null )
                        {
                            Log.i("Voy a agregar " , "ADDASDALJSDKALDKASD");
                            messageActivity.AddChatLastMessageFrom( messages  );
                        }

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }



                }
            }).on("MessagesList", new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    JSONObject obj = (JSONObject)args[0];
                    try
                    {
                        JSONArray message = obj.getJSONArray("messages");
                        if( chatActivity != null )
                        {
                            chatActivity.AddAllChatMessageFrom( message );
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener()
            {
                @Override
                public void call(Object... args) {}

            });
            socket.connect();
        }
        catch (Exception e)
        {
            Log.i("Socket" , e.getMessage() );
        }
    }
    public void NewMessage( JSONObject obj )
    {
        try
        {
            socket.emit("NewMessage", obj );
        }
        catch (Exception e)
        {
            Log.i("Socket" , e.getMessage() );
        }
    }
    public void GetMessages( JSONObject obj )
    {
        try
        {
            socket.emit("GetMessages", obj );
        }
        catch (Exception e)
        {
            Log.i("Socket" , e.getMessage() );
        }
    }
    public void GetLastMessages( JSONObject obj )
    {
        try
        {
            socket.emit("GetLastMessages", obj );
        }
        catch (Exception e)
        {
            Log.i("Socket" , e.getMessage() );
        }
    }
    public void NotificationMsg(int id, String largeImg ,int iconId, String titulo, String contenido) throws URISyntaxException
    {

        byte[] decodedString = Base64.decode( largeImg , Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        NotificationCompat.Builder builder =(NotificationCompat.Builder) new NotificationCompat.Builder( context )
                .setSmallIcon(iconId)
                .setLargeIcon(decodedByte)
                .setContentTitle(titulo)
                .setContentText(contenido) ;

        notificationManager.notify(id, builder.build());
    }
}
