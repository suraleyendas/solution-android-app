package clases;

import java.util.List;

import adparter_class.Answer;

/**
 * Created by Nelson on 24/03/2017.
 */

public class Question
{
    private int QuestionId;
    private String StrQuestion;
    private List<Answer> ListAnswer;
    private Boolean IsSelected;

    public Question(int questionId, String strQuestion, Boolean isSelected , List<Answer> listAnswer)
    {
        QuestionId = questionId;
        StrQuestion = strQuestion;
        IsSelected = isSelected;
        ListAnswer = listAnswer;
    }

    public Boolean getSelected() {
        return IsSelected;
    }

    public void setSelected(Boolean selected) {
        IsSelected = selected;
    }

    public int getQuestionId()
    {
        return QuestionId;
    }

    public void setQuestionId(int questionId)
    {
        QuestionId = questionId;
    }

    public String getStrQuestion()
    {
        return StrQuestion;
    }

    public void setStrQuestion(String strQuestion)
    {
        StrQuestion = strQuestion;
    }

    public List<Answer> getListAnswer()
    {
        return ListAnswer;
    }

    public void setListAnswer(List<Answer> listAnswer)
    {
        ListAnswer = listAnswer;
    }
}
