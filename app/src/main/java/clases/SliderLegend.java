package clases;

/**
 * Created by Nelson on 28/02/2017.
 */

public class SliderLegend
{
    private String photoUser;
    private String nameUser;
    private String date;
    private String email;
    private String city;
    private Boolean isVoted;
    private String UserId;

    public SliderLegend(String UserId , String photoUser, String nameUser, String date, String email, String city, Boolean isVoted) {

        this.UserId = UserId;
        this.photoUser = photoUser;
        this.nameUser = nameUser;
        this.date = date;
        this.email = email;
        this.city = city;
        this.isVoted = isVoted;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Boolean getVoted() {
        return isVoted;
    }

    public void setVoted(Boolean voted) {
        isVoted = voted;
    }
}
