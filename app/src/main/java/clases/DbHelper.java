package clases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import schema.Users;

/**
 * Created by Nelson on 3/02/2017.
 */

public class DbHelper extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Leyendas.db";
    SQLiteDatabase _db;
    Context context;

    public DbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        this._db = db;
        db.execSQL("CREATE TABLE " + Users.UsersEntry.TABLE_NAME + " ("
                + Users.UsersEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Users.UsersEntry.ID_USER + " INTEGER NOT NULL,"
                + Users.UsersEntry.NAME + " TEXT,"
                + Users.UsersEntry.ROLE + " TEXT,"
                + Users.UsersEntry.COMPANY + " TEXT,"
                + Users.UsersEntry.CITY + " TEXT,"
                + Users.UsersEntry.EMAIL + " TEXT,"
                + Users.UsersEntry.CITY_ID + " INTEGER,"
                + Users.UsersEntry.COMPANY_ID + " INTEGER,"
                + Users.UsersEntry.DOMAIN_ID + " INTEGER,"
                + Users.UsersEntry.PICTURE + " TEXT"
                + ")");

    }
    public void saveUser( User user , Context context , boolean type , int id )
    {
        ContentValues values = new ContentValues();
        values.put(Users.UsersEntry.ID_USER, user.getId() );
        values.put(Users.UsersEntry.NAME, user.getName() );
        values.put(Users.UsersEntry.ROLE, user.getRole() );
        values.put(Users.UsersEntry.COMPANY, user.getCompany() );
        values.put(Users.UsersEntry.CITY, user.getCity() );
        values.put(Users.UsersEntry.EMAIL, user.getEmail() );

        values.put(Users.UsersEntry.CITY_ID, user.getCity_id() );
        values.put(Users.UsersEntry.COMPANY_ID, user.getCompany_id() );
        values.put(Users.UsersEntry.DOMAIN_ID, user.getDomain_id() );


        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        if( type == true )
        {
            sqLiteDatabase.insert(Users.UsersEntry.TABLE_NAME, null, values);
        }
        else
        {
            sqLiteDatabase.update(Users.UsersEntry.TABLE_NAME, values, "_id=" + id , null);
        }

    }
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(  Users.UsersEntry.TABLE_NAME , null, null);
        db.close();
    }

    public Cursor getUser()
    {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("select * from " + Users.UsersEntry.TABLE_NAME, null);

        return  c;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Users.UsersEntry.TABLE_NAME );
        onCreate(db);

    }
}


