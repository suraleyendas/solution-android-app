package clases;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adparter_class.Award;
import leyendasoy.leyenda.AwardsActivity;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.WallActivity;

/**
 * Created by Nelson on 5/04/2017.
 */

public class ModalResume extends DialogFragment
{
    TextView txtName , txtRedimido , txtQuedan;
    ImageView photoProfile;
    Button btnContinue;
    static ModalResume ModalResume;
    Context context;
 ;
    int Redimido , Quedan;
    Award Award;

    public static ModalResume newInstance(AwardsActivity activity , Award award , int Redimido , int Quedan  )
    {

        ModalResume = new ModalResume();
        ModalResume.Award = award;
        ModalResume.Quedan = Quedan;
        ModalResume.Redimido = Redimido;
        ModalResume.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        return ModalResume;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View v = inflater.inflate(R.layout.modal_resume, container, false);

        ModalResume.context = v.getContext();
        RelativeLayout popup_root = ( RelativeLayout ) v.findViewById(R.id.popup_root);
        popup_root.getBackground().setAlpha(30);

        txtName = (TextView) v.findViewById(R.id.txtName);
        txtRedimido = (TextView) v.findViewById(R.id.txtRedimido);
        txtQuedan = (TextView) v.findViewById(R.id.txtQuedan);
        photoProfile = (ImageView) v.findViewById(R.id.photoProfile);

        byte[] decodedString = Base64.decode(ModalResume.Award.getPictureUser(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        photoProfile.setImageBitmap(decodedByte);

        txtRedimido.setText( "Has redimido " + ModalResume.Redimido + " Puntos" );
        txtQuedan.setText( "Te quedan " + ModalResume.Quedan + " Puntos" );
        txtName.setText( ModalResume.Award.getNameUser() );

        v.findViewById(R.id.btnContinue).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ModalResume.dismiss();
            }
        });

        return v;
    }

}
