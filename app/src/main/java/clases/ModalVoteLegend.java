package clases;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adapters.UserAdapter;
import leyendasoy.leyenda.R;
import clases.User;

/**
 * Created by Nelson on 15/02/2017.
 */

public class ModalVoteLegend extends DialogFragment
{
    EditText txtDescripcion;
    static ModalVoteLegend ModalVoteLegend;
    int _iduser ;
    Context context;
    ProgressDialog progressDialog;
    boolean vote;
    UserAdapter userAdapter;
    Button btn;
    User user;

    public static ModalVoteLegend newInstance(UserAdapter userAdapter , User user , int _iduser , Boolean vote )
    {
        ModalVoteLegend = new ModalVoteLegend();
        ModalVoteLegend.userAdapter = userAdapter;

        ModalVoteLegend.user = user;
        ModalVoteLegend._iduser = _iduser;
        ModalVoteLegend.vote = vote;

        ModalVoteLegend.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        return ModalVoteLegend;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View v = inflater.inflate(R.layout.modal_vote, container, false);

        ModalVoteLegend.context = v.getContext();

        TextView titulo = (TextView) v.findViewById(R.id.titulo);
        ImageView photoProfile = (ImageView) v.findViewById(R.id.photoProfile);
        TextView txtNombre = (TextView) v.findViewById(R.id.txtNombre);
        TextView txtCorreo = (TextView) v.findViewById(R.id.txtCorreo);
        TextView txtEmpresa = (TextView) v.findViewById(R.id.txtEmpresa);
        txtDescripcion  = (EditText) v.findViewById(R.id.txtDescripcion);

        btn = (Button) v.findViewById(R.id.btn_vote);

        txtDescripcion.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                String cadena = s.toString().trim();
                if( cadena.length() > 0 )
                {
                    btn.setEnabled(true);
                    btn.setBackgroundResource(R.drawable.double_blue);
                }
                else
                {
                    btn.setEnabled(false);
                    btn.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        txtNombre.setText( this.user.getName() );
        txtCorreo.setText( this.user.getEmail() );
        txtEmpresa.setText( this.user.getCompany() );

        if( this.user.getPicture().length() > 0 )
        {
            byte[] decodedString = Base64.decode(this.user.getPicture(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            photoProfile.setImageBitmap(decodedByte);
        }

        Typeface customFont1  = Typeface.createFromAsset( getActivity().getAssets() , "fonts/DINPro-Bold.otf");
        titulo.setTypeface(customFont1);
        txtNombre.setTypeface(customFont1);

        btn.setEnabled(false);
        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
        v.findViewById(R.id.btn_vote).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( txtDescripcion.getText().length() > 0 )
                {
                    txtDescripcion.setError(null);
                    ModalVoteLegend.SaveUserVote( ModalVoteLegend._iduser , ModalVoteLegend.user.getId() , ModalVoteLegend.txtDescripcion.getText().toString() , ModalVoteLegend.vote );
                }
                else
                {
                    txtDescripcion.setError("Esta campo es obligatorio.");
                }
            }
        });
        return v;
    }
    public  void SaveUserVote( int _iduser, int _iduser_vote, String description, boolean isActive )
    {

        progressDialog = new ProgressDialog( ModalVoteLegend.context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Por espere ...");
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(ModalVoteLegend.context);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("UserIdVote", String.valueOf(_iduser_vote) );
        params.put("Description", description );
        params.put("IsActive", String.valueOf(isActive) );


        final MiHelper miHelper = new MiHelper();
        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/SaveUserVote", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        ModalVoteLegend.userAdapter.UpdateColorItem( ModalVoteLegend.user.getId() );
                        dismiss();
                        Toast.makeText(ModalVoteLegend.context, "Voto guardado exitosamente", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        dismiss();
                        Toast.makeText(ModalVoteLegend.context, "No es posible votar por cuarta vez", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( ModalVoteLegend.context );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //SaveUserFollow(  userIdFollow ,  isFollow );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);


    }
}