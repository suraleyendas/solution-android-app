package adparter_class;

/**
 * Created by Nelson on 10/03/2017.
 */

public class Chat
{
    private  String id;
    private  int from_user;
    private  String photoUser;
    private  String nameUser;
    private  String textMessage;
    private  int type;

    public Chat(String id, int type , int from_user , String photoUser, String nameUser, String textMessage) {
        this.id = id;
        this.type = type;
        this.photoUser = photoUser;
        this.nameUser = nameUser;
        this.from_user = from_user;
        this.textMessage = textMessage;
    }

    public int getFrom_user() {
        return from_user;
    }

    public void setFrom_user(int from_user) {
        this.from_user = from_user;
    }

    public String getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }
}
