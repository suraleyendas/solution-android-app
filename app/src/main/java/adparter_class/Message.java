package adparter_class;

/**
 * Created by Nelson on 10/03/2017.
 */

public  class Message
{
    private int id;
    private String photoUser;
    private String nameUser;
    private String message;

    public Message(int id , String photoUser, String nameUser, String message) {
        this.id = id;
        this.photoUser = photoUser;
        this.nameUser = nameUser;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}