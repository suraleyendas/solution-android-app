package adparter_class;

/**
 * Created by Nelson on 4/04/2017.
 */

public class Award
{
    private  int Id;
    private  String Name;
    private  String Description;
    private  int Point;
    private  String Image;
    private  Boolean IsActive;
    private  int PointUser;
    private  String NameUser;
    private  String PictureUser;
    public Award(int id, String name, String description, int point, String image, Boolean isActive , int pointUser , String pictureUser , String nameUser ) {
        Id = id;
        Name = name;
        Description = description;
        Point = point;
        Image = image;
        IsActive = isActive;
        PointUser = pointUser;
        NameUser = nameUser;
        PictureUser = pictureUser;
    }

    public String getNameUser() {
        return NameUser;
    }
    public void setNameUser(String nameUser) {
        NameUser = nameUser;
    }
    public String getPictureUser() {
        return PictureUser;
    }

    public void setPictureUser(String pictureUser) {
        PictureUser = pictureUser;
    }

    public int getPointUser() {
        return PointUser;
    }

    public void setPointUser(int pointUser) {
        PointUser = pointUser;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getPoint() {
        return Point;
    }

    public void setPoint(int point) {
        Point = point;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }
}
