package adparter_class;

/**
 * Created by Nelson on 3/03/2017.
 */

public class EventLegend
{
    private String photoUser;
    private String nameUser;
    private String strContent;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    private String contentId;

    public EventLegend(String photoUser, String nameUser, String strContent , String contentId ) {
        this.photoUser = photoUser;
        this.nameUser = nameUser;
        this.strContent = strContent;
        this.contentId = contentId;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getStrContent() {
        return strContent;
    }

    public void setStrContent(String strContent) {
        this.strContent = strContent;
    }
}
