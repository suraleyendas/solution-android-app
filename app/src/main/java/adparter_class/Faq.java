package adparter_class;

/**
 * Created by Nelson on 10/04/2017.
 */

public class Faq
{
    private String icon;
    private int Id;
    private String Question;
    private String Answer;
    private Boolean IsSeleted;


    public Faq(String icon, int id, String question, String answer , Boolean isSeleted) {
        this.icon = icon;
        Id = id;
        Question = question;
        Answer = answer;
        IsSeleted = isSeleted;
    }

    public Boolean getSeleted() {
        return IsSeleted;
    }

    public void setSeleted(Boolean seleted) {
        IsSeleted = seleted;
    }

    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }
}
