package adparter_class;

/**
 * Created by Nelson on 13/02/2017.
 */

public class User
{
    private  String userId;
    private  String imagen;
    private  String titulo;
    private  String correo;
    private  boolean icon;
    private  int CountVotes;
    private  boolean IsVoted;

    public User(String userId , String imagen, String correo, String titulo, boolean icon , boolean isVoted , int countVotes , boolean isLegend  ) {
        this.userId = userId;
        this.imagen = imagen;
        this.correo = correo;
        this.titulo = titulo;
        this.icon = icon;
        this.isLegend = isLegend;
        CountVotes = countVotes;
        IsVoted = isVoted;
    }

    public int getCountVotes() {
        return CountVotes;
    }

    public void setCountVotes(int countVotes) {
        CountVotes = countVotes;
    }

    public boolean isVoted() {
        return IsVoted;
    }

    public void setVoted(boolean voted) {
        IsVoted = voted;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    private  boolean isFollow;

    public boolean isLegend() {
        return isLegend;
    }

    public void setLegend(boolean legend) {
        isLegend = legend;
    }

    private boolean isLegend;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        userId = userId;
    }



    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isIcon() {
        return icon;
    }

    public void setIcon(boolean icon) {
        this.icon = icon;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
