package adparter_class;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nelson on 20/02/2017.
 */

public class Wall
{
    private String UserName;
    private String Picture;
    private String ContentId;
    private String UserId;
    private String FilePath;
    private String strContent;
    private Boolean IsActive;
    private int ContentTypeId;
    private String ContentType;
    private Boolean IsLike;
    private int CountLike;
    private int CountComment;
    List<Comment> commentList;
    private int type;

    private Boolean IsChallenge;
    private String ChallengeTitle;
    private String ChallengeDescription;
    private int ChallengePoint;
    private Boolean IsParticipated;
    private String LimitDateChallenge;
    private int ChallengeTypeId;
    private int ChallengeId;
    private String ContentDescription;

    public Wall(String userName , String contentDescription , String Picture , String contentId, String userId, String filePath, String strContent, Boolean isActive, int contentTypeId, String contentType,  Boolean isLike, int countLike, int countComment , List<Comment> commentList ,  int type , Boolean IsChallenge ,  String ChallengeTitle ,String ChallengeDescription , int ChallengePoint , Boolean IsParticipated , String LimitDateChallenge , int challengeTypeId , int challengeId )
    {

        ContentDescription = contentDescription;
        ChallengeId = challengeId;
        ChallengeTypeId = challengeTypeId;
        this.IsChallenge = IsChallenge;
        this.ChallengeTitle = ChallengeTitle;
        this.ChallengeDescription = ChallengeDescription;
        this.ChallengePoint = ChallengePoint;
        this.IsParticipated = IsParticipated;
        this.LimitDateChallenge = LimitDateChallenge;

        UserName = userName;
        this.Picture = Picture;

        ContentId = contentId;
        UserId = userId;
        FilePath = filePath;
        this.strContent = strContent;
        IsActive = isActive;
        ContentTypeId = contentTypeId;
        ContentType = contentType;
        this.IsLike = isLike;
        CountLike = countLike;
        CountComment = countComment;
        this.commentList = commentList;
        this.type = type;
    }

    public String getContentDescription() {
        return ContentDescription;
    }

    public void setContentDescription(String contentDescription) {
        ContentDescription = contentDescription;
    }

    public int getChallengeId() {
        return ChallengeId;
    }

    public void setChallengeId(int challengeId) {
        ChallengeId = challengeId;
    }

    public int getChallengeTypeId() {
        return ChallengeTypeId;
    }

    public void setChallengeTypeId(int challengeTypeId) {
        ChallengeTypeId = challengeTypeId;
    }

    public Boolean getChallenge() {
        return IsChallenge;
    }

    public void setChallenge(Boolean challenge) {
        IsChallenge = challenge;
    }

    public String getChallengeTitle() {
        return ChallengeTitle;
    }

    public void setChallengeTitle(String challengeTitle) {
        ChallengeTitle = challengeTitle;
    }

    public String getChallengeDescription() {
        return ChallengeDescription;
    }

    public void setChallengeDescription(String challengeDescription) {
        ChallengeDescription = challengeDescription;
    }

    public int getChallengePoint() {
        return ChallengePoint;
    }

    public void setChallengePoint(int challengePoint) {
        ChallengePoint = challengePoint;
    }

    public Boolean getParticipated() {
        return IsParticipated;
    }

    public void setParticipated(Boolean participated) {
        IsParticipated = participated;
    }

    public String getLimitDateChallenge() {
        return LimitDateChallenge;
    }

    public void setLimitDateChallenge(String limitDateChallenge) {
        LimitDateChallenge = limitDateChallenge;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getContentId() {
        return ContentId;
    }

    public void setContentId(String contentId) {
        ContentId = contentId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public String getStrContent() {
        return strContent;
    }

    public void setStrContent(String strContent) {
        this.strContent = strContent;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public int getContentTypeId() {
        return ContentTypeId;
    }

    public void setContentTypeId(int contentTypeId) {
        ContentTypeId = contentTypeId;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String contentType) {
        ContentType = contentType;
    }



    public Boolean getLike() {
        return IsLike;
    }

    public void setLike(Boolean like) {
        IsLike = like;
    }

    public int getCountLike() {
        return CountLike;
    }

    public void setCountLike(int countLike) {
        CountLike = countLike;
    }

    public int getCountComment() {
        return CountComment;
    }

    public void setCountComment(int countComment) {
        CountComment = countComment;
    }



}


