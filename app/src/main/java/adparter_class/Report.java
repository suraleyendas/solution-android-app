package adparter_class;

/**
 * Created by Nelson on 3/03/2017.
 */

public class Report
{
    private int id;
    private String text;
    private Boolean isSeleted;

    public Report(int id , String text , Boolean isSeleted )
    {
        this.id = id;
        this.text = text;
        this.isSeleted = isSeleted;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
            this.text = text;
        }

    public Boolean getSeleted() {
        return isSeleted;
    }

    public void setSeleted(Boolean seleted) {
        isSeleted = seleted;
    }
}
