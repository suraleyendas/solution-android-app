package adparter_class;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nelson on 24/02/2017.
 */

public class Comment
{
    private String photoUser;
    private String nameUser;
    private String textComment;

    public Comment(String photoUser, String nameUser, String textComment) {
        this.photoUser = photoUser;
        this.nameUser = nameUser;
        this.textComment = textComment;
    }

    protected Comment(Parcel in) {
        photoUser = in.readString();
        nameUser = in.readString();
        textComment = in.readString();
    }



    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

}
