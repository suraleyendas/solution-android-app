package adparter_class;

/**
 * Created by Nelson on 6/04/2017.
 */

public class Notification
{
    private int NotificationId;
    private String Message;
    private int UserNotificaterId;
    private String UserName;
    private String Picture;
    private Boolean IsViewed;
    private int ContentId;
    private int NotificationTypeId;

    public Notification(int notificationId, String message, int userNotificaterId, String userName, String picture, Boolean isViewed, int contentId, int notificationTypeId) {
        NotificationId = notificationId;
        Message = message;
        UserNotificaterId = userNotificaterId;
        UserName = userName;
        Picture = picture;
        IsViewed = isViewed;
        ContentId = contentId;
        NotificationTypeId = notificationTypeId;
    }

    public int getNotificationId() {
        return NotificationId;
    }

    public void setNotificationId(int notificationId) {
        NotificationId = notificationId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getUserNotificaterId() {
        return UserNotificaterId;
    }

    public void setUserNotificaterId(int userNotificaterId) {
        UserNotificaterId = userNotificaterId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public Boolean getViewed() {
        return IsViewed;
    }

    public void setViewed(Boolean viewed) {
        IsViewed = viewed;
    }

    public int getContentId() {
        return ContentId;
    }

    public void setContentId(int contentId) {
        ContentId = contentId;
    }

    public int getNotificationTypeId() {
        return NotificationTypeId;
    }

    public void setNotificationTypeId(int notificationTypeId) {
        NotificationTypeId = notificationTypeId;
    }
}
