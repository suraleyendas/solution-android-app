package adparter_class;

/**
 * Created by Nelson on 13/02/2017.
 */

public class Followed
{
    private  int userId;
    private  String imagen;
    private  String titulo;
    private  String correo;
    private  boolean icon;

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    private  boolean isFollow;

    public boolean isLegend() {
        return isLegend;
    }

    public void setLegend(boolean legend) {
        isLegend = legend;
    }

    private boolean isLegend;

    public int getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        userId = userId;
    }

    public Followed(int userId , String imagen, String correo, String titulo, boolean icon , boolean isLegend  ) {
        this.userId = userId;
        this.imagen = imagen;
        this.correo = correo;
        this.titulo = titulo;
        this.icon = icon;
        this.isLegend = isLegend;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isIcon() {
        return icon;
    }

    public void setIcon(boolean icon) {
        this.icon = icon;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
