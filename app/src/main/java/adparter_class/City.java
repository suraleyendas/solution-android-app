package adparter_class;

/**
 * Created by Nelson on 6/02/2017.
 */

public class City
{
    private int id;
    private String name;

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Created by Nelson on 3/03/2017.
     */


    /**
     * Created by Nelson on 10/03/2017.
     */


}
