package adparter_class;

/**
 * Created by Nelson on 24/03/2017.
 */

public class Answer
{
    private int AnswerId;
    private String StrAnswer;
    private boolean IsCorrect;
    private String Color;
    private boolean IsSelected ;
    private int QuestionId;
    private int ShowIcon;

    public Answer(int answerId, String strAnswer, boolean isCorrect , String color , boolean isSelected , int questionId , int showIcon )
    {
        Color = color;
        AnswerId = answerId;
        StrAnswer = strAnswer;
        IsCorrect = isCorrect;
        IsSelected = isSelected;
        QuestionId = questionId;
        ShowIcon = showIcon;
    }

    public int getShowIcon() {
        return ShowIcon;
    }

    public void setShowIcon(int showIcon) {
        ShowIcon = showIcon;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public boolean isSelected() {
        return IsSelected;
    }

    public void setSelected(boolean selected) {
        IsSelected = selected;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        this.Color = color;
    }

    public int getAnswerId() {
        return AnswerId;
    }

    public void setAnswerId(int answerId) {
        AnswerId = answerId;
    }

    public String getStrAnswer() {
        return StrAnswer;
    }

    public void setStrAnswer(String strAnswer) {
        StrAnswer = strAnswer;
    }

    public boolean isCorrect() {
        return IsCorrect;
    }

    public void setCorrect(boolean correct) {
        IsCorrect = correct;
    }
}
