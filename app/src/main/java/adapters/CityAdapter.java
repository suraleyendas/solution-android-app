package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import adparter_class.City;

/**
 * Created by Nelson on 6/02/2017.
 */

public class CityAdapter extends ArrayAdapter<City>
{

        private Context context;
        private List<City> values;

        public CityAdapter(Context context, int textViewResourceId,List<City> values)
        {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
        }
        public int getCount()
        {
            return values.size();
        }

        public City getItem(int position)
        {
            return values.get(position);
        }

        public long getItemId(int position)
        {
            return position;
        }
        public List<City> getItems()
        {
           return this.values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TextView label = new TextView(context);
            label.setTextSize(10);
            label.setTextColor(Color.BLACK);
            label.setText(values.get(position).getName() );
            return label;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            TextView label = new TextView(context);
            label.setPadding(10,20,5,20);
            label.setTextColor(Color.BLACK);
            label.setText(values.get(position).getName());

            return label;
        }

}
