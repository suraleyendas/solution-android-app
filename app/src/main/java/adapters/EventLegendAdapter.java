package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adparter_class.EventLegend;
import leyendasoy.leyenda.EventActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 13/02/2017.
 */

public class EventLegendAdapter extends  RecyclerView.Adapter<EventLegendAdapter.EventLegendViewHolder>  implements  Filterable
{
    private List<EventLegend> items;
    private ArrayList<EventLegend> itemsFilter;
    private CustomFilter mFilter;
    EventActivity eventActivity;

    public EventLegendAdapter(List<EventLegend> items , EventActivity eventActivity )
    {
        this.eventActivity = eventActivity;
        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(EventLegendAdapter.this);
    }

    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<EventLegend> getList() {
        return this.itemsFilter;
    }
    @Override
    public EventLegendViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.base_event_legend, viewGroup, false);
        return new EventLegendViewHolder(v , this , this.eventActivity );
    }
    @Override
    public void onBindViewHolder(EventLegendViewHolder viewHolder, int i)
    {
        viewHolder.nameUser.setText(itemsFilter.get(i).getNameUser());
        viewHolder.strContent.setText(itemsFilter.get(i).getStrContent());

        if (itemsFilter.get(i).getPhotoUser().length() > 0)
        {
            byte[] decodedString = Base64.decode(itemsFilter.get(i).getPhotoUser(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.photoUser.setImageBitmap(decodedByte);
        }
        else
            viewHolder.photoUser.setImageResource(R.drawable.default_user);

    }
    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class EventLegendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView photoUser;
        public TextView nameUser;
        public TextView strContent;

        public EventActivity eventActivity;
        public EventLegendAdapter eventLegendAdapter;


        public EventLegendViewHolder(View v,  EventLegendAdapter eventLegendAdapter , EventActivity eventActivity )
        {
            super(v);
            photoUser = (ImageView) v.findViewById(R.id.photoUser);
            nameUser = (TextView) v.findViewById(R.id.nameUser);
            strContent = (TextView) v.findViewById(R.id.strContent);

            this.eventActivity = eventActivity;
            this.eventLegendAdapter = eventLegendAdapter;

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            EventLegend eventLegend = eventLegendAdapter.itemsFilter.get(getAdapterPosition());
            eventActivity.goToComment( eventLegend.getContentId() );
        }
    }
    public class CustomFilter extends Filter
    {
        private EventLegendAdapter listAdapter;

        private CustomFilter(EventLegendAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final EventLegend eventLegend : items)
                {
                    if ( eventLegend.getNameUser().toLowerCase().contains(filterPattern)  )
                    {
                        itemsFilter.add(eventLegend);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}