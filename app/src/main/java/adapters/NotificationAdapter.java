package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import adparter_class.Answer;
import adparter_class.Comment;
import adparter_class.Notification;
import adparter_class.Wall;
import leyendasoy.leyenda.ChallengeQuestionActivity;
import leyendasoy.leyenda.CommentActivity;
import leyendasoy.leyenda.NotificationActivity;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.ShareActivity;

/**
 * Created by Nelson on 3/03/2017.
 */

public class NotificationAdapter extends  RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>
{
    private List<Notification> items;
    Context context;
    public NotificationAdapter(List<Notification> items , Context context )
    {
        this.items = items;
        this.context = context;
    }
    public void clear()
    {
        items.clear();
    }
    public void addAll(List<Notification> list)
    {
        items.addAll(list);
        this.notifyDataSetChanged();
    }
    @Override
    public int getItemCount()
    {
        return items.size();
    }
    public List<Notification> getList()
    {
        return this.items;
    }
    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_list_base, viewGroup, false);
        return new NotificationAdapter.NotificationViewHolder(v,this,context);
    }
    @Override
    public void onBindViewHolder(NotificationAdapter.NotificationViewHolder viewHolder, int i)
    {
        viewHolder.UserName.setText( items.get(i).getUserName() );
        viewHolder.Message.setText( items.get(i).getMessage() );
        byte[] decodedString = Base64.decode(items.get(i).getPicture(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        viewHolder.Picture.setImageBitmap(decodedByte);
    }
    public static class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView UserName;
        public TextView Message;
        public ImageView Picture;
        public NotificationAdapter notificationAdapter;
        public Context context;
        public RelativeLayout Relative;

        public NotificationViewHolder(View v, NotificationAdapter notificationAdapter , Context context )
        {
            super(v);

            UserName = (TextView) v.findViewById(R.id.UserName);
            Message = (TextView) v.findViewById(R.id.Message);
            Picture = (ImageView) v.findViewById(R.id.Picture);
            Relative = (RelativeLayout) v.findViewById(R.id.Relative);
            this.notificationAdapter = notificationAdapter;
            this.context = context;

            Relative.setOnClickListener(this);

        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view)
        {

            Notification notification = notificationAdapter.items.get(getAdapterPosition());
            GoToComment( notification.getContentId() , notification.getNotificationId() );

        }
        public  void GoToComment(  int contentId ,  int NotificationID  )
        {
            Intent intent = new Intent(context, CommentActivity.class);
            intent.putExtra("ContentID", contentId );
            intent.putExtra("NotificationID", NotificationID );
            context.startActivity( intent );
        }
    }


}