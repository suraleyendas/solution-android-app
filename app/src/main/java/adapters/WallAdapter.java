package adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adparter_class.User;
import adparter_class.Wall;
import at.blogc.android.views.ExpandableTextView;
import clases.CustomVolleyRequest;
import clases.MiHelper;
import clases.Singleton;
import leyendasoy.leyenda.ChallegeDetailActivity;
import leyendasoy.leyenda.CommentActivity;
import leyendasoy.leyenda.MediaChallengeActivity;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.ReportActivity;
import leyendasoy.leyenda.ShareActivity;

/**
 * Created by Nelson on 13/02/2017.
 */

public class WallAdapter extends  RecyclerView.Adapter<WallAdapter.WallViewHolder>  implements  Filterable
{
    private List<Wall> items;
    private Context context;
    private ArrayList<Wall> itemsFilter;
    private CustomFilter mFilter;
    private MiHelper miHelper;
    private int Redirect;

    public WallAdapter(List<Wall> items , Context context , int redirect )
    {
        this.context = context;
        miHelper = new MiHelper();

        Redirect = redirect;
        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(WallAdapter.this);
    }
    public void clear()
    {
        items.clear();
        itemsFilter.clear();
    }
    public void addAll(List<Wall> list)
    {
        items.addAll(list);
        itemsFilter.addAll(list);
        this.notifyDataSetChanged();
    }
    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Wall> getList() {
        return this.itemsFilter;
    }
    @Override
    public WallViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wall_base_list, viewGroup, false);
        return new WallViewHolder(v ,  context , this , Redirect );
    }
    @Override
    public void onBindViewHolder(final WallViewHolder viewHolder, int i)
    {
        viewHolder.nameUser.setText( itemsFilter.get(i).getUserName() );
        byte[] decodedString = Base64.decode(itemsFilter.get(i).getPicture(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        viewHolder.pictureUser.setImageBitmap(decodedByte);

        viewHolder.viewDetailChallenge.setVisibility(View.GONE);
        viewHolder.layoutChallenge2.setVisibility(View.GONE);
        viewHolder.contentText.setVisibility(View.GONE);
        viewHolder.layoutChallenge1.setVisibility(View.GONE);
        viewHolder.layoutUser.setVisibility(View.GONE);
        viewHolder.contentImg.setVisibility(View.GONE);
        viewHolder.contentVideo.setVisibility(View.GONE);
        viewHolder.txtInfoContent.setVisibility(View.GONE);
        viewHolder.listaComments.setVisibility(View.GONE);
        viewHolder.layoutRecycler.setVisibility(View.GONE);
        viewHolder.seekBarVideo.setVisibility(View.GONE);

        viewHolder.play.setVisibility(View.GONE);
        viewHolder.pause.setVisibility(View.GONE);
        viewHolder.seguirLeyendo.setVisibility(View.GONE);

        viewHolder.countLike.setText( String.valueOf( itemsFilter.get(i).getCountLike()) );
        viewHolder.countComment.setText( String.valueOf( itemsFilter.get(i).getCountComment()) );


        if( itemsFilter.get(i).getChallenge() )
        {
            viewHolder.viewDetailChallenge.setVisibility(View.VISIBLE);
            viewHolder.layoutChallenge1.setVisibility(View.VISIBLE);
            viewHolder.tituloPuntos.setText( String.valueOf( itemsFilter.get(i).getChallengePoint()) + " Pts." );
            viewHolder.tituloChallenge.setText( itemsFilter.get(i).getChallengeTitle().toUpperCase() );

            viewHolder.layoutChallenge2.setVisibility(View.VISIBLE);
            viewHolder.descChallenge.setText( String.valueOf( itemsFilter.get(i).getChallengeDescription()) );
            viewHolder.fechaLimite.setText( "Fecha límite : " + String.valueOf( itemsFilter.get(i).getLimitDateChallenge()) );

        }
        else
        {
            viewHolder.layoutUser.setVisibility(View.VISIBLE);
        }

        if( itemsFilter.get(i).getContentTypeId() ==  miHelper.contentText )
        {
            if( itemsFilter.get(i).getChallenge() == false )
            {
                viewHolder.contentText.setVisibility( View.VISIBLE);
                viewHolder.contentText.setText( itemsFilter.get(i).getStrContent() );

                if( itemsFilter.get(i).getStrContent().length() > 200 )
                {
                    viewHolder.seguirLeyendo.setVisibility(View.VISIBLE);
                }

                if( itemsFilter.get(i).getContentDescription().trim().length() > 0 )
                {
                    viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                    viewHolder.txtInfoContent.setText( itemsFilter.get(i).getContentDescription().trim() );
                }

            }
            else
            {
                viewHolder.txtInfoContent.setVisibility( View.VISIBLE);
                viewHolder.txtInfoContent.setText( itemsFilter.get(i).getStrContent() );
            }

        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentEvent )
        {
            try
            {
                if( itemsFilter.get(i).getType() ==  miHelper.contentText )
                {
                    viewHolder.contentText.setVisibility( View.VISIBLE);
                    viewHolder.contentText.setText( itemsFilter.get(i).getStrContent() );

                    if( itemsFilter.get(i).getStrContent().length() > 200 )
                    {
                        viewHolder.seguirLeyendo.setVisibility(View.VISIBLE);
                    }
                }
                else if( itemsFilter.get(i).getType() == miHelper.contentVideo )
                {
                    if( itemsFilter.get(i).getFilePath().toString().length() > 0 )
                    {
                        Uri uri = Uri.parse(itemsFilter.get(i).getFilePath().toString());
                        viewHolder.contentVideo.setVideoURI(uri);
                        viewHolder.contentVideo.seekTo(100);

                        viewHolder.contentVideo.setVisibility(View.VISIBLE);

                        viewHolder.contentVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            @Override
                            public void onPrepared(MediaPlayer mp)
                            {
                                viewHolder.seekBarVideo.setVisibility(View.VISIBLE);
                                viewHolder.play.setVisibility(View.VISIBLE);

                            }
                        });
                    }
                    else
                    {

                        viewHolder.contentImg.setBackgroundResource(R.drawable.video_no_disponible);
                        viewHolder.contentImg.setVisibility(View.VISIBLE);

                    }
                    if (itemsFilter.get(i).getStrContent().length() > 0)
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText(itemsFilter.get(i).getStrContent());
                    }
                }
                else if( itemsFilter.get(i).getType() == miHelper.contentImage )
                {
                    viewHolder.contentImg.setVisibility(View.VISIBLE);

                    ImageLoader imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
                    imageLoader.get( itemsFilter.get(i).getFilePath() , ImageLoader.getImageListener(viewHolder.contentImg, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
                    viewHolder.contentImg.setImageUrl( itemsFilter.get(i).getFilePath() , imageLoader);

                    if( itemsFilter.get(i).getStrContent().length() > 0 )
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText( itemsFilter.get(i).getStrContent() );
                    }
                }
            }
            catch (Exception e)
            {
                Log.i("Errrrrrr" , e.getMessage() );
            }
        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentVideo )
        {
            try
            {
                if(Build.VERSION.SDK_INT >=  Build.VERSION_CODES.LOLLIPOP)
                {
                }
                if( itemsFilter.get(i).getFilePath().toString().length() > 0 )
                {
                    Uri uri = Uri.parse(itemsFilter.get(i).getFilePath().toString());

                    viewHolder.contentVideo.setVisibility(View.VISIBLE);
                    viewHolder.contentVideo.setVideoURI(uri);

                    viewHolder.contentVideo.seekTo(100);
                    viewHolder.contentVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                    {
                        @Override
                        public void onPrepared(MediaPlayer mp)
                        {
                            viewHolder.seekBarVideo.setVisibility(View.VISIBLE);
                            viewHolder.play.setVisibility(View.VISIBLE);
                        }
                    });

                    if (itemsFilter.get(i).getStrContent().length() > 0)
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText(itemsFilter.get(i).getStrContent());
                    }

                }
                else
                {
                    viewHolder.contentImg.setBackgroundResource(R.drawable.video_no_disponible);
                    viewHolder.contentImg.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e)
            {
                 Log.i("Errrrrrr" , e.getMessage() );
            }
        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentImage )
        {
            viewHolder.contentImg.setVisibility(View.VISIBLE);

            ImageLoader imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
            imageLoader.get( itemsFilter.get(i).getFilePath() , ImageLoader.getImageListener(viewHolder.contentImg, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
            viewHolder.contentImg.setImageUrl( itemsFilter.get(i).getFilePath() , imageLoader);

            if( itemsFilter.get(i).getStrContent().length() > 0 )
            {
                viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                viewHolder.txtInfoContent.setText( itemsFilter.get(i).getStrContent() );
            }
        }



        Wall miWall = itemsFilter.get(i);
        if( miWall.getLike() )
        {
              viewHolder.like.setBackgroundResource(R.mipmap.like_blue);
        }
        else
        {
            viewHolder.like.setBackgroundResource(R.mipmap.like);
        }

        if( itemsFilter.get(i).getCommentList().size() > 0 )
        {
            viewHolder.listaComments.setVisibility(View.VISIBLE);
            viewHolder.layoutRecycler.setVisibility(View.VISIBLE);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager( context.getApplicationContext() );
            viewHolder.listaComments.setLayoutManager(linearLayoutManager);
            final CommentAdapter listAdapter = new CommentAdapter( itemsFilter.get(i).getCommentList() );
            viewHolder.listaComments.setAdapter(listAdapter);
        }
    }
    @Override
    public Filter getFilter()
    {
        return mFilter;
    }
    public static class WallViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ProgressDialog progressDialog = null;

        public ImageView pictureUser;
        public TextView nameUser , countLike , countComment ,  seguirLeyendo , txtInfoContent ,  descChallenge , fechaLimite;

        public ExpandableTextView contentText;
        public NetworkImageView contentImg;

        LinearLayout layoutChallenge1 , layoutUser ,  layoutChallenge2;
        public VideoView contentVideo;
        RecyclerView listaComments;

        public ImageView like , comment , share , report;

        SeekBar seekBarVideo;

        public Context context;
        public WallAdapter wallAdapter;

        public LinearLayout layoutRecycler ,  layoutLike , layoutComment ,  layoutShare , layoutReport;
        RelativeLayout play , pause;

        public FrameLayout tooltip;

        public Wall selectedWall = null;
        PopupWindow mpopup = null;

        TextView tituloPuntos , tituloChallenge ;
        Button viewDetailChallenge;
        int Redirect;


        public WallViewHolder(View v , Context context ,  WallAdapter wallAdapter , int redirect )
        {
            super(v);
            this.context =  context;
            this.wallAdapter = wallAdapter;
            Redirect = redirect;
            viewDetailChallenge = ( Button ) v.findViewById(R.id.viewDetailChallenge);

            pictureUser = (ImageView) v.findViewById(R.id.pictureUser);
            nameUser = ( TextView ) v.findViewById(R.id.nameUser);
            countLike = ( TextView ) v.findViewById(R.id.countLike);
            countComment = ( TextView ) v.findViewById(R.id.countComment);

            tituloPuntos = ( TextView ) v.findViewById(R.id.tituloPuntos);
            tituloChallenge = ( TextView ) v.findViewById(R.id.tituloChallenge);

            descChallenge = ( TextView ) v.findViewById(R.id.descChallenge);
            fechaLimite = ( TextView ) v.findViewById(R.id.fechaLimite);

            seguirLeyendo = ( TextView ) v.findViewById(R.id.seguirLeyendo);
            layoutChallenge1 = ( LinearLayout ) v.findViewById(R.id.layoutChallenge1);

            layoutUser = ( LinearLayout ) v.findViewById(R.id.layoutUser);
            layoutChallenge2 = ( LinearLayout ) v.findViewById(R.id.layoutChallenge2);

            contentText = (ExpandableTextView) v.findViewById(R.id.contentText);
            contentText.setAnimationDuration(1000L);
            contentText.setInterpolator(new OvershootInterpolator());
            contentText.setExpandInterpolator(new OvershootInterpolator());
            contentText.setCollapseInterpolator(new OvershootInterpolator());

            contentImg = (NetworkImageView) v.findViewById(R.id.contentImg);
            contentVideo = (VideoView) v.findViewById(R.id.contentVideo);

            play = ( RelativeLayout ) v.findViewById(R.id.play );
            play.setVisibility(View.GONE);

            pause = ( RelativeLayout ) v.findViewById(R.id.pause );
            pause.setVisibility(View.GONE);

            txtInfoContent = ( TextView ) v.findViewById(R.id.txtInfoContent);
            listaComments = ( RecyclerView ) v.findViewById(R.id.listaComments);

            seekBarVideo = ( SeekBar )  v.findViewById(R.id.seekBarVideo);
            seekBarVideo.setVisibility(View.GONE);

            like = ( ImageView ) v.findViewById(R.id.like);
            comment = ( ImageView ) v.findViewById(R.id.comment);
            share = ( ImageView ) v.findViewById(R.id.share);
            report = ( ImageView ) v.findViewById(R.id.report);

            layoutRecycler = (LinearLayout) v.findViewById(R.id.layoutRecycler);

            layoutLike = (LinearLayout) v.findViewById(R.id.layoutLike);
            layoutComment = (LinearLayout) v.findViewById(R.id.layoutComment);
            layoutShare = (LinearLayout) v.findViewById(R.id.layoutShare);
            layoutReport = (LinearLayout) v.findViewById(R.id.layoutReport);

            layoutLike.setOnClickListener(this);
            layoutComment.setOnClickListener(this);
            layoutShare.setOnClickListener(this);
            layoutReport.setOnClickListener(this);
            viewDetailChallenge.setOnClickListener(this);

            play.setOnClickListener(this);
            pause.setOnClickListener(this);

            seguirLeyendo.setOnClickListener(this);
        }
        @Override
        public void onClick(final View view)
        {
            int position = getAdapterPosition();
            Wall miWall = wallAdapter.itemsFilter.get(position);


            if( mpopup != null )
                mpopup.dismiss();

            MiHelper miHelper = new MiHelper();
            if( view.getId() == R.id.viewDetailChallenge )
            {
                goToChallegeDatail( Integer.parseInt( miWall.getContentId() ) , miWall.getChallengeId() , miWall.getChallengeTypeId()  );
            }
            else if( view.getId() == seguirLeyendo.getId() )
            {
                TextView seguirLeyendo = (TextView) view;
                View parent = (View) view.getParent();
                ExpandableTextView contentText = ( ExpandableTextView ) parent.findViewById(R.id.contentText );

                if (contentText.isExpanded())
                {
                    contentText.collapse();
                    seguirLeyendo.setText("Seguir Leyendo");
                }
                else
                {
                    contentText.expand();
                    seguirLeyendo.setText("Ocultar");
                }

            }
            else if( view.getId() == R.id.layoutLike )
            {
                View parent = (View) view.getParent();
                TextView countLike = ( TextView ) parent.findViewById(R.id.countLike);

                ImageView like = ( ImageView ) view.findViewById(R.id.like);

                if( miWall.getLike() == false )
                {
                    countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) + 1 ) );
                    wallAdapter.itemsFilter.get(position).setLike(true);
                    like.setBackgroundResource(R.mipmap.like_blue);
                    SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");


                    try
                    {
                        int _iduser = Singleton.getInstance().getUserID();
                        String _name = Singleton.getInstance().getName();
                        Singleton.getInstance().SaveUserNotification( Integer.parseInt(miWall.getUserId()) , _iduser ,  "A " + _name +  " le ha gustado tu publicación" , true , miHelper.NotificationContentLike , Integer.parseInt(miWall.getContentId()) );

                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) - 1 ) );
                    wallAdapter.itemsFilter.get(position).setLike(false);
                    like.setBackgroundResource(R.mipmap.like);
                    SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");
                }
            }
            else if( view.getId() == R.id.layoutComment )
            {

               goToComment( miWall.getContentId()  );
            }
            else if( view.getId() == R.id.layoutReport )
            {
                selectedWall = wallAdapter.itemsFilter.get(position);

                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View popUpView = li.inflate(R.layout.poput_report,null);
                mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
                mpopup.showAsDropDown(view , 0, 0);

                TextView reportContent = ( TextView ) popUpView.findViewById(R.id.ReportContent);
                reportContent.setOnClickListener(this);
                TextView saveContent = ( TextView ) popUpView.findViewById(R.id.SaveContent);
                saveContent.setOnClickListener(this);

            }
            else if( view.getId() == R.id.ReportContent)
            {
               goToReport( selectedWall.getContentId() );
            }
            else if( view.getId() == R.id.SaveContent)
            {
                 SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeSave ) , "" , "");
                 Toast.makeText(context, "Contenido guardado", Toast.LENGTH_SHORT).show();
            }
            else if( view.getId() == R.id.layoutShare )
            {
                selectedWall = wallAdapter.itemsFilter.get(position);
                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popUpView = li.inflate(R.layout.poput_share,null);
                mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
                mpopup.showAsDropDown(view , 0, 0);

                TextView shareWithFriend = ( TextView ) popUpView.findViewById(R.id.ShareWithFriends);
                shareWithFriend.setOnClickListener(this);

                TextView sendAsPrivateMsg = ( TextView ) popUpView.findViewById(R.id.SendAsPrivateMsg);
            }
            else if( view.getId() == R.id.ShareWithFriends)
            {
                goToShare(  String.valueOf(selectedWall.getContentId() ) );
            }
            else if( view.getId() == R.id.SendAsPrivateMsg)
            {
                ///Toast.makeText(wallActivity, "SendAsPrivateMsg", Toast.LENGTH_SHORT).show();
            }
            else if( view.getId() == play.getId() )
            {
                view.setVisibility(View.GONE);
                final View parent = (View) view.getParent();

                final  RelativeLayout play = ( RelativeLayout ) parent.findViewById(R.id.play);



                final VideoView videoV = ( VideoView ) parent.findViewById(R.id.contentVideo);
                final SeekBar seekbar = ( SeekBar ) parent.findViewById(R.id.seekBarVideo);
                videoV.start();
                seekbar.setMax( videoV.getDuration() );



                final boolean[] mVideoCompleted = {false};

                videoV.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer)
                    {
                        mVideoCompleted[0] = true;
                        videoV.seekTo(100);
                        view.setVisibility(View.VISIBLE);
                    }
                });
                videoV.setOnTouchListener(new View.OnTouchListener()
                {
                    RelativeLayout pause = ( RelativeLayout ) parent.findViewById(R.id.pause);
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if (mVideoCompleted[0])
                        {
                            view.setVisibility(View.VISIBLE);
                            videoV.seekTo(100);
                        }
                        if (videoV.isPlaying())
                        {
                            videoV.pause();
                            view.setVisibility(View.GONE);
                            pause.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    view.setVisibility(View.VISIBLE);
                                    pause.setVisibility(View.GONE);
                                }
                            }, 500);
                        }
                        return true;
                    }
                });
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
                {
                    int toque = 0;
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar)
                    {
                        // TODO Auto-generated method stub
                        toque = 0;
                        Log.i("Dejado " ," " + toque );
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar)
                    {
                        // TODO Auto-generated method stub
                        toque = 1;
                        Log.i("INdex tocado " ," Me han empezado tocado");
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                    {
                        // TODO Auto-generated method stub
                        //Log.i("Mi toque" ," " + toque );
                        if( toque == 1 )
                        {
                            videoV.seekTo( progress );
                        }
                    }
                });

                Runnable onEverySecond=new Runnable()
                {
                    public void run()
                    {
                        if(seekbar != null)
                        {
                            seekbar.setProgress(videoV.getCurrentPosition());
                        }
                        if(videoV.isPlaying())
                        {
                            seekbar.postDelayed(this, 10);
                        }
                    }
                };
                seekbar.postDelayed(onEverySecond, 10);


            }
        }


        public void SaveUserContent( String ContentId , String UserShareId , String UserContentTypeId , String ReportTypeId , String ReporDescription )
        {
            final MiHelper miHelper = new MiHelper();
            if( progressDialog == null )
            {
                progressDialog = new ProgressDialog( context );
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setIndeterminate(true);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setMessage( miHelper.loadinMsg );
                progressDialog.show();
            }
            final RequestQueue queue = Volley.newRequestQueue(context);

            int _iduser = 0;
            try
            {
                _iduser = Singleton.getInstance().getUserID();
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }

            Map<String, String> params = new HashMap<String, String>();
            params.put("UserId", String.valueOf(_iduser ));
            params.put("ContentId", ContentId );
            params.put("UserShareId", UserShareId );
            params.put("UserContentTypeId", UserContentTypeId );
            params.put("ReportTypeId", ReportTypeId );
            params.put("ReportDescription", ReporDescription );
            params.put("Viewed", ReporDescription );


            JSONObject jsonObj = new JSONObject(params);

            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveUserContent"  , jsonObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        Log.i( "Rs " , response.toString() );
                        progressDialog.dismiss();
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();
                    if (error.networkResponse == null)
                    {
                        if (error.getClass().equals(TimeoutError.class))
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Error en la conexión");
                            builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    //getFollowed();
                                }
                            }).show();
                        }
                    }
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                    return params;
                }
            };

            queue.add(getRequest);
        }


        public  void goToComment( String ContentID  )
        {
            Intent intent = new Intent(context, CommentActivity.class);
            intent.putExtra("ContentID", Integer.parseInt(ContentID) );
            intent.putExtra("Redirect", Redirect );
            context.startActivity(intent);
        }
        public  void goToReport( String ContentID )
        {
            Intent intent = new Intent(context, ReportActivity.class);
            intent.putExtra("ContentID", Integer.parseInt(ContentID) );
            context.startActivity(intent);
        }
        public  void goToShare(  String contentId )
        {
            Intent intent = new Intent(context, ShareActivity.class);
            intent.putExtra("ContentID", Integer.parseInt(contentId) );
            intent.putExtra("Redirect", Redirect );
            context.startActivity( intent );
        }
        public  void goToChallegeDatail( int ContentID , int ChallengeID , int ChallegeTypeID )
        {
            Intent intent = new Intent(context, ChallegeDetailActivity.class);
            intent.putExtra("ContentID", ContentID );
            intent.putExtra("ChallengeID", ChallengeID );
            intent.putExtra("ChallengeTypeID", ChallegeTypeID  );
            context.startActivity(intent);
        }
    }
    public class CustomFilter extends Filter
    {
        private WallAdapter listAdapter;

        private CustomFilter(WallAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Wall wall : items)
                {
                    /*
                    if ( wall.getTitulo().toLowerCase().contains(filterPattern) || person.getCorreo().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(person);
                    }
                    */
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }
}