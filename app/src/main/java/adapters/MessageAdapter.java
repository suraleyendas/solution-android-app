package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import adparter_class.City;
import adparter_class.Followed;
import adparter_class.Message;
import leyendasoy.leyenda.ChatActivity;
import leyendasoy.leyenda.MessageActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 13/02/2017.
 */

public class MessageAdapter extends  RecyclerView.Adapter<MessageAdapter.MessageViewHolder>  implements  Filterable
{
    private List<Message> items;
    private  Context context;
    private ArrayList<Message> itemsFilter;
    private CustomFilter mFilter;
    public MessageAdapter(List<Message> items , Context context )
    {
        this.context = context;
        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(MessageAdapter.this);
    }
    public void clear()
    {
        items.clear();
        itemsFilter.clear();
    }
    public void addAll(List<Message> list)
    {
        items.addAll(list);
        itemsFilter.addAll(list);
        this.notifyDataSetChanged();
    }
    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Message> getList() {
        return this.itemsFilter;
    }
    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_base_list, viewGroup, false);
        return new MessageViewHolder(v , this.context , this );
    }
    public void removeItem(int position)
    {
        itemsFilter.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, itemsFilter.size());
    }

    public int UpdateLastMessage(JSONArray messages ) throws JSONException
    {

        for ( int i = 0 ; i < messages.length() ; i++ )
        {
             JSONObject message = messages.getJSONObject(i);
             for ( int j = 0 ; j < items.size() ; j++ )
             {
                 if( message.getInt("from") == items.get(j).getId() ||  message.getInt("to") == items.get(j).getId()  )
                 {
                      items.get(j).setMessage( message.getString("message") );
                 }
             }
        }
        return itemsFilter.size();

    }
    @Override
    public void onBindViewHolder(MessageViewHolder viewHolder, int i)
    {

        if (itemsFilter.get(i).getPhotoUser().length() > 0)
        {
            byte[] decodedString = Base64.decode(itemsFilter.get(i).getPhotoUser(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.photoUser.setImageBitmap(decodedByte);
        }
        else
            viewHolder.photoUser.setImageResource(R.drawable.default_user);

        viewHolder.nameUser.setText(itemsFilter.get(i).getNameUser());
        viewHolder.message.setText(itemsFilter.get(i).getMessage());

    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView photoUser;
        public TextView nameUser;
        public TextView message;
        public Context context;
        public MessageAdapter messageAdapter;


        public MessageViewHolder(View v ,Context context , MessageAdapter messageAdapter  )
        {
            super(v);
            photoUser = (ImageView) v.findViewById(R.id.photoUser);
            nameUser = (TextView) v.findViewById(R.id.nameUser);
            message = (TextView) v.findViewById(R.id.message);

            this.context = context;
            this.messageAdapter = messageAdapter;

            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View view)
        {
            Message message = messageAdapter.itemsFilter.get( getAdapterPosition() );
            goToChat( message.getId() ,  message.getPhotoUser() );
        }
        public  void goToChat( int UserTo ,  String PhotoUser )
        {
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("UserTo", UserTo );
            intent.putExtra("PhotoUser", PhotoUser );
            context.startActivity(intent);
        }
    }
    public class CustomFilter extends Filter
    {
        private MessageAdapter listAdapter;

        private CustomFilter(MessageAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Message message : items)
                {
                    if ( message.getMessage().toLowerCase().contains(filterPattern) || message.getNameUser().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(message);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}