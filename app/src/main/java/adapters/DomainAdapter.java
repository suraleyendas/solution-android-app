package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import adparter_class.Domain;

/**
 * Created by Nelson on 13/02/2017.
 */

public class DomainAdapter extends ArrayAdapter<Domain>
{
    private Context context;
    private List<Domain> values;

    public DomainAdapter(Context context, int textViewResourceId, List<Domain> values)
    {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount()
    {
        return values.size();
    }

    public Domain getItem(int position)
    {
        return values.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView label = new TextView(context);
        label.setTextSize(10);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getDomain() );

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    public List<Domain> getItems()
    {
        return this.values;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        TextView label = new TextView(context);
        label.setPadding(10,20,5,20);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getDomain());

        return label;
    }
}
