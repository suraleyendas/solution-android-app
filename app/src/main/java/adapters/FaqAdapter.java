package adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

import adparter_class.Faq;
import adparter_class.Report;
import leyendasoy.leyenda.FaqActivity;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.ReportActivity;

/**
 * Created by Nelson on 3/03/2017.
 */

public class FaqAdapter extends  RecyclerView.Adapter<FaqAdapter.FaqViewHolder>
{
    private List<Faq> items;
    private FaqActivity faqActivity;
    public  TextView viewAnterior = null;

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    public FaqAdapter(List<Faq> items, FaqActivity parent)
    {
        this.faqActivity = parent;
        this.items = items;
        mPref = parent.getSharedPreferences("faq", Context.MODE_PRIVATE);
        mEditor = mPref.edit();
    }
    public void setSelected(int pos)
    {
        try
        {
            if (items.size() > 1)
            {
                items.get(mPref.getInt("position", 0)).setSeleted(false);
                mEditor.putInt("position", pos);
                mEditor.commit();
            }
            items.get(pos).setSeleted(true);
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<Faq> getList() {
        return this.items;
    }
    @Override
    public FaqAdapter.FaqViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faq_base_list, viewGroup, false);
        return new FaqAdapter.FaqViewHolder(v, this.faqActivity, this);
    }
    @Override
    public void onBindViewHolder(FaqAdapter.FaqViewHolder viewHolder, int i)
    {

        viewHolder.Question.setText(items.get(i).getQuestion());
        viewHolder.Answer.setText(items.get(i).getAnswer());

        if ( items.get(i).getSeleted())
        {
            viewHolder.LayoutQuestion.setBackgroundColor(Color.parseColor("#0063a7"));
            viewHolder.Question.setTextColor(Color.parseColor("#ffffff"));
        }
        else
        {
            viewHolder.LayoutQuestion.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.Question.setTextColor(Color.parseColor("#256289"));
        }
    }
    public static class FaqViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView Icon;
        public TextView Question;
        public TextView Answer;
        public ExpandableLayout LayoutAnswer;
        public LinearLayout LayoutQuestion;

        public FaqActivity faqActivity;
        public FaqAdapter reportAdapter;


        public FaqViewHolder(View v, FaqActivity faqActivity, FaqAdapter reportAdapter)
        {
            super(v);

            Icon = (ImageView) v.findViewById(R.id.Icon);
            Question = (TextView) v.findViewById(R.id.Question);
            Answer = (TextView) v.findViewById(R.id.Answer);

            LayoutQuestion = (LinearLayout) v.findViewById(R.id.LayoutQuestion);
            LayoutAnswer = (ExpandableLayout) v.findViewById(R.id.LayoutAnswer);

            this.faqActivity = faqActivity;
            this.reportAdapter = reportAdapter;

            LayoutQuestion.setOnClickListener(this);

             // LayoutAnswer.setVisibility(View.GONE);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view)
        {
            int position = getAdapterPosition();
            Faq faq = reportAdapter.items.get(getAdapterPosition());

            View parent = (View) view.getParent();
            ImageView icon = ( ImageView ) parent.findViewById(R.id.Icon);

            if (view.getId() == LayoutQuestion.getId())
            {
                reportAdapter.setSelected(position);
                ExpandableLayout expandableLayout = ( ExpandableLayout ) parent.findViewById(R.id.LayoutAnswer);


                if ( !expandableLayout.isExpanded())
                {
                    expandableLayout.expand();
                    //Animation animation = AnimationUtils.loadAnimation(faqActivity, R.anim.rotate_faq_1);
                    //icon.startAnimation(animation);
                }
                else
                {
                    expandableLayout.collapse();
                    //Animation animation = AnimationUtils.loadAnimation(faqActivity, R.anim.rotate_faq_2);
                    //icon.startAnimation(animation);
                }
            }

        }
    }
}