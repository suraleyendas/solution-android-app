package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import adparter_class.Chat;
import adparter_class.Message;
import leyendasoy.leyenda.ChatActivity;
import leyendasoy.leyenda.MessageActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 13/02/2017.
 */

public class ChatAdapter extends  RecyclerView.Adapter<ChatAdapter.ChatViewHolder>  implements  Filterable
{
    private List<Chat> items;
    private ChatActivity chatActivity;
    private ArrayList<Chat> itemsFilter;
    private CustomFilter mFilter;
    public ChatAdapter(List<Chat> items , ChatActivity parent )
    {
        this.chatActivity = parent;

        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(ChatAdapter.this);
    }
    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Chat> getList() {
        return this.itemsFilter;
    }
    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_base_list, viewGroup, false);
        return new ChatViewHolder(v , this.chatActivity , this );
    }
    public int addItem(String msg , int from_user , String usuario , int type , String photo )
    {
        itemsFilter.add( new Chat("10" , type  , from_user , photo , usuario , msg  ) );
        return itemsFilter.size();
    }
    @Override
    public void onBindViewHolder(ChatViewHolder viewHolder, int i)
    {
        if( itemsFilter.get(i).getType() == 1 )
        {
            viewHolder.linearLayout1.setVisibility(View.VISIBLE);
            viewHolder.linearLayout2.setVisibility(View.GONE);


            byte[] decodedString = Base64.decode(itemsFilter.get(i).getPhotoUser(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.photoUser1.setImageBitmap(decodedByte);

            viewHolder.textMessage1.setText(itemsFilter.get(i).getTextMessage());
        }
        else
        {
            viewHolder.linearLayout2.setVisibility(View.VISIBLE);
            viewHolder.linearLayout1.setVisibility(View.GONE);


            byte[] decodedString = Base64.decode(itemsFilter.get(i).getPhotoUser(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.photoUser2.setImageBitmap(decodedByte);


            viewHolder.textMessage2.setText(itemsFilter.get(i).getTextMessage());
        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap>
    {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage)
        {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls)
        {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try
            {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            }
            catch (Exception e)
            {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result)
        {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class ChatViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout linearLayout1;
        LinearLayout linearLayout2;

        public ImageView photoUser1;
        public TextView textMessage1;
        public ImageView photoUser2;
        public TextView textMessage2;
        public ChatActivity chatActivity;
        public ChatAdapter chatAdapter;


        public ChatViewHolder(View v , ChatActivity chatActivity , ChatAdapter chatAdapter )
        {
            super(v);

            linearLayout1 = ( LinearLayout ) v.findViewById(R.id.layout1);
            linearLayout2 = ( LinearLayout ) v.findViewById(R.id.layout2);

            linearLayout1.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.GONE);

            photoUser1 = (ImageView) v.findViewById(R.id.photoUser1);

            textMessage1 = (TextView) v.findViewById(R.id.textMessage1);

            photoUser2 = (ImageView) v.findViewById(R.id.photoUser2);
            textMessage2 = (TextView) v.findViewById(R.id.textMessage2);

            this.chatActivity = chatActivity;
            this.chatAdapter = chatAdapter;
        }
    }
    public class CustomFilter extends Filter
    {
        private ChatAdapter listAdapter;

        private CustomFilter(ChatAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Chat chat : items)
                {
                    if ( chat.getNameUser().toLowerCase().contains(filterPattern) || chat.getTextMessage().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(chat);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}