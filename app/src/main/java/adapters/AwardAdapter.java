package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.net.URISyntaxException;
import java.util.List;

import adparter_class.Answer;
import adparter_class.Award;
import clases.CustomVolleyRequest;
import clases.ModalConfirm;
import clases.ModalVote;
import clases.Singleton;
import leyendasoy.leyenda.AwardsActivity;
import leyendasoy.leyenda.ChallengeQuestionActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 3/03/2017.
 */

public class AwardAdapter extends  RecyclerView.Adapter<AwardAdapter.AwardViewHolder>
{
    private List<Award> items;
    AwardsActivity AwardsActivity;
    public AwardAdapter(List<Award> items , AwardsActivity awardsActivity )
    {
        this.items = items;
        AwardsActivity = awardsActivity;
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    @Override
    public AwardAdapter.AwardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.awards_base_list, viewGroup, false);
        return new AwardAdapter.AwardViewHolder(v,this,AwardsActivity);
    }
    @Override
    public void onBindViewHolder(AwardAdapter.AwardViewHolder viewHolder, int i)
    {
        viewHolder.Name.setText( items.get(i).getName() );
        viewHolder.Description.setText( items.get(i).getDescription() );
        viewHolder.Point.setText( String.valueOf( items.get(i).getPoint()) +  " Pts." );

        ImageLoader imageLoader = CustomVolleyRequest.getInstance(AwardsActivity).getImageLoader();
        imageLoader.get( items.get(i).getImage() , ImageLoader.getImageListener(viewHolder.Image, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
        viewHolder.Image.setImageUrl( items.get(i).getImage() , imageLoader);
        if( i == items.size() - 1 )
            viewHolder.Line.setVisibility(View.GONE);

    }
    public static class AwardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView Name , Description , Point;
        public NetworkImageView Image;
        LinearLayout Line;
        Button Redeem;
        public AwardAdapter awardAdapter;
        public AwardsActivity awardsActivity;

        public AwardViewHolder(View v, AwardAdapter awardAdapter , AwardsActivity awardsActivity )
        {
            super(v);
            Line = ( LinearLayout ) v.findViewById(R.id.Line);
            Image = (NetworkImageView) v.findViewById(R.id.Image);
            Name = (TextView) v.findViewById(R.id.Name);
            Description = (TextView) v.findViewById(R.id.Description);
            Point = (TextView) v.findViewById(R.id.Point);
            Redeem = (Button) v.findViewById(R.id.Redeem);

            this.awardAdapter = awardAdapter;
            this.awardsActivity = awardsActivity;

            Redeem.setOnClickListener(this);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view)
        {
            Award MiAward = awardAdapter.items.get(getAdapterPosition());
            if( view.getId() == R.id.Redeem )
            {
                ModalConfirm.newInstance( awardsActivity , MiAward  ).show( awardsActivity.getFragmentManager(), null);
            }
        }
    }
}