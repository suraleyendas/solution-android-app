package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

import adparter_class.Wall;
import at.blogc.android.views.ExpandableTextView;
import clases.CustomVolleyRequest;
import clases.MiHelper;
import leyendasoy.leyenda.ChallengeActivity;
import leyendasoy.leyenda.ContentSavedActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 21/03/2017.
 */

public class SavedAdapter extends  RecyclerView.Adapter<SavedAdapter.SavedViewHolder>  implements Filterable
{
    private List<Wall> items;
    private ContentSavedActivity contentSavedActivity;
    private ArrayList<Wall> itemsFilter;
    private CustomFilter mFilter;
    private MiHelper miHelper;


    public SavedAdapter(List<Wall> items , ContentSavedActivity parent )
    {
        this.contentSavedActivity = parent;
        miHelper = new MiHelper();

        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(SavedAdapter.this);
    }
    public void clear()
    {
        items.clear();
        itemsFilter.clear();
    }
    public void addAll(List<Wall> list)
    {
        items.addAll(list);
        itemsFilter.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Wall> getList() {
        return this.itemsFilter;
    }
    @Override
    public SavedViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wall_base_list, viewGroup, false);
        return new SavedViewHolder(v , this.contentSavedActivity, this );
    }
    @Override
    public void onBindViewHolder(final SavedViewHolder viewHolder, int i)
    {
        viewHolder.nameUser.setText( itemsFilter.get(i).getUserName() );
        byte[] decodedString = Base64.decode(itemsFilter.get(i).getPicture(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        viewHolder.pictureUser.setImageBitmap(decodedByte);

        viewHolder.layoutChallenge2.setVisibility(View.GONE);
        viewHolder.contentText.setVisibility(View.GONE);
        viewHolder.layoutChallenge1.setVisibility(View.GONE);
        viewHolder.layoutUser.setVisibility(View.GONE);
        viewHolder.contentImg.setVisibility(View.GONE);
        viewHolder.contentVideo.setVisibility(View.GONE);
        viewHolder.txtInfoContent.setVisibility(View.GONE);
        viewHolder.listaComments.setVisibility(View.GONE);
        viewHolder.layoutRecycler.setVisibility(View.GONE);
        viewHolder.seekBarVideo.setVisibility(View.GONE);

        viewHolder.play.setVisibility(View.GONE);
        viewHolder.pause.setVisibility(View.GONE);
        viewHolder.seguirLeyendo.setVisibility(View.GONE);

        viewHolder.countLike.setText( String.valueOf( itemsFilter.get(i).getCountLike()) );
        viewHolder.countComment.setText( String.valueOf( itemsFilter.get(i).getCountComment()) );

        if( itemsFilter.get(i).getChallenge() )
        {
            viewHolder.layoutChallenge1.setVisibility(View.VISIBLE);
            viewHolder.tituloPuntos.setText( String.valueOf( itemsFilter.get(i).getChallengePoint()) + " Pts." );
            viewHolder.tituloChallenge.setText( itemsFilter.get(i).getChallengeTitle() );

            viewHolder.layoutChallenge2.setVisibility(View.VISIBLE);
            viewHolder.descChallenge.setText( String.valueOf( itemsFilter.get(i).getChallengeDescription()) );
            viewHolder.fechaLimite.setText( "Fecha límite : " + String.valueOf( itemsFilter.get(i).getLimitDateChallenge()) );

        }
        else
        {
            viewHolder.layoutUser.setVisibility(View.VISIBLE);
        }


        if( itemsFilter.get(i).getContentTypeId() ==  miHelper.contentText )
        {
            viewHolder.contentText.setVisibility( View.VISIBLE);
            viewHolder.contentText.setText( itemsFilter.get(i).getStrContent() );

            if( itemsFilter.get(i).getStrContent().length() > 200 )
            {
                viewHolder.seguirLeyendo.setVisibility(View.VISIBLE);
            }
        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentEvent )
        {
            try
            {
                if( itemsFilter.get(i).getType() ==  miHelper.contentText )
                {
                    viewHolder.contentText.setVisibility( View.VISIBLE);
                    viewHolder.contentText.setText( itemsFilter.get(i).getStrContent() );

                    if( itemsFilter.get(i).getStrContent().length() > 200 )
                    {
                        viewHolder.seguirLeyendo.setVisibility(View.VISIBLE);
                    }
                }
                else if( itemsFilter.get(i).getType() == miHelper.contentVideo )
                {
                    if( itemsFilter.get(i).getFilePath().toString().length() > 0 )
                    {
                        Uri uri = Uri.parse(itemsFilter.get(i).getFilePath().toString());
                        viewHolder.contentVideo.setVideoURI(uri);
                        viewHolder.contentVideo.seekTo(100);

                        viewHolder.contentVideo.setVisibility(View.VISIBLE);

                        viewHolder.contentVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            @Override
                            public void onPrepared(MediaPlayer mp)
                            {
                                viewHolder.seekBarVideo.setVisibility(View.VISIBLE);
                                viewHolder.play.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    else
                    {

                        viewHolder.contentImg.setBackgroundResource(R.drawable.video_no_disponible);
                        viewHolder.contentImg.setVisibility(View.VISIBLE);

                    }
                    if (itemsFilter.get(i).getStrContent().length() > 0)
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText(itemsFilter.get(i).getStrContent());
                    }
                }
                else if( itemsFilter.get(i).getType() == miHelper.contentImage )
                {
                    viewHolder.contentImg.setVisibility(View.VISIBLE);

                    ImageLoader imageLoader = CustomVolleyRequest.getInstance(contentSavedActivity).getImageLoader();
                    imageLoader.get( itemsFilter.get(i).getFilePath() , ImageLoader.getImageListener(viewHolder.contentImg, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
                    viewHolder.contentImg.setImageUrl( itemsFilter.get(i).getFilePath() , imageLoader);

                    if( itemsFilter.get(i).getStrContent().length() > 0 )
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText( itemsFilter.get(i).getStrContent() );
                    }
                }
            }
            catch (Exception e)
            {
                Log.i("Errrrrrr" , e.getMessage() );
            }
        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentVideo )
        {
            try
            {
                if(Build.VERSION.SDK_INT >=  Build.VERSION_CODES.LOLLIPOP)
                {
                }
                if( itemsFilter.get(i).getFilePath().toString().length() > 0 )
                {
                    Uri uri = Uri.parse(itemsFilter.get(i).getFilePath().toString());

                    viewHolder.contentVideo.setVisibility(View.VISIBLE);
                    viewHolder.contentVideo.setVideoURI(uri);

                    viewHolder.contentVideo.seekTo(100);
                    viewHolder.contentVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                    {
                        @Override
                        public void onPrepared(MediaPlayer mp)
                        {
                            viewHolder.seekBarVideo.setVisibility(View.VISIBLE);
                            viewHolder.play.setVisibility(View.VISIBLE);
                        }
                    });

                    if (itemsFilter.get(i).getStrContent().length() > 0)
                    {
                        viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                        viewHolder.txtInfoContent.setText(itemsFilter.get(i).getStrContent());
                    }

                }
                else
                {
                    viewHolder.contentImg.setBackgroundResource(R.drawable.video_no_disponible);
                    viewHolder.contentImg.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e)
            {
                Log.i("Errrrrrr" , e.getMessage() );
            }
        }
        else if( itemsFilter.get(i).getContentTypeId() == miHelper.contentImage )
        {
            viewHolder.contentImg.setVisibility(View.VISIBLE);

            ImageLoader imageLoader = CustomVolleyRequest.getInstance(contentSavedActivity).getImageLoader();
            imageLoader.get( itemsFilter.get(i).getFilePath() , ImageLoader.getImageListener(viewHolder.contentImg, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
            viewHolder.contentImg.setImageUrl( itemsFilter.get(i).getFilePath() , imageLoader);

            if( itemsFilter.get(i).getStrContent().length() > 0 )
            {
                viewHolder.txtInfoContent.setVisibility(View.VISIBLE);
                viewHolder.txtInfoContent.setText( itemsFilter.get(i).getStrContent() );
            }
        }


        Wall miWall = itemsFilter.get(i);
        if( miWall.getLike() )
        {
            viewHolder.like.setBackgroundResource(R.mipmap.like_blue);
        }
        else
        {
            viewHolder.like.setBackgroundResource(R.mipmap.like);
        }

        if( itemsFilter.get(i).getCommentList().size() > 0 )
        {
            viewHolder.listaComments.setVisibility(View.VISIBLE);
            viewHolder.layoutRecycler.setVisibility(View.VISIBLE);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager( contentSavedActivity.getApplicationContext() );
            viewHolder.listaComments.setLayoutManager(linearLayoutManager);
            final CommentAdapter listAdapter = new CommentAdapter( itemsFilter.get(i).getCommentList() );
            viewHolder.listaComments.setAdapter(listAdapter);
        }
    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class SavedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        RelativeLayout videoRelative , imgRelative ;

        TextView tituloChallenge , tituloPuntos , fechaLimite ;
        public ImageView pictureUser;
        public TextView nameUser , countLike , countComment ,  seguirLeyendo , txtInfoContent , descChallenge;
        public ExpandableTextView contentText;
        public NetworkImageView contentImg;
        public VideoView contentVideo;

        RecyclerView listaComments;
        public ImageView like , comment , share , report;
        SeekBar seekBarVideo;
        public ContentSavedActivity contentSavedActivity;
        public SavedAdapter savedAdapter;

        public LinearLayout layoutRecycler ,  layoutLike , layoutComment ,  layoutShare , layoutReport , layoutContentText ,  layoutUser;
        public LinearLayout layoutChallenge1 , layoutChallenge2;
        RelativeLayout play , pause;

        public FrameLayout tooltip;

        public Wall selectedWall = null;
        PopupWindow mpopup = null;
        Button viewDetailChallenge;


        public SavedViewHolder(View v , ContentSavedActivity contentSavedActivity , SavedAdapter savedAdapter )
        {
            super(v);
            this.contentSavedActivity = contentSavedActivity;
            this.savedAdapter = savedAdapter;

            viewDetailChallenge = ( Button ) v.findViewById(R.id.viewDetailChallenge);

            videoRelative = ( RelativeLayout ) v.findViewById(R.id.videoRelative);
            imgRelative = ( RelativeLayout ) v.findViewById(R.id.imgRelative);

            pictureUser = (ImageView) v.findViewById(R.id.pictureUser);
            layoutContentText = (LinearLayout) v.findViewById(R.id.layoutContentText);
            layoutChallenge1 = (LinearLayout) v.findViewById(R.id.layoutChallenge1);
            layoutChallenge2 = (LinearLayout) v.findViewById(R.id.layoutChallenge2);

            descChallenge = ( TextView ) v.findViewById(R.id.descChallenge);

            nameUser = ( TextView ) v.findViewById(R.id.nameUser);
            countLike = ( TextView ) v.findViewById(R.id.countLike);
            countComment = ( TextView ) v.findViewById(R.id.countComment);

            tituloChallenge = ( TextView ) v.findViewById(R.id.tituloChallenge);
            tituloPuntos = ( TextView ) v.findViewById(R.id.tituloPuntos);
            fechaLimite = ( TextView ) v.findViewById(R.id.fechaLimite);

            seguirLeyendo = ( TextView ) v.findViewById(R.id.seguirLeyendo);
            layoutChallenge1 = ( LinearLayout ) v.findViewById(R.id.layoutChallenge1);

            contentText = (ExpandableTextView) v.findViewById(R.id.contentText);
            contentText.setAnimationDuration(1000L);
            contentText.setInterpolator(new OvershootInterpolator());
            contentText.setExpandInterpolator(new OvershootInterpolator());
            contentText.setCollapseInterpolator(new OvershootInterpolator());

            contentImg = (NetworkImageView) v.findViewById(R.id.contentImg);
            contentVideo = (VideoView) v.findViewById(R.id.contentVideo);

            play = ( RelativeLayout ) v.findViewById(R.id.play );
            pause = ( RelativeLayout ) v.findViewById(R.id.pause );

            txtInfoContent = ( TextView ) v.findViewById(R.id.txtInfoContent);
            listaComments = ( RecyclerView ) v.findViewById(R.id.listaComments);

            seekBarVideo = ( SeekBar )  v.findViewById(R.id.seekBarVideo);

            like = ( ImageView ) v.findViewById(R.id.like);
            comment = ( ImageView ) v.findViewById(R.id.comment);
            share = ( ImageView ) v.findViewById(R.id.share);
            report = ( ImageView ) v.findViewById(R.id.report);

            layoutRecycler = (LinearLayout) v.findViewById(R.id.layoutRecycler);
            layoutUser = (LinearLayout) v.findViewById(R.id.layoutUser);

            layoutLike = (LinearLayout) v.findViewById(R.id.layoutLike);
            layoutComment = (LinearLayout) v.findViewById(R.id.layoutComment);
            layoutShare = (LinearLayout) v.findViewById(R.id.layoutShare);
            layoutReport = (LinearLayout) v.findViewById(R.id.layoutReport);

            layoutLike.setOnClickListener(this);
            layoutComment.setOnClickListener(this);
            layoutShare.setOnClickListener(this);
            layoutReport.setOnClickListener(this);

            play.setOnClickListener(this);
            viewDetailChallenge.setOnClickListener(this);

            seguirLeyendo.setOnClickListener(this);
        }
        @Override
        public void onClick(final View view)
        {
            int position = getAdapterPosition();
            Wall challenge = savedAdapter.itemsFilter.get(position);

            if( mpopup != null )
                mpopup.dismiss();

            MiHelper miHelper = new MiHelper();

            if( view.getId() == R.id.viewDetailChallenge )
            {

            }
            if( view.getId() == seguirLeyendo.getId() )
            {
                TextView seguirLeyendo = (TextView) view;
                View parent = (View) view.getParent();
                ExpandableTextView contentText = ( ExpandableTextView ) parent.findViewById(R.id.contentText );

                if (contentText.isExpanded())
                {
                    contentText.collapse();
                    seguirLeyendo.setText("Seguir Leyendo");
                }
                else
                {
                    contentText.expand();
                    seguirLeyendo.setText("Ocultar");
                }

            }

            if( view.getId() == R.id.layoutLike )
            {
                View parent = (View) view.getParent();
                TextView countLike = ( TextView ) parent.findViewById(R.id.countLike);

                ImageView like = ( ImageView ) view.findViewById(R.id.like);

                if( challenge.getLike() == false )
                {
                    countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) + 1 ) );
                    savedAdapter.itemsFilter.get(position).setLike(true);
                    like.setBackgroundResource(R.mipmap.like_blue);
                    //challengeActivity.SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");
                }
                else
                {
                    countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) - 1 ) );
                    savedAdapter.itemsFilter.get(position).setLike(false);
                    like.setBackgroundResource(R.mipmap.like);
                    //wallActivity.SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");
                }
            }
            else if( view.getId() == R.id.layoutComment )
            {
                //wallActivity.goToComment( miWall.getContentId() );
            }
            else if( view.getId() == R.id.layoutReport )
            {
                selectedWall = savedAdapter.itemsFilter.get(position);

                View popUpView = contentSavedActivity.getLayoutInflater().inflate(R.layout.poput_report,null);
                mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
                mpopup.showAsDropDown(view , 0, 0);

                TextView reportContent = ( TextView ) popUpView.findViewById(R.id.ReportContent);
                reportContent.setOnClickListener(this);
                TextView saveContent = ( TextView ) popUpView.findViewById(R.id.SaveContent);
                saveContent.setOnClickListener(this);
            }
            else if( view.getId() == R.id.ReportContent)
            {
                // wallActivity.goToReport( selectedWall.getContentId() );
            }
            else if( view.getId() == R.id.SaveContent)
            {
                // wallActivity.SaveUserContent( miWall.getContentId() , "" , String.valueOf(  miHelper.userContentTypeSave ) , "" , "");
                Toast.makeText(contentSavedActivity, "Contenido guardado", Toast.LENGTH_SHORT).show();
            }
            else if( view.getId() == R.id.layoutShare )
            {
                selectedWall = savedAdapter.itemsFilter.get(position);

                View popUpView = contentSavedActivity.getLayoutInflater().inflate(R.layout.poput_share,null);
                mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
                mpopup.showAsDropDown(view , 0, 0);

                TextView shareWithFriend = ( TextView ) popUpView.findViewById(R.id.ShareWithFriends);
                shareWithFriend.setOnClickListener(this);

                TextView sendAsPrivateMsg = ( TextView ) popUpView.findViewById(R.id.SendAsPrivateMsg);
                sendAsPrivateMsg.setOnClickListener(this);

            }
            else if( view.getId() == R.id.ShareWithFriends)
            {

                //wallActivity.goToShare(  String.valueOf(selectedWall.getContentId() ) );
            }
            else if( view.getId() == R.id.SendAsPrivateMsg)
            {
                ///Toast.makeText(wallActivity, "SendAsPrivateMsg", Toast.LENGTH_SHORT).show();
            }
            else if( view.getId() == play.getId() )
            {
                view.setVisibility(View.GONE);
                final View parent = (View) view.getParent();

                final  RelativeLayout play = ( RelativeLayout ) parent.findViewById(R.id.play);

                final VideoView videoV = ( VideoView ) parent.findViewById(R.id.contentVideo);
                final SeekBar seekbar = ( SeekBar ) parent.findViewById(R.id.seekBarVideo);
                videoV.start();
                seekbar.setMax( videoV.getDuration() );


                final boolean[] mVideoCompleted = {false};

                videoV.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer)
                    {
                        mVideoCompleted[0] = true;
                        view.setVisibility(View.VISIBLE);
                    }
                });
                videoV.setOnTouchListener(new View.OnTouchListener()
                {
                    RelativeLayout pause = ( RelativeLayout ) parent.findViewById(R.id.pause);
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if (mVideoCompleted[0])
                        {
                            view.setVisibility(View.VISIBLE);
                        }
                        if (videoV.isPlaying())
                        {
                            videoV.pause();
                            view.setVisibility(View.VISIBLE);
                        }
                        return true;
                    }
                });
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
                {
                    int toque = 0;
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar)
                    {
                        // TODO Auto-generated method stub
                        toque = 0;
                        Log.i("Dejado " ," " + toque );
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar)
                    {
                        // TODO Auto-generated method stub
                        toque = 1;
                        Log.i("INdex tocado " ," Me han empezado tocado");
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                    {
                        // TODO Auto-generated method stub
                        Log.i("Mi toque" ," " + toque );
                        if( toque == 1 )
                        {
                            videoV.seekTo( progress );
                        }
                    }
                });

                Runnable onEverySecond=new Runnable()
                {
                    public void run()
                    {
                        if(seekbar != null)
                        {
                            seekbar.setProgress(videoV.getCurrentPosition());
                        }
                        if(videoV.isPlaying())
                        {
                            seekbar.postDelayed(this, 10);
                        }
                    }
                };
                seekbar.postDelayed(onEverySecond, 10);
            }
        }
    }

    public class CustomFilter extends Filter
    {
        private SavedAdapter listAdapter;

        private CustomFilter(SavedAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Wall challenge : items)
                {
                    /*
                    if ( wall.getTitulo().toLowerCase().contains(filterPattern) || person.getCorreo().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(person);
                    }
                    */
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}