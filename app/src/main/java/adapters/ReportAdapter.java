package adapters;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import adparter_class.Followed;
import adparter_class.Report;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.ReportActivity;

/**
 * Created by Nelson on 3/03/2017.
 */
import android.content.Context;

public class ReportAdapter extends  RecyclerView.Adapter<ReportAdapter.ReportViewHolder>
{
    private List<Report> items;
    private ReportActivity reportActivity;
    private FollowedAdapter.CustomFilter mFilter;
    public  TextView viewAnterior = null;

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    public ReportAdapter(List<Report> items, ReportActivity parent)
    {
        this.reportActivity = parent;
        this.items = items;
        mPref = parent.getSharedPreferences("report", Context.MODE_PRIVATE);
        mEditor = mPref.edit();
    }
    public void setSelected(int pos)
    {
        try
        {
            if (items.size() > 1)
            {
                items.get(mPref.getInt("position", 0)).setSeleted(false);
                mEditor.putInt("position", pos);
                mEditor.commit();
            }
            items.get(pos).setSeleted(true);
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<Report> getList() {
        return this.items;
    }
    @Override
    public ReportAdapter.ReportViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.base_list_report, viewGroup, false);
        return new ReportAdapter.ReportViewHolder(v, this.reportActivity, this);
    }
    @Override
    public void onBindViewHolder(ReportAdapter.ReportViewHolder viewHolder, int i)
    {
        viewHolder.titulo.setText(items.get(i).getText());
        if ( items.get(i).getSeleted())
        {
            viewHolder.list_row.setBackgroundColor(Color.parseColor("#0063a7"));
            viewHolder.titulo.setTextColor(Color.parseColor("#ffffff"));
        }
        else
        {
            viewHolder.list_row.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.titulo.setTextColor(Color.parseColor("#000000"));
        }
    }
    public static class ReportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView titulo;
        public ReportActivity reportActivity;
        public ReportAdapter reportAdapter;
        public TextView textView = null;
        public RelativeLayout list_row;


        public ReportViewHolder(View v, ReportActivity reportActivity, ReportAdapter reportAdapter)
        {
            super(v);
            list_row = (RelativeLayout) v.findViewById(R.id.list_row);
            titulo = (TextView) v.findViewById(R.id.titulo);
            this.reportActivity = reportActivity;
            this.reportAdapter = reportAdapter;

            titulo.setOnClickListener(this);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view)
        {

            int position = getAdapterPosition();
            Report miReport = reportAdapter.items.get(getAdapterPosition());

            if (view.getId() == titulo.getId())
            {
                reportAdapter.setSelected(position);
                if( miReport.getId() == 12 )
                {
                    reportActivity.ChangeStatusViews( true , true , miReport.getId() );
                }
                else
                {
                    reportActivity.ChangeStatusViews( false , true , miReport.getId() );
                }
            }
        }
    }
}