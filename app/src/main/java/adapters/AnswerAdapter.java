package adapters;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import adparter_class.Answer;
import leyendasoy.leyenda.ChallengeQuestionActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 3/03/2017.
 */

public class AnswerAdapter extends  RecyclerView.Adapter<AnswerAdapter.ReportViewHolder>
{
    private List<Answer> items;
    ChallengeQuestionActivity challengeQuestionActivity;
    public AnswerAdapter(List<Answer> items , ChallengeQuestionActivity challengeQuestionActivity )
    {
        this.items = items;
        this.challengeQuestionActivity = challengeQuestionActivity;
    }
    public void setColor(int pos )
    {

        for( int i = 0 ; i < items.size() ; i++ )
        {
            if( items.get(i).getQuestionId() == items.get(pos).getQuestionId() )
            {
                items.get(i).setSelected(true);
            }
        }

        if( items.get(pos).isCorrect() )
        {
            items.get( pos ).setShowIcon(1);
            items.get( pos ).setColor("#8ba549");
        }
        else
        {
            items.get( pos ).setShowIcon(2);
            items.get( pos ).setColor("#b02228");
            for( int i = 0 ; i < items.size() ; i++ )
            {
                if (items.get(i).isCorrect())
                {
                    items.get(i).setColor("#8ba549");
                    items.get( i ).setShowIcon(1);
                }
            }
        }
        challengeQuestionActivity.SaveAnswer(   items.get(pos).getAnswerId() ,  items.get(pos).isCorrect()  );
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount()
    {
        return items.size();
    }
    public List<Answer> getList()
    {
        return this.items;
    }
    @Override
    public AnswerAdapter.ReportViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.base_list_answer, viewGroup, false);
        return new AnswerAdapter.ReportViewHolder(v,this,challengeQuestionActivity);
    }
    @Override
    public void onBindViewHolder(AnswerAdapter.ReportViewHolder viewHolder, int i)
    {
        viewHolder.titulo.setText( items.get(i).getStrAnswer() );
        viewHolder.list_row.setBackgroundColor( Color.parseColor( items.get(i).getColor() ) );

        if( items.get(i).getShowIcon() == 1 )
        {
            viewHolder.icon.getLayoutParams().width = 24;
            viewHolder.icon.getLayoutParams().height = 20;
            viewHolder.icon.setBackgroundResource(R.drawable.done);
            viewHolder.titulo.setTextColor(Color.WHITE);
        }
        else if( items.get(i).getShowIcon() == 2 )
        {
            viewHolder.icon.getLayoutParams().width = 22;
            viewHolder.icon.getLayoutParams().height = 20;
            viewHolder.icon.setBackgroundResource(R.drawable.fail);
            viewHolder.titulo.setTextColor(Color.WHITE);
        }
    }
    public static class ReportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView titulo;
        public ImageView icon;
        public RelativeLayout list_row;
        public AnswerAdapter AnswerAdapter;
        public ChallengeQuestionActivity challengeQuestionActivity;

        public ReportViewHolder(View v, AnswerAdapter AnswerAdapter , ChallengeQuestionActivity challengeQuestionActivity )
        {
            super(v);

            titulo = (TextView) v.findViewById(R.id.titulo);
            icon = (ImageView) v.findViewById(R.id.icon);
            list_row = (RelativeLayout) v.findViewById(R.id.list_row);
            this.AnswerAdapter = AnswerAdapter;
            this.challengeQuestionActivity = challengeQuestionActivity;

            list_row.setOnClickListener(this);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view)
        {
            Answer MiAnswer = AnswerAdapter.items.get(getAdapterPosition());


            if (view.getId() == list_row.getId())
            {
                if( MiAnswer.isSelected() == false )
                {
                    AnswerAdapter.setColor(getAdapterPosition());
                }
            }
        }
    }


}