package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import adparter_class.Chat;
import adparter_class.Followed;
import leyendasoy.leyenda.GetSocialActivity;
import leyendasoy.leyenda.R;

/**
 * Created by Nelson on 13/02/2017.
 */

public class SocialAdapter extends  RecyclerView.Adapter<SocialAdapter.FollowedViewHolder>  implements  Filterable
{
    private List<Followed> items;
    private  GetSocialActivity socialActivity;
    private ArrayList<Followed> itemsFilter;
    private CustomFilter mFilter;

    public SocialAdapter(List<Followed> items , GetSocialActivity parent )
    {
        this.socialActivity = parent;

        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(SocialAdapter.this);
    }
    public void clear()
    {
        items.clear();
        itemsFilter.clear();
    }
    public void addAll(List<Followed> list)
    {
        items.addAll(list);
        itemsFilter.addAll(list);
        this.notifyDataSetChanged();
    }

    public  void changeStatusItem( int id , boolean status )
    {
         for ( int i = 0 ; i < this.itemsFilter.size() ; i++ )
         {
             if( this.itemsFilter.get(i).getUserId() == id )
             {
                  this.itemsFilter.get(i).setIcon( status );
                  break;
             }
         }
    }


    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Followed> getList() {
        return this.itemsFilter;
    }
    @Override
    public FollowedViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.followed_list_base, viewGroup, false);
        return new FollowedViewHolder(v , this.socialActivity , this );
    }

    @Override
    public void onBindViewHolder(FollowedViewHolder viewHolder, int i)
    {
        viewHolder.icon.setTag(R.id.tag_user_id , itemsFilter.get(i).getUserId() );
        viewHolder.icon.setTag(R.id.tag_is_follow , itemsFilter.get(i).isIcon() );


        if( itemsFilter.get(i).isLegend() )
        {
            viewHolder.imagen.setImageResource(R.drawable.leyendas_del_servicio );
            viewHolder.titulo.setText(itemsFilter.get(i).getTitulo());
            viewHolder.correo.setText(itemsFilter.get(i).getCorreo());
            viewHolder.icon.setImageResource(R.drawable.follow_gray);
            viewHolder.icon.setTag(R.id.tag_is_legend , true );
        }
        else
        {
            RequestQueue mRequestQueue = Volley.newRequestQueue( socialActivity.getApplicationContext() );
            ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache()
            {
                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
                public void putBitmap(String url, Bitmap bitmap)
                {
                    mCache.put(url, bitmap);
                }
                public Bitmap getBitmap(String url)
                {
                    return mCache.get(url);
                }
            });

            if (itemsFilter.get(i).getImagen().length() > 0)
            {
                byte[] decodedString = Base64.decode(itemsFilter.get(i).getImagen(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                viewHolder.imagen.setImageBitmap(decodedByte);
            }
            else
            viewHolder.imagen.setImageResource(R.drawable.default_user);

            viewHolder.titulo.setText(itemsFilter.get(i).getTitulo());
            viewHolder.correo.setText(itemsFilter.get(i).getCorreo());
            if (itemsFilter.get(i).isIcon())
                viewHolder.icon.setImageResource(R.drawable.foolow_yes);
            else
                viewHolder.icon.setImageResource(R.drawable.foolow_no);

            viewHolder.icon.setTag(R.id.tag_is_legend , false );


        }

    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class FollowedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView imagen;
        public TextView titulo;
        public TextView correo;
        public ImageView icon;
        public GetSocialActivity socialActivity;
        private  SocialAdapter socialAdapter;


        public FollowedViewHolder(View v , GetSocialActivity socialActivity , SocialAdapter socialAdapter )
        {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.field_photo);
            titulo = (TextView) v.findViewById(R.id.fieldName);
            correo = (TextView) v.findViewById(R.id.fieldEmail);
            icon = (ImageView) v.findViewById(R.id.fieldFollow);

            this.socialActivity = socialActivity;
            this.socialAdapter = socialAdapter;

            icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            boolean isFollow = (boolean) view.getTag(R.id.tag_is_follow);
            boolean isLegend = (boolean) view.getTag(R.id.tag_is_legend);
            int  userIdFollow = (int) view.getTag(R.id.tag_user_id);

            if( view.getId() == icon.getId() )
            {
                if( isLegend == false )
                {
                    ImageView icon = (ImageView) view;
                    int position = getAdapterPosition();

                    if (isFollow == true)
                    {
                        icon.setImageResource(R.drawable.foolow_no);
                        icon.setTag(R.id.tag_is_follow, false);
                        isFollow = false;
                    }
                    else
                    {
                        icon.setImageResource(R.drawable.foolow_yes);
                        icon.setTag(R.id.tag_is_follow, true);
                        isFollow = true;
                    }
                    this.socialAdapter.changeStatusItem( userIdFollow , isFollow );


                    socialActivity.SaveUserFollow(userIdFollow, isFollow);
                }
            }
        }
    }

    public class CustomFilter extends Filter
    {
        private SocialAdapter listAdapter;

        private CustomFilter(SocialAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Followed person : items)
                {
                    if (person.getTitulo().toLowerCase().contains(filterPattern) || person.getCorreo().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(person);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}