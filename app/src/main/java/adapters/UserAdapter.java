package adapters;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adparter_class.User;
import clases.MiHelper;
import clases.ModalVote;
import clases.ModalVoteLegend;
import clases.Singleton;
import leyendasoy.leyenda.LegendActivity;
import leyendasoy.leyenda.ProfileActivity;
import leyendasoy.leyenda.R;

import static leyendasoy.leyenda.R.id.txtComments;

/**
 * Created by Nelson on 13/02/2017.
 */

public class UserAdapter extends  RecyclerView.Adapter<UserAdapter.FollowedViewHolder>  implements  Filterable
{
    private List<User> items;
    private  LegendActivity legendActivity;
    private ArrayList<User> itemsFilter;
    private CustomFilter mFilter;
    private Boolean IsAvailable;

    public UserAdapter(List<User> items , LegendActivity parent , Boolean isAvailable )
    {
        this.legendActivity = parent;
        IsAvailable = isAvailable;
        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(UserAdapter.this);
    }
    public  void changeStatusItem( String id , boolean status )
    {
        for ( int i = 0 ; i < this.itemsFilter.size() ; i++ )
        {
            if( this.itemsFilter.get(i).getUserId().compareToIgnoreCase(id) == 0 )
            {
                this.itemsFilter.get(i).setIcon( status );
                break;
            }
        }
    }
    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public void UpdateColorItem( int id )
    {
        for (int i = 0 ; i < itemsFilter.size() ; i++ )
        {
            if(  id == Integer.parseInt( itemsFilter.get(i).getUserId()) )
            {
                if ( itemsFilter.get(i).isVoted() == false )
                {
                    itemsFilter.get(i).setVoted(true);
                }
                else
                {
                    itemsFilter.get(i).setVoted(false);
                }
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<User> getList() {
        return this.itemsFilter;
    }
    @Override
    public FollowedViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.base_user, viewGroup, false);
        return new FollowedViewHolder(v , this.legendActivity , this );
    }

    @Override
    public void onBindViewHolder(FollowedViewHolder viewHolder, int i)
    {
        RequestQueue mRequestQueue = Volley.newRequestQueue( legendActivity.getApplicationContext() );
        ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache()
        {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap)
            {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url)
            {
                return mCache.get(url);
            }
        });

        if (itemsFilter.get(i).getImagen().length() > 0)
        {
            byte[] decodedString = Base64.decode(itemsFilter.get(i).getImagen(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.imagen.setImageBitmap(decodedByte);
        }
        else
            viewHolder.imagen.setImageResource(R.drawable.default_user);

        if( itemsFilter.get(i).isVoted() == true )
        {
            viewHolder.icon.setText("Ya has votado");
            viewHolder.icon.setBackgroundResource(R.drawable.bg_green);
            viewHolder.icon.setTextColor(Color.parseColor("#FFFFFF"));

        }
        else
        {
            viewHolder.icon.setText("Votar");
            viewHolder.icon.setBackgroundResource(R.drawable.border_green);
            viewHolder.icon.setTextColor(Color.parseColor("#00A396"));
        }

        viewHolder.titulo.setText(itemsFilter.get(i).getTitulo());
        viewHolder.correo.setText(itemsFilter.get(i).getCorreo());
    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class FollowedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView imagen;
        public TextView titulo;
        public TextView correo;
        public Button icon;
        public LegendActivity legendActivity;
        public UserAdapter userAdapter;

        public FollowedViewHolder(View v , LegendActivity legendActivity , UserAdapter userAdapter )
        {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.field_photo);
            titulo = (TextView) v.findViewById(R.id.fieldName);
            correo = (TextView) v.findViewById(R.id.fieldEmail);
            icon = (Button) v.findViewById(R.id.vote);

            this.legendActivity = legendActivity;
            this.userAdapter = userAdapter;

            imagen.setOnClickListener(this);
            icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            Boolean vote;
            User user = userAdapter.itemsFilter.get(getAdapterPosition());
            if ( view.getId() == R.id.vote )
            {
                if( userAdapter.IsAvailable )
                {
                    try
                    {
                        if( user.isVoted() == true )
                            vote = false;
                        else
                            vote = true;

                        int _iduser = Singleton.getInstance().getUserID();
                        GetUser( Integer.parseInt( user.getUserId() ) , _iduser , vote );
                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder( legendActivity );
                    builder.setTitle("Advertencia");
                    builder.setMessage("No es posible votar por cuarta vez.").setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                        }
                    }).show();
                }
            }
        }
        public void GetUser(  final int iduser ,  final int _iduser , final Boolean vote )
        {
            final MiHelper miHelper = new MiHelper();
            final ProgressDialog progressDialog = new ProgressDialog(legendActivity);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();

            final RequestQueue queue = Volley.newRequestQueue(legendActivity);
            Map<String, String> params = new HashMap<String, String>();
            params.put("UserId", String.valueOf(iduser) );
            params.put("Width", "800" );
            JSONObject jsonObj = new JSONObject(params);


            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser/", jsonObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        progressDialog.dismiss();
                        Log.i("Profile " , response.toString() );
                        boolean result = (boolean) response.getBoolean("Result");
                        if( result == true )
                        {
                            clases.User user = new clases.User( response.getInt("UserId") , response.getString("Name") , "", response.getString("Company") , response.getString("City") , response.getString("Email") , response.getInt("CompanyId") , response.getInt("CityId") , 1 , response.getString("Picture") );
                            ModalVoteLegend.newInstance( userAdapter ,  user , _iduser , vote ).show( legendActivity.getFragmentManager(), null);
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();
                    if (error.networkResponse == null)
                    {
                        if (error.getClass().equals(TimeoutError.class))
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(legendActivity);
                            builder.setTitle("Error en la conexión");
                            builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    GetUser( iduser , _iduser , vote );
                                }
                            }).show();
                        }
                    }
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                    return params;
                }
            };

            queue.add(getRequest);
        }
    }


    public class CustomFilter extends Filter
    {
        private UserAdapter listAdapter;

        private CustomFilter(UserAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final User person : items)
                {
                    if ( person.getTitulo().toLowerCase().contains(filterPattern) || person.getCorreo().toLowerCase().contains(filterPattern ) )
                    {
                        itemsFilter.add(person);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }
}