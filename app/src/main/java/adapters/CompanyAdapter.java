package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import adparter_class.Company;

/**
 * Created by Nelson on 6/02/2017.
 */

public class CompanyAdapter extends ArrayAdapter
{
    private Context context;
    private List<Company> values;

    public CompanyAdapter(Context context, int textViewResourceId, List<Company> values)
    {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }
    public int getCount()
    {
        return values.size();
    }

    public Company getItem(int position)
    {
        return values.get(position);
    }
    public List<Company> getItems()
    {
        return this.values;
    }

    public long getItemId(int position)
    {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView label = new TextView(context);
        label.setTextSize(10);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName() );

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        TextView label = new TextView(context);
        label.setPadding(10,20,5,20);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName());

        return label;
    }
}
