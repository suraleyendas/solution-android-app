package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adparter_class.Comment;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.WallActivity;

/**
 * Created by Nelson on 13/02/2017.
 */

public class CommentAdapter extends  RecyclerView.Adapter<CommentAdapter.CommentViewHolder>  implements  Filterable
{
    private List<Comment> items;
    private ArrayList<Comment> itemsFilter;
    private CustomFilter mFilter;
    public CommentAdapter(List<Comment> items )
    {

        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(CommentAdapter.this);
    }

    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Comment> getList() {
        return this.itemsFilter;
    }
    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comments_list_base, viewGroup, false);
        return new CommentViewHolder(v , this );
    }
    @Override
    public void onBindViewHolder(CommentViewHolder viewHolder, int i)
    {
        viewHolder.nameUser.setText(itemsFilter.get(i).getNameUser());
        viewHolder.textComment.setText(itemsFilter.get(i).getTextComment());

        if (itemsFilter.get(i).getPhotoUser().length() > 0)
        {
            byte[] decodedString = Base64.decode(itemsFilter.get(i).getPhotoUser(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.photoUser.setImageBitmap(decodedByte);
        }
        else
            viewHolder.photoUser.setImageResource(R.drawable.default_user);

    }
    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView photoUser;
        public TextView nameUser;
        public TextView textComment;




        public WallActivity wallActivity;
        public CommentAdapter commentAdapter;


        public CommentViewHolder(View v,  CommentAdapter commentAdapter)
        {
            super(v);
            photoUser = (ImageView) v.findViewById(R.id.photoUser);
            nameUser = (TextView) v.findViewById(R.id.nameUser);
            textComment = (TextView) v.findViewById(R.id.textComment);

            this.wallActivity = wallActivity;
            this.commentAdapter = commentAdapter;
        }
    }
    public class CustomFilter extends Filter
    {
        private CommentAdapter listAdapter;

        private CustomFilter(CommentAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Comment comment : items)
                {
                    if ( comment.getNameUser().toLowerCase().contains(filterPattern)  )
                    {
                        itemsFilter.add(comment);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}