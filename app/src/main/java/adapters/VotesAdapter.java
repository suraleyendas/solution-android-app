package adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import adparter_class.Followed;
import leyendasoy.leyenda.FollowersActivity;
import leyendasoy.leyenda.R;
import leyendasoy.leyenda.VotersByUserActivity;

/**
 * Created by Nelson on 16/02/2017.
 */

public class VotesAdapter extends  RecyclerView.Adapter<VotesAdapter.VotersByUserViewHolder>  implements  Filterable
{
    private List<Followed> items;
    private VotersByUserActivity votersByUserActivity;
    private ArrayList<Followed> itemsFilter;
    private CustomFilter mFilter;

    public VotesAdapter(List<Followed> items , VotersByUserActivity parent )
    {
        this.votersByUserActivity = parent;

        this.items = items;
        this.itemsFilter = new ArrayList<>();
        this.itemsFilter.addAll(items);
        this.mFilter = new CustomFilter(VotesAdapter.this);
    }
    public void clear()
    {
        items.clear();
        itemsFilter.clear();
    }
    public void addAll(List<Followed> list)
    {
        items.addAll(list);
        itemsFilter.addAll(list);
        this.notifyDataSetChanged();
    }
    public  void changeStatusItem( int id , boolean status )
    {
        for ( int i = 0 ; i < this.itemsFilter.size() ; i++ )
        {
            if( this.itemsFilter.get(i).getUserId() == id )
            {
                this.itemsFilter.get(i).setIcon( status );
                break;
            }
        }
    }
    @Override
    public int getItemCount()
    {
        return itemsFilter.size();
    }

    public List<Followed> getList() {
        return this.itemsFilter;
    }
    @Override
    public VotersByUserViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.followed_list_base, viewGroup, false);
        return new VotersByUserViewHolder(v , this.votersByUserActivity , this );
    }
    @Override
    public void onBindViewHolder(VotersByUserViewHolder viewHolder, int i)
    {
        if (itemsFilter.get(i).getImagen().length() > 0)
        {
            byte[] decodedString = Base64.decode(itemsFilter.get(i).getImagen(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            viewHolder.imagen.setImageBitmap(decodedByte);
        }
        else
            viewHolder.imagen.setImageResource(R.drawable.default_user);


        viewHolder.titulo.setText( itemsFilter.get(i).getTitulo() );
        viewHolder.correo.setText( itemsFilter.get(i).getCorreo() );
    }

    @Override
    public Filter getFilter()
    {
        return mFilter;
    }

    public static class VotersByUserViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imagen;
        public TextView titulo;
        public TextView correo;
        public ImageView icon;
        public VotersByUserActivity votersByUserActivity;
        public VotesAdapter votesAdapter;

        public VotersByUserViewHolder(View v , VotersByUserActivity votersByUserActivity , VotesAdapter votesAdapter )
        {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.field_photo);
            titulo = (TextView) v.findViewById(R.id.fieldName);
            correo = (TextView) v.findViewById(R.id.fieldEmail);
            icon = (ImageView) v.findViewById(R.id.fieldFollow);

            this.votersByUserActivity = votersByUserActivity;
            this.votesAdapter = votesAdapter;
        }
    }

    public class CustomFilter extends Filter
    {
        private VotesAdapter listAdapter;

        private CustomFilter(VotesAdapter listAdapter)
        {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            itemsFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0)
            {
                itemsFilter.addAll(items);
            }
            else
            {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Followed person : items)
                {
                    if (person.getTitulo().toLowerCase().contains(filterPattern))
                    {
                        itemsFilter.add(person);
                    }
                }
            }
            results.values = itemsFilter;
            results.count = itemsFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            this.listAdapter.notifyDataSetChanged();
        }
    }


}