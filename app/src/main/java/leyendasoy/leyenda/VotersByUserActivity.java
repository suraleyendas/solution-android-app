package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.FollowedAdapter;
import adapters.VotesAdapter;
import adparter_class.Followed;
import clases.MiHelper;
import clases.Singleton;
import clases.Util;

public class VotersByUserActivity extends AppCompatActivity implements View.OnClickListener
{
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    ProgressDialog progressDialog;
    List<Followed> items;
    Util util;
    Context miContex;
    VotersByUserActivity ref;
    ArrayList<Followed> follows;
    LinearLayout tab_inicio , tab_leyenda , tab_retos ;

    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    VotesAdapter listAdapter = null;

    String _nombre , _foto , _email , _city , _company ;
    int _iduser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voters_by_user);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );


        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        miContex = this;
        ref = this;
        util = new Util();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        try
        {
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        TextView titulo = (TextView) findViewById(R.id.titulo);
        Typeface dinoBold = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");
        titulo.setTypeface(dinoBold);

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources( R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    getVotes( page , false );
                /*
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       // setupAdapter();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
                */
            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                getVotes( page , true );
                        }
                    }
                }
            }
        });

        if (util.compruebaConexion(this))
        {
            getVotes( page , false );
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error en la conexión");
            builder.setMessage("Error no tienes conexión a internet.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    getVotes( page , false  );
                }
            });
            builder.show();
        }
    }
    public void getVotes( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();
        progressDialog = new ProgressDialog(VotersByUserActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("Width", "50");
        params.put("Height", "50");
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", "5");
        params.put("SearchQuery", "");


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUserVotersByUserId", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    loading = false;
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }

                        JSONArray users = response.getJSONArray("ArrayUserVoters");
                        for (int i = 0; i < users.length(); i++)
                        {
                            JSONObject user = users.getJSONObject(i);
                            items.add(new Followed( user.getInt("UserId") , user.getString("Picture"), user.getString("Email"), user.getString("Name"), false , false ));
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new VotesAdapter(items, ref);
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Followed> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }
                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                getVotes( page , add );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    Intent intent = new Intent(VotersByUserActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;
                case R.id.message:
                    intent = new Intent(VotersByUserActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;
            }
        }
        return (super.onOptionsItemSelected(item));
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.tab_inicio )
        {
            Intent intent = new Intent(VotersByUserActivity.this, WallActivity.class);

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(VotersByUserActivity.this, LegendActivity.class);

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(VotersByUserActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
    }
}