package leyendasoy.leyenda;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import adapters.CityAdapter;
import adapters.CompanyAdapter;
import adapters.DomainAdapter;
import adparter_class.City;
import adparter_class.Company;
import clases.CustomVolleyRequest;
import clases.DbHelper;
import adparter_class.Domain;
import clases.MiHelper;
import clases.Singleton;
import clases.User;

public class EditprofileActivity extends Activity implements View.OnClickListener
{
    Map<String,String> params;
    RequestQueue queue;
    JSONObject jsonObj;
    JsonObjectRequest request;
    ProgressDialog progressDialog;
    DbHelper dbHelper;
    MiHelper miHelper;
    ImageView photoProfile;
    int peticiones = 0;
    TextView txtNombre , txtEmail;
    Spinner spinner1 , spinner2 , spinner3;
    String imageString;
    ImageButton editarFoto;

    private Uri mCropImageUri;

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;

    private Button btnUpdate;
    private  String  urlFoto , role , _summary;
    private String mPath;
    private int _iduser = 0;
    Context miContext;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        this.miContext = miContext;

        dbHelper = new DbHelper(getApplicationContext());
        //getApplicationContext().deleteDatabase( dbHelper.DATABASE_NAME );


        TextView titulo1 = (TextView) findViewById(R.id.titulo1);
        TextView titulo2 = (TextView) findViewById(R.id.titulo2);

        Typeface dinoLight = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Light 2.otf");
        Typeface dinoBold = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");

        titulo1.setTypeface(dinoLight);
        titulo2.setTypeface(dinoBold);

        photoProfile = ( ImageView ) findViewById(R.id.photoProfile);
        editarFoto = ( ImageButton ) findViewById(R.id.editarFoto);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtEmail = (TextView) findViewById(R.id.txtEmail);

        editarFoto.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            if (extras.containsKey("UserID"))
            {
                _iduser = extras.getInt("UserID");
            }
        }
        if( _iduser == 0 )
        {
            try
            {
                _iduser = Singleton.getInstance().getUserID();
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }

        role = (String) extras.get("role");
        btnUpdate.setText( extras.get("textoBtn").toString() );

        miHelper = new MiHelper();
        progressDialog = new ProgressDialog(EditprofileActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);


        spinner1 = ( Spinner ) findViewById(R.id.spinner1);
        spinner2 = ( Spinner ) findViewById(R.id.spinner2);
        spinner3 = ( Spinner ) findViewById(R.id.spinner3);

        progressDialog.show();


        queue = Volley.newRequestQueue(EditprofileActivity.this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("Width", "200" );
        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "/UsersAPI/GetUser/", jsonObj, new Response.Listener<JSONObject>()
        {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            txtNombre.setText( response.getString("Name").toString() );
                            _summary = response.getString("Summary");
                            String email = response.getString("Email");
                            String[] separated = email.split("@");
                            email = separated[0];
                            txtEmail.setText( email );

                            byte[] decodedString = Base64.decode(response.getString("Picture"), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            photoProfile.setImageBitmap(decodedByte);

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        peticiones++;
                        if( peticiones == 4 )
                            if( progressDialog != null )
                            progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.toString() );
                        progressDialog.dismiss();
                    }
                })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                        return params;
                    }
                };
        queue.add(getRequest);


        getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "/CityAPI", null, new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        List<City> citiesList = new ArrayList<>();
                        try
                        {
                            Boolean res = response.getBoolean("Result");
                            if( res )
                            {
                                JSONArray cities = response.getJSONArray("arrayCities");
                                if( cities.length() > 0  )
                                {
                                    citiesList.add( new City( -1 , "Seleccione una ciudad" ));
                                    for( int i = 0 ; i < cities.length() ; i++ )
                                    {
                                        JSONObject city = cities.getJSONObject(i);
                                        citiesList.add( new City( city.getInt("Id") , city.getString("Name") ));
                                    }

                                    CityAdapter spinner_adapter = new CityAdapter( getBaseContext() ,android.R.layout.simple_spinner_item , citiesList );
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinner1.setAdapter(spinner_adapter);


                                    CityAdapter adapter = (CityAdapter) spinner1.getAdapter();
                                    List<City> lista = adapter.getItems();

                                    Cursor cursor = dbHelper.getUser();
                                    if(cursor != null)
                                    {
                                        if (cursor.moveToFirst())
                                        {
                                            for (int position = 0; position < lista.size(); position++)
                                            {
                                                if(   lista.get(position).getId() == Integer.parseInt(cursor.getString(7)) )
                                                {
                                                    spinner1.setSelection(position);
                                                }

                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    citiesList.add( new City( -1 , "Seleccione una ciudad" ));
                                    CityAdapter spinner_adapter = new CityAdapter( getBaseContext() ,android.R.layout.simple_spinner_item , citiesList );
                                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinner1.setAdapter(spinner_adapter);
                                }

                            }
                            else
                            {
                                citiesList.add( new City( -1 , "Seleccione una ciudad" ));
                                CityAdapter spinner_adapter = new CityAdapter( getBaseContext() ,android.R.layout.simple_spinner_item , citiesList );
                                spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner1.setAdapter(spinner_adapter);
                            }

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                        peticiones++;
                        if( peticiones == 4 )
                            progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.toString() );
                        progressDialog.dismiss();
                    }
                })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                        return params;
                    }
                };
        queue.add(getRequest);


        getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "/CompanyAPI/GetCompanies", null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        List<Company> companiesList = new ArrayList<>();
                        try
                        {
                            Log.i("Companies :  " ,response.toString() );
                            Boolean res = response.getBoolean("Result");

                            if( res )
                            {
                                    JSONArray companies = response.getJSONArray("arrayCompanies");
                                    if( companies.length() > 0 )
                                    {
                                        companiesList.add( new Company ( -1 , "Seleccione una compañía" ) );
                                        for( int i = 0 ; i < companies.length() ; i++ )
                                        {
                                            JSONObject company = companies.getJSONObject(i);
                                            companiesList.add( new Company ( company.getInt("Id") , company.getString("Name") ) );
                                        }

                                        CompanyAdapter dataAdapter = new CompanyAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, companiesList );
                                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinner2.setAdapter(dataAdapter);


                                        CompanyAdapter adapter = (CompanyAdapter) spinner2.getAdapter();
                                        List<Company> lista = adapter.getItems();
                                        Cursor cursor = dbHelper.getUser();
                                        if(cursor != null)
                                        {
                                            if (cursor.moveToFirst())
                                            {
                                                for (int position = 0; position < lista.size(); position++)
                                                {
                                                    Log.i("Coma" , lista.get(position).getId()+ "**"+Integer.parseInt(cursor.getString(8)));
                                                    if(   lista.get(position).getId() == Integer.parseInt(cursor.getString(8)) )
                                                    {
                                                        spinner2.setSelection(position);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        companiesList.add( new Company ( -1 , "Seleccione una compañia" ) );
                                        CompanyAdapter dataAdapter = new CompanyAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, companiesList );
                                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinner2.setAdapter(dataAdapter);
                                    }
                            }
                            else
                            {
                                companiesList.add( new Company ( -1 , "Seleccione una compañia" ) );
                                CompanyAdapter dataAdapter = new CompanyAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, companiesList );
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner2.setAdapter(dataAdapter);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        peticiones++;
                        if( peticiones == 4 )
                            progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.toString() );
                        progressDialog.dismiss();
                    }
                })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                        return params;
                    }
                };
        queue.add(getRequest);


        getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() +"/DomainAPI/1", null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.i("Domain",response.toString() );
                        List<Domain> list = new ArrayList<>();

                        try
                        {
                            JSONArray domains = response.getJSONArray("arrayDomains");
                            Boolean res = response.getBoolean("Result");

                            if( res )
                            {
                                if( domains.length() > 0 )
                                {
                                    list.add( new Domain( -1 , "Seleccione un dominio" ) );
                                    for( int i = 0 ; i < domains.length() ; i++ )
                                    {
                                        JSONObject domain = domains.getJSONObject(i);
                                        list.add( new Domain( domain.getInt("Id") , domain.getString("DomainName")) );
                                    }
                                    DomainAdapter dataAdapter = new DomainAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, list);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinner3.setAdapter(dataAdapter);


                                    DomainAdapter adapter = (DomainAdapter) spinner3.getAdapter();
                                    List<Domain> lista = adapter.getItems();
                                    Cursor cursor = dbHelper.getUser();
                                    if(cursor != null)
                                    {
                                        if (cursor.moveToFirst())
                                        {
                                            for (int position = 0; position < lista.size(); position++)
                                            {
                                                if(   lista.get(position).getId() == Integer.parseInt(cursor.getString(9)) )
                                                {
                                                    spinner3.setSelection(position);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    list.add( new Domain( -1 , "Seleccione un dominio" ) );

                                    DomainAdapter dataAdapter = new DomainAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, list);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinner3.setAdapter(dataAdapter);
                                }

                            }
                            else
                            {
                                list.add( new Domain( -1 , "Seleccione un dominio" ) );

                                DomainAdapter dataAdapter = new DomainAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, list);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner3.setAdapter(dataAdapter);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                        peticiones++;
                        if( peticiones == 4 )
                            progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.toString() );
                        progressDialog.dismiss();
                    }
                })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                        return params;
                    }
                };
        queue.add(getRequest);

    }
    public void onSelectImageClick(View view)
    {
        CropImage.startPickImageActivity(this);
    }

    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.editarFoto )
        {
            onSelectImageClick(v);
        }
        else if( v.getId() == R.id.btnUpdate )
        {
            Pattern pat = Pattern.compile("[A-Za-z1-9_.-]*");
            Matcher mat = pat.matcher( txtEmail.getText() );
            if( txtEmail.getText().length() <= 0 )
            {
                txtEmail.setError("Ingrese un correo valido");
            }
            else
            {
                if ( mat.matches() )
                {
                    final City obj_city = (City) ((Spinner) findViewById(R.id.spinner1)).getSelectedItem();
                    final Company company = (Company) ((Spinner) findViewById(R.id.spinner2)).getSelectedItem();
                    final Domain domain = (Domain) ((Spinner) findViewById(R.id.spinner3)).getSelectedItem();

                    boolean error = false;
                    String msg = "";
                    if (obj_city.getId() <= 0)
                    {

                        msg += "Seleccione una ciudad\n";
                        error = true;
                    }
                    if (company.getId() <= 0)
                    {
                        msg += "Seleccione una compañia\n";
                        error = true;
                    }
                    if (domain.getId() <= 0)
                    {
                        msg += "Seleccione una dominio\n";
                        error = true;
                    }
                    if( error == true )
                        showMsgError("Erros " , msg);
                    else
                    {
                        params = new HashMap<String, String>();

                        params.put("UserId", String.valueOf(_iduser));
                        params.put("CityId", String.valueOf(obj_city.getId()));
                        params.put("CompanyId", String.valueOf(company.getId()));
                        params.put("Email", txtEmail.getText().toString() + "" + domain.getDomain());
                        params.put("Picture", this.imageString);

                        queue = Volley.newRequestQueue(this);
                        jsonObj = new JSONObject(params);


                        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/UpdateUser", jsonObj, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response)
                            {
                                progressDialog.dismiss();

                                User user = new User( _iduser , txtNombre.getText().toString(), role, company.getName(), obj_city.getName(), txtEmail.getText() + "" + domain.getDomain(), company.getId(), obj_city.getId(), domain.getId() , imageString );
                                dbHelper = new DbHelper(getApplicationContext());
                                Cursor cursor = dbHelper.getUser();
                                if (cursor != null)
                                {
                                    if (cursor.getCount() <= 0)
                                    {
                                        Log.i("Create 1 " ,"Create1 ");
                                        //Create
                                        dbHelper.saveUser(user, getApplicationContext(), true, 0);
                                    }
                                    else
                                    {
                                        Log.i("Update 1 " ,"Update1 ");
                                        //Update
                                        cursor.moveToFirst();
                                        dbHelper.saveUser(user, getApplicationContext(), false, Integer.parseInt(cursor.getString(0)));
                                    }
                                }
                                else
                                {
                                    //Create
                                    dbHelper.saveUser(user, getApplicationContext(), true, 0);
                                }
                                cursor.close();

                                Intent intent = new Intent(EditprofileActivity.this, GetSocialActivity.class);
                                startActivity(intent);


                            }
                        }, new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                progressDialog.dismiss();
                            }
                        })
                        {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError
                            {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                                return params;
                            }
                        };

                        queue.add(getRequest);
                    }

                }
                else
                {
                    txtEmail.setError("Ingrese un correo valido");
                    progressDialog.dismiss();
                }
            }
        }
    }
    public void showMsgError( String err , String text )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle( err );
        builder.setMessage(text).setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
            }
        }).show();
    }
    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK)
        {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri))
            {
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }
            else
            {
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity|
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK)
            {
                Uri path = result.getUri();
                try
                {
                    photoProfile.setImageURI(result.getUri());

                    ExifInterface ei = new ExifInterface(path.getPath());
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);


                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), path);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
                    byte[] b = baos.toByteArray();
                    imageString = Base64.encodeToString(b, Base64.DEFAULT);

                    imageString = imageString.replace(" ","");
                    imageString = imageString.replace("\\s","");


                    Log.i("StringImg" , imageString );
                    //photoProfile.setImageBitmap(bitmap);

                }
                catch (Exception e)
                {
                    Toast.makeText(this, "" + e.getMessage() , Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                //Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        }
        else
        {
            //Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }
    private void startCropImageActivity(Uri imageUri)
    {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).start(this);
    }
    private Bitmap getBitmapFromUri ( Uri uri ) throws IOException
    {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver (). openFileDescriptor ( uri , "r" );
        FileDescriptor fileDescriptor = parcelFileDescriptor . getFileDescriptor ();
        Bitmap image = BitmapFactory . decodeFileDescriptor ( fileDescriptor );
        parcelFileDescriptor . close ();
        return image ;
    }
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation)
    {
          Matrix matrix = new Matrix();
            switch (orientation)
            {
                case ExifInterface.ORIENTATION_NORMAL:
                    return bitmap;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            try
            {
                Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return bmRotated;
            }
            catch (OutOfMemoryError e)
            {
                e.printStackTrace();
                return null;
            }
    }
    public Bitmap Redimensionar(Bitmap mBitmap, int newWidth)
    {
        float aspectRatio = mBitmap.getWidth() / (float) mBitmap.getHeight();
        int width = newWidth;
        int height = Math.round(width / aspectRatio);

        mBitmap = Bitmap.createScaledBitmap( mBitmap, width, height, false);

        return  mBitmap;
    }

}
