package leyendasoy.leyenda;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.CompanyAdapter;
import adapters.UserAdapter;
import adparter_class.Company;
import adparter_class.Followed;
import adparter_class.User;
import clases.MiHelper;
import clases.ModalVote;
import clases.Singleton;
import clases.SliderLegend;
import clases.VotersCompany;

public class LegendActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ProgressDialog progressDialog = null;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout ,  linearGraphics;
    LinearLayout tab_inicio , tab_leyenda , tab_retos;
    String CompanyID = "";

    Spinner companies;

    private int totalRequest = 0;

    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    LegendActivity ref;
    Context miContex;

    TextView titulo1 , titulo2;
    List<SliderLegend> sliderLegends;
    List<VotersCompany> votersCompanies;

    private int[] layouts;
    private TextView[] dots;
    LinearLayout expand_collapse;
    ExpandableLayout layoutDescription;
    ImageView icon_expand_collapse;

    EditText txtSearch;

    String _nombre , _foto , _email , _city , _company;
    int _iduser;
    Boolean IsAvailable;
    int peticiones = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legend);

        ref = this;
        miContex = this;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        recycler = (RecyclerView) findViewById(R.id.recycler);
        txtSearch = (EditText) findViewById(R.id.txtSearch);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        companies = ( Spinner ) findViewById( R.id.companies );

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        layoutDescription = (ExpandableLayout) findViewById(R.id.layoutDescription);
        expand_collapse = ( LinearLayout ) findViewById(R.id.expand_collapse);
        icon_expand_collapse = ( ImageView ) findViewById(R.id.icon_expand_collapse);


        Typeface dinoLight = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Light 2.otf");
        Typeface dinoBold = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");

        titulo1 = ( TextView ) findViewById(R.id.titulo1);
        titulo2 = ( TextView ) findViewById(R.id.titulo2);

        titulo1.setTypeface(dinoLight);
        titulo2.setTypeface(dinoBold);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        linearGraphics = ( LinearLayout)  findViewById(R.id.linearGraphics);

        expand_collapse.setOnClickListener(this);


        companies.setOnItemSelectedListener(this);

        GetAvailableUserVote();
    }
    private void addBottomDots(int currentPage)
    {
        dots = new TextView[layouts.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++)
        {

            dots[i] = new TextView(this);
            dots[i].setHeight(24);
            dots[i].setWidth(24);
            LinearLayout.LayoutParams layoutMargins = new LinearLayout.LayoutParams(ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.WRAP_CONTENT);
            layoutMargins.setMargins(5, 0, 5, 0);
            dots[i].setLayoutParams(layoutMargins);
            dots[i].setBackgroundResource(R.drawable.border_item_slide_legend_inactive);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setBackgroundResource(R.drawable.border_item_slide_legend_active);
    }
    private int getItem(int i)
    {
        return viewPager.getCurrentItem() + i;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    Intent intent = new Intent(LegendActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);
                    startActivity(intent);
                break;
                case R.id.message:
                    intent = new Intent(LegendActivity.this, MessageActivity.class);
                    startActivity(intent);
                break;
                case R.id.notification:
                    intent = new Intent(LegendActivity.this, NotificationActivity.class);
                    startActivity(intent);
                break;
            }
        }
        return(super.onOptionsItemSelected(item));
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.expand_collapse )
        {
            /*
                RotateAnimation r = new RotateAnimation(0, 180,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                r.setDuration(500);
                r.setRepeatCount(0);
                r.setFillAfter(true);
                icon_expand_collapse.startAnimation(r);
            */

            if (layoutDescription.isExpanded())
            {
                layoutDescription.collapse();
                Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_expand);
                icon_expand_collapse.startAnimation(animation);
            }
            else
            {
                layoutDescription.expand();
                Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_collapse);
                icon_expand_collapse.startAnimation(animation);
            }
        }
        else if( view.getId() == R.id.tab_inicio )
        {
            Intent intent = new Intent(LegendActivity.this, WallActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(LegendActivity.this, LegendActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(LegendActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
    }
    public  int currentPosition;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if( position > 0  )
        {
            Company company = (Company) ((Spinner) findViewById(R.id.companies)).getSelectedItem();
            CompanyID = String.valueOf(company.getId());
        }
        else
            CompanyID = "";

        GetAllSocialUser();
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
    }

    public class MyViewPagerAdapter extends PagerAdapter implements View.OnClickListener
    {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter()
        {
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            //Typeface customFontArial = Typeface.createFromAsset( getAssets() , "fonts/Arial.ttf");

            TextView txtNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView txtFecha = (TextView) view.findViewById(R.id.txtFecha);
            TextView txtCorreo = (TextView) view.findViewById(R.id.txtCorreo);
            TextView txtEmpresa = (TextView) view.findViewById(R.id.txtEmpresa);
            ImageView photoUser = ( ImageView ) view.findViewById(R.id.photoUser);
            LinearLayout linearVote = ( LinearLayout ) view.findViewById(R.id.linearVote);

            linearVote.setOnClickListener(this);

            txtNombre.setText( sliderLegends.get(position).getNameUser() );
            txtFecha.setText( sliderLegends.get(position).getDate() );
            txtCorreo.setText( sliderLegends.get(position).getEmail() );
            txtEmpresa.setText( sliderLegends.get(position).getCity() );

            if( sliderLegends.get(position).getPhotoUser().length() > 0 )
            {
                photoUser.setBackgroundResource(0);
                byte[] decodedString = Base64.decode(sliderLegends.get(position).getPhotoUser(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                photoUser.setImageBitmap(decodedByte);
            }

            container.addView(view);

            return view;
        }

        @Override
        public int getCount()
        {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }
        @Override
        public void onClick(View v) 
        {
            if( v.getId() == R.id.linearVote )
            {
                if( sliderLegends.get(currentPosition).getVoted() )
                {
                    v.setBackgroundResource(R.drawable.border_white);
                    sliderLegends.get(currentPosition).setVoted( false );
                    SaveUserFollow( sliderLegends.get(currentPosition).getUserId() , false );
                }
                else
                {
                    v.setBackgroundResource(R.drawable.bg_green);
                    SaveUserFollow( sliderLegends.get(currentPosition).getUserId() , true );
                    sliderLegends.get(currentPosition).setVoted( true );
                }
            }
        }
    }
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position)
        {
            currentPosition = position;
            addBottomDots(position);

            if (position == layouts.length - 1)
            {
            }
            else
            {

            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2)
        {

        }

        @Override
        public void onPageScrollStateChanged(int arg0)
        {

        }
    };
    public void GetLegendsOfTheService()
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "ContentAPI/GetLegendsOfTheService/" +_iduser , null , new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    peticiones++;
                    if( peticiones == 4 )
                        progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        JSONArray ArrayLegends = response.getJSONArray("ArrayLegends");
                        sliderLegends = new ArrayList<>();
                        if( ArrayLegends.length() > 0 )
                        {
                            for (int i = 0 ; i < ArrayLegends.length() ; i++ )
                            {
                                JSONObject data = ArrayLegends.getJSONObject(i);
                                sliderLegends.add( new SliderLegend( data.getString("UserId") , data.getString("Picture"), data.getString("Name"),"trimestre " + data.getString("Trimester") + "/" + data.getString("Year"),data.getString("Mail"),data.getString("Company") + " - " + data.getString("City") , data.getBoolean("IsFollow") ));
                            }

                            layouts = new int[sliderLegends.size()];

                            for ( int i = 0 ; i < sliderLegends.size() ; i++ )
                            {
                                layouts[i] = R.layout.layout_slider_legend;
                            }

                            dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
                            dotsLayout.setGravity(Gravity.CENTER);
                            addBottomDots(0);

                            myViewPagerAdapter = new MyViewPagerAdapter();
                            viewPager.setAdapter(myViewPagerAdapter);
                            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
                        }
                    }
                    GetCompanyGraphicVotes();
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetLegendsOfTheService();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetCompanyGraphicVotes()
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "CompanyAPI/GetCompanyGraphicVotes", null , new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    totalRequest++;
                    if( totalRequest == 3 )
                        progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        int total = 0;
                        JSONArray arrayCompaniesGraphisc = response.getJSONArray("arrayCompaniesGraphisc");
                        votersCompanies = new ArrayList<>();
                        for (int i = 0 ; i < arrayCompaniesGraphisc.length() ; i++ )
                        {
                            JSONObject data = arrayCompaniesGraphisc.getJSONObject(i);
                            total += data.getInt("CountVotes");
                            votersCompanies.add( new VotersCompany( data.getString("Name") , data.getInt("CountVotes")  , data.getString("HexColor")) );

                        }
                        linearGraphics.setWeightSum( votersCompanies.size() );


                        for( int i = 0 ; i < votersCompanies.size() ; i++ )
                        {
                            LinearLayout bar = new LinearLayout(getApplicationContext());
                            bar.setGravity( Gravity.BOTTOM );
                            bar.setOrientation(LinearLayout.VERTICAL);

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, ViewPager.LayoutParams.MATCH_PARENT ,1);
                            params.setMargins(2,0,2,0);
                            bar.setLayoutParams( params );


                            LinearLayout.LayoutParams paramsProgress = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT ,1);
                            final ProgressBar progressBar= new ProgressBar(getApplicationContext(),null,android.R.attr.progressBarStyleHorizontal);
                            progressBar.setProgressDrawable(getDrawable(R.drawable.verticalprogressbar));
                            progressBar.setLayoutParams(paramsProgress);
                            progressBar.setMax( total );

                            progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor( votersCompanies.get(i).getColor())));

                            bar.addView(progressBar);
                            if( votersCompanies.get(i).getValue() > 0 )
                            {

                                final float startSize = 0;
                                final float endSize = votersCompanies.get(i).getValue();
                                final int animationDuration = 1000;

                                ValueAnimator animator = ValueAnimator.ofFloat(startSize, endSize);
                                animator.setDuration(animationDuration);

                                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                    @Override
                                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                        float animatedValue = (float) valueAnimator.getAnimatedValue();
                                        progressBar.setProgress((int) animatedValue);
                                    }
                                });

                                animator.start();
                            }


                            TextView nameCompany = new TextView( getApplicationContext() );

                            nameCompany.setText( votersCompanies.get(i).getCompany() );
                            nameCompany.setTextSize(6);
                            nameCompany.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
                            nameCompany.setTextColor(Color.BLACK);
                            nameCompany.setHeight(50);

                            bar.addView(nameCompany);

                            linearGraphics.addView( bar );
                        }
                    }
                    GetCompanies();

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetCompanyGraphicVotes();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetCompanies()
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "CompanyAPI/GetCompanies", null , new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    peticiones++;
                    if( peticiones == 4 )
                        progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        int total = 0;
                        JSONArray arrayCompaniesGraphisc = response.getJSONArray("arrayCompanies");
                        List<Company> companiesList = new ArrayList<>();
                        if( arrayCompaniesGraphisc.length() > 0 )
                        {
                            companiesList.add( new Company ( -1 , "Seleccione una compañia" ) );
                            for( int i = 0 ; i < arrayCompaniesGraphisc.length() ; i++ )
                            {
                                JSONObject company = arrayCompaniesGraphisc.getJSONObject(i);
                                companiesList.add( new Company ( company.getInt("Id") , company.getString("Name") ) );
                            }
                            CompanyAdapter dataAdapter = new CompanyAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, companiesList );
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            companies.setAdapter(dataAdapter);
                        }
                        else
                        {
                            companiesList.add( new Company ( -1 , "Seleccione una compañia" ) );
                            CompanyAdapter dataAdapter = new CompanyAdapter( getBaseContext() ,android.R.layout.simple_spinner_item, companiesList );
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            companies.setAdapter(dataAdapter);
                        }
                    }
                    GetAllSocialUser();
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetAllSocialUser();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetAllSocialUser()
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser));
        params.put("CompanyId", CompanyID );
        params.put("SearchQuery", "");
        params.put("Width", "50");
        params.put("Height", "50");
        params.put("PageIndex", "0");
        params.put("PageSize", "10");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetAllSocialUser", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    peticiones++;
                    if( peticiones == 4 )
                        progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    List<User> items = new ArrayList<>();

                    if (result == true)
                    {
                        JSONArray users = response.getJSONArray("UserArray");

                        for (int i = 0; i < users.length(); i++)
                        {
                            JSONObject user = users.getJSONObject(i);
                            items.add(new User( user.getString("UserId") , user.getString("Picture"), user.getString("Email"), user.getString("Name"), user.getBoolean("IsFollow") , user.getBoolean("IsVoted") , user.getInt("CountVotes") , false ));
                        }
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ref);
                    recycler.setLayoutManager(linearLayoutManager);
                    final UserAdapter listAdapter = new UserAdapter(items,ref,IsAvailable);
                    recycler.setAdapter(listAdapter);


                    txtSearch.addTextChangedListener(new TextWatcher()
                    {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            listAdapter.getFilter().filter(s.toString());
                        }
                        @Override
                        public void afterTextChanged(Editable s)
                        {
                        }
                    });
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetAllSocialUser();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public  void SaveUserFollow( final String userIdFollow ,final boolean isFollow )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser));
        params.put("UserFollowId", userIdFollow );
        params.put("IsFollow", String.valueOf(isFollow) );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/SaveUserFollow", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                SaveUserFollow( userIdFollow , isFollow);
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetAvailableUserVote()
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(LegendActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }

        final RequestQueue queue = Volley.newRequestQueue( this );


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "UsersAPI/GetAvailableUserVote/" + _iduser , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    peticiones++;
                    if( peticiones == 4 )
                       progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    Log.i("GetAvailableUserVote" ,  response.toString() );
                    IsAvailable = result;
                    GetLegendsOfTheService();

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContex );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetAvailableUserVote();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }



}
