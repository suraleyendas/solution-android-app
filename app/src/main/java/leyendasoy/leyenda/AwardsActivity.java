package leyendasoy.leyenda;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.AwardAdapter;
import adapters.WallAdapter;
import adparter_class.Award;
import adparter_class.Comment;
import adparter_class.Wall;
import clases.MiHelper;
import clases.Singleton;

public class AwardsActivity extends AppCompatActivity implements View.OnClickListener
{
    private RecyclerView recycler;
    List<Award> items;
    String _nombre , _foto , _email , _city , _company;
    int _iduser;
    ProgressDialog progressDialog = null;
    Context miContex;
    AwardsActivity ref;
    LinearLayout tab_inicio , tab_leyenda , tab_retos;
    boolean loading = false;
    ImageView photoProfile;
    AwardAdapter listAdapter;
    int Points = 0;

    TextView txtPoint , txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_awards);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        miContex = this;
        ref = this;

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
             _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company  = Singleton.getInstance().getCompany();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        txtPoint = ( TextView ) findViewById(R.id.txtPoints);
        txtName = ( TextView ) findViewById(R.id.txtName);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        photoProfile = (ImageView) findViewById(R.id.photoProfile);

        recycler = ( RecyclerView ) findViewById(R.id.recycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        GetUser( _iduser );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(AwardsActivity.this, ProfileActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(AwardsActivity.this, MessageActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));

    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.layoutExpress )
        {
            Intent intent = new Intent(AwardsActivity.this, ExpressYourselfActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("city", _city );
            intent.putExtra("company", _company );
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutPhoto )
        {
            Intent intent = new Intent(AwardsActivity.this, MediaWallActivity.class);

            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("city", _city );
            intent.putExtra("company", _company );
            intent.putExtra("type", "1" );
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutVideo )
        {
            Intent intent = new Intent(AwardsActivity.this, MediaWallActivity.class);

            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("city", _city );
            intent.putExtra("company", _company );
            intent.putExtra("type", "2" );
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutEvent )
        {
            Intent intent = new Intent(AwardsActivity.this, EventActivity.class);

            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("city", _city );
            intent.putExtra("company", _company );
            intent.putExtra("type", "2" );
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(AwardsActivity.this, WallActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(AwardsActivity.this, LegendActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(AwardsActivity.this, ChallengeActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );
            startActivity(intent);
        }
    }
    public void GetAllAwards(final int Points , final String picture , final String name  )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(AwardsActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "AwardAPI/GetAllAwards"  , null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        items = new ArrayList<>();
                        JSONArray contents = response.getJSONArray("ArrayAwards");
                        for (int i = 0; i < contents.length(); i++)
                        {
                            JSONObject content = contents.getJSONObject(i);
                            items.add(new Award( content.getInt("Id") , content.getString("Name") , content.getString("Description") , content.getInt("Point") , content.getString("Image") , content.getBoolean("IsActive") ,  Points  , picture , name ) );
                        }
                        listAdapter = new AwardAdapter(items, ref );
                        recycler.setAdapter(listAdapter);
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetAllAwards( Points , picture , name );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
    public void GetUser( final int UserId )
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(AwardsActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(UserId) );
        params.put("Width", "50");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        byte[] decodedString = Base64.decode(response.getString("Picture"), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        photoProfile.setImageBitmap(decodedByte);

                        txtPoint.setText( ""+response.getInt("Points") + " Puntos" );
                        txtName.setText( response.getString("Name") );




                        _nombre = response.getString("Name");
                        _foto = response.getString("Picture");
                        _email = response.getString("Email");
                        _company = response.getString("Company");
                        _city = response.getString("City");

                        GetAllAwards( response.getInt("Points") ,  response.getString("Picture") ,  response.getString("Name") );
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContex );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetUser( UserId );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
    public void UpdatePoint( int value )
    {
         txtPoint.setText( value + " Puntos " );
    }

}
