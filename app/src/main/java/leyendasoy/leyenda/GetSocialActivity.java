package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.SocialAdapter;
import adparter_class.Followed;
import clases.MiHelper;
import clases.PrefManager;
import clases.Singleton;
import clases.Util;

public class GetSocialActivity extends AppCompatActivity implements View.OnClickListener
{

    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    ProgressDialog progressDialog = null;
    String userId;
    List<Followed> items = null;
    Util util;
    Context miContex;
    GetSocialActivity ref;
    EditText txtSearch;
    Button btnNext;

    int _iduser ;
    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SocialAdapter listAdapter = null;

    SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<Followed> follows;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_social);

        miContex = this;
        ref = this;


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        PrefManager prefManager = new PrefManager(this);
        prefManager.setFirstTimeLaunch(false);

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _iduser = Singleton.getInstance().getUserID();
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);


        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        txtSearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        
        util = new Util();

        if (util.compruebaConexion(this))
        {
            getSocial( page , false  );
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error en la conexión");
            builder.setMessage("Error no tienes conexión a internet.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    getSocial( page , false );
                }
            });
            builder.show();
        }


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources( R.color.blue);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    getSocial( page , false );
                /*
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       // setupAdapter();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
                */
            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {

                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    //Log.i("visibleItemCount" , " " + visibleItemCount );
                    //Log.i("pastVisiblesItems" , " " + pastVisiblesItems );
                    //Log.i("totalItemCount" , " " + totalItemCount );

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                               getSocial( page , true );

                        }
                    }

                }
            }
        });

    }
    public void getSocial(final int page , final Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();
        if( progressDialog ==  null )
        {
            progressDialog = new ProgressDialog(GetSocialActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("Width", "50");
        params.put("Height", "50");
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", "5");
        params.put("SearchQuery", "");

        Log.i("Params" , params.toString() );


        JSONObject jsonObj = new JSONObject(params);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetSocialByUser", jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                Log.i("response" , response.toString() );
                try
                {
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                            items.add(new Followed( 0 , "leyendas_del_servicio.png" , "", "Leyendas del Servicio", true , true ));
                        }

                        JSONArray users = response.getJSONArray("UserArray");
                        for (int i = 0; i < users.length(); i++)
                        {
                            JSONObject user = users.getJSONObject(i);
                            items.add(new Followed( user.getInt("UserId") , user.getString("Picture"), user.getString("Email"), user.getString("Name"), user.getBoolean("IsFollow") , false ));
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new SocialAdapter(items, ref);
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Followed> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();

                            listAdapter.addAll( newList );
                        }
                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                getSocial( page , add );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //getSocial( page , false );
    }

    public  void SaveUserFollow(  int userIdFollow , boolean isFollow )
    {
        final MiHelper miHelper = new MiHelper();

        progressDialog = new ProgressDialog(GetSocialActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);
        final Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("UserFollowId", String.valueOf(userIdFollow) );
        params.put("IsFollow", String.valueOf(isFollow) );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/SaveUserFollow", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

    @Override
    public void onClick(View view)
    {

         if( view.getId() == R.id.btnNext )
         {
             Intent intent = new Intent(GetSocialActivity.this, ProfileActivity.class);
             intent.putExtra("userId", _iduser );
             intent.putExtra("showButtons", false  );
             intent.putExtra("isFollow", false );
             startActivity(intent);
         }

    }

}
