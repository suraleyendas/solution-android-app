package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.VideoView;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import clases.FilePath;
import clases.MiHelper;
import clases.Singleton;

public class MediaChallengeActivity extends AppCompatActivity implements View.OnClickListener {

    String _nombre , _foto  , _email , _city , _company ;
    int _iduser;
    int _type;
    ImageView imgPreview;
    VideoView videoView;
    Button btnAceptar , btnTakePhoto , btnSelectPhoto , btnTakeVideo , btnSelectVideo , btnVolver;
    ProgressDialog progressDialog = null;
    String type = "";

    RelativeLayout layoutImage , layoutVideo;


    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_SELECT_IMAGE_REQUEST_CODE = 101;

    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int CAMERA_SELECT_VIDEO_REQUEST_CODE = 201;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    ByteArrayOutputStream baos;

    long totalSize = 0;
    private Uri fileUri = null;
    String path = null;
    File file = null;

    int ChallengeID , ContentID;
    RelativeLayout play , pause;
    SeekBar seekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_challenge);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        _type = extras.getInt("type");
        ChallengeID = extras.getInt("ChallengeID");
        ContentID = extras.getInt("ContentID");

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        layoutImage = ( RelativeLayout ) findViewById(R.id.layoutImage );
        layoutVideo = ( RelativeLayout ) findViewById(R.id.layoutVideo );
        layoutImage.setVisibility(View.GONE);
        layoutVideo.setVisibility(View.GONE);

        btnVolver = (Button) findViewById(R.id.btnVolver);
        btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
        btnSelectPhoto = (Button) findViewById(R.id.btnSelectPhoto);
        btnTakeVideo = (Button) findViewById(R.id.btnTakeVideo);
        btnSelectVideo = (Button) findViewById(R.id.btnSelectVideo);

        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        videoView = (VideoView) findViewById(R.id.videoPreview);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        btnVolver.setOnClickListener(this);


        btnTakePhoto.setOnClickListener(this);
        btnSelectPhoto.setOnClickListener(this);
        btnTakeVideo.setOnClickListener(this);
        btnSelectVideo.setOnClickListener(this);
        btnAceptar.setOnClickListener(this);

        MiHelper miHelper = new MiHelper();

        if( _type == 1 )
        {
            btnTakePhoto.setVisibility(View.VISIBLE);
            btnSelectPhoto.setVisibility(View.VISIBLE);
            imgPreview.setVisibility(View.VISIBLE);
            layoutImage.setVisibility(View.VISIBLE);

            btnTakeVideo.setVisibility(View.GONE);
            btnSelectVideo.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            type = String.valueOf(miHelper.contentImage);
        }
        else
        {
            btnTakePhoto.setVisibility(View.GONE);
            btnSelectPhoto.setVisibility(View.GONE);
            imgPreview.setVisibility(View.GONE);
            layoutVideo.setVisibility(View.VISIBLE);

            btnTakeVideo.setVisibility(View.VISIBLE);
            btnSelectVideo.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.VISIBLE);
            type = String.valueOf(miHelper.contentVideo);
        }
    }
    private void captureImage()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    public void selectImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), CAMERA_SELECT_IMAGE_REQUEST_CODE);
    }
    private void recordVideo()
    {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(android.provider.MediaStore.EXTRA_SIZE_LIMIT, 5242880);
        intent.putExtra("android.intent.extra.durationLimit", 20 );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }
    public void selectVideo()
    {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione un video"), CAMERA_SELECT_VIDEO_REQUEST_CODE);
    }
    public Uri getOutputMediaFileUri(int type)
    {
        this.file = getOutputMediaFile(type);
        return Uri.fromFile( this.file );
    }
    private static File getOutputMediaFile(int type)
    {
        MiHelper miHelper = new MiHelper();
        File mediaStorageDir = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),miHelper.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                Log.i("Error", "Oops! Failed create " + miHelper.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        }
        else if (type == MEDIA_TYPE_VIDEO)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        }
        else
        {
            return null;
        }
        return mediaFile;
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.btnTakePhoto )
        {
            captureImage();
        }
        else if( view.getId() == R.id.btnSelectPhoto )
        {
            selectImage();
        }
        else if( view.getId() == R.id.btnTakeVideo )
        {
            recordVideo();
        }
        else if( view.getId() == R.id.btnSelectVideo )
        {
            selectVideo();
        }
        else if( view.getId() == R.id.btnVolver )
        {
            onBackPressed();
        }
        else if( view.getId() == R.id.btnAceptar )
        {
            if( fileUri != null )
                file = new File( fileUri.getPath() );

            try
            {
                Singleton.getInstance().setFile( file );
                Intent intent = new Intent(MediaChallengeActivity.this, TakePartUploadActivity.class);
                intent.putExtra("ChallengeID", ChallengeID);
                intent.putExtra("ContentID", ContentID);
                intent.putExtra("type", _type);
                intent.putExtra("hasFile", true);

                startActivity(intent);
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }
        else if( view.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", fileUri);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }
    public Bitmap Redimensionar(Bitmap mBitmap, int newWidth)
    {
        float aspectRatio = mBitmap.getWidth() / (float) mBitmap.getHeight();
        int width = newWidth;
        int height = Math.round(width / aspectRatio);

        mBitmap = Bitmap.createScaledBitmap( mBitmap, width, height, false);

        return  mBitmap;
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private File SaveBitmap(String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;

    }
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation)
    {
        Matrix matrix = new Matrix();
        switch (orientation)
        {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try
        {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e)
        {
            e.printStackTrace();
            return null;
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                path = null;
                btnAceptar.setEnabled(true);
                btnAceptar.setBackgroundResource(R.drawable.double_blue);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), null);
                //bitmap = Redimensionar(bitmap, 800);

                ExifInterface ei = null;
                try
                {
                    ei = new ExifInterface( fileUri.getPath() );
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap b = rotateBitmap( bitmap , orientation );
                    Singleton.getInstance().setBitmap( b );
                    imgPreview.setImageBitmap(b);

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    String filename = "IMG_" + timeStamp + ".jpg";

                    File sd = Environment.getExternalStorageDirectory();
                    File dest = new File(sd, filename);
                    file = dest;


                    FileOutputStream out = new FileOutputStream(dest);
                    b.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();

                }
                catch (IOException e)
                {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }
            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( fileUri == null )
                {
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( fileUri == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la captura de imagen", Toast.LENGTH_SHORT).show();
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if( requestCode == CAMERA_SELECT_IMAGE_REQUEST_CODE )
        {
            if (resultCode == RESULT_OK)
            {
                fileUri = null;
                btnAceptar.setEnabled(true);
                btnAceptar.setBackgroundResource(R.drawable.double_blue);

                Uri selectedFileUri = data.getData();

                String path = getRealPathFromURI( selectedFileUri );
                file = new File(path);

                /*
                file = SaveBitmap( path );
                Toast.makeText(this, "Path " + path , Toast.LENGTH_SHORT).show();


                Log.i("File" , file.toString() );
                Log.i("File" , file.getPath() );
                */

                try
                {

                    Bitmap mbitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedFileUri);
                    Bitmap b = Redimensionar(mbitmap, 800);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    imgPreview.setImageBitmap(b);
                    try
                    {
                        Singleton.getInstance().setBitmap( b );
                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }

                    //Bitmap bitmap = BitmapFactory.decodeFile(selectedFileUri.getPath(), null);
                    /*


                   String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                   String filename = "IMG_" + timeStamp + ".jpg";

                   File sd = Environment.getExternalStorageDirectory();
                   File dest = new File(sd, filename);
                   file = dest;
                   */
                }
                catch (IOException e)
                {
                    Toast.makeText(this, "Error " + e.getMessage() , Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( path == null )
                {
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( path == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la selección de imagen", Toast.LENGTH_SHORT).show();
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                path = null;
                btnAceptar.setEnabled(true);
                btnAceptar.setBackgroundResource(R.drawable.double_blue);

                videoView.setBackground(new ColorDrawable(Color.TRANSPARENT));
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoPath( fileUri.getPath() );
                try
                {
                    Singleton.getInstance().setVideoPath( fileUri.getPath() );
                }
                catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }

                videoView.seekTo(100);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        seekbar.setVisibility(View.VISIBLE);
                        play.setVisibility(View.VISIBLE);
                    }
                });

            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( fileUri == null )
                {
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( fileUri == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la captura de video", Toast.LENGTH_SHORT).show();
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if( requestCode == CAMERA_SELECT_VIDEO_REQUEST_CODE )
        {
            if (resultCode == RESULT_OK)
            {
                fileUri = null;
                btnAceptar.setEnabled(true);
                btnAceptar.setBackgroundResource(R.drawable.double_blue);

                Uri selectedFileUri = data.getData();
                path = FilePath.getPath(this, selectedFileUri);

                videoView.setBackground(new ColorDrawable(Color.TRANSPARENT));
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoPath( path );

                try
                {
                    Singleton.getInstance().setVideoPath( path );
                }
                catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }

                videoView.seekTo(100);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        seekbar.setVisibility(View.VISIBLE);
                        play.setVisibility(View.VISIBLE);
                    }
                });


            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( path == null )
                {
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( path == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la selección de video", Toast.LENGTH_SHORT).show();
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
    }
    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:

                    Intent intent = new Intent(MediaChallengeActivity.this, ProfileActivity.class);

                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);

                    break;

            }
        }
        return(super.onOptionsItemSelected(item));
    }

}
