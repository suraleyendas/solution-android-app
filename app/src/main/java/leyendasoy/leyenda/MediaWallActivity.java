package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import clases.AndroidMultiPartEntity;
import clases.FilePath;
import clases.MiHelper;
import clases.Singleton;

public class MediaWallActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar;
    private TextView txtPercentage;
    String _nombre , _foto , _email , _city , _company , _type;
    int _iduser;
    ImageView imgPreview;
    VideoView vidPreview;
    Button btnPublicar , btnTakePhoto , btnSelectPhoto , btnTakeVideo , btnSelectVideo;
    LinearLayout layoutPercent;
    ProgressDialog progressDialog = null;
    String type = "";


    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_SELECT_IMAGE_REQUEST_CODE = 101;

    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int CAMERA_SELECT_VIDEO_REQUEST_CODE = 201;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    ByteArrayOutputStream baos;

    long totalSize = 0;
    private Uri fileUri = null;
    String path = null;
    File  file = null;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_wall);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        _type = extras.getString("type");

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        layoutPercent = (LinearLayout)  findViewById(R.id.layoutPercent);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        txtPercentage = (TextView) findViewById(R.id.txtPercentage);

        btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
        btnSelectPhoto = (Button) findViewById(R.id.btnSelectPhoto);
        btnTakeVideo = (Button) findViewById(R.id.btnTakeVideo);
        btnSelectVideo = (Button) findViewById(R.id.btnSelectVideo);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnPublicar = (Button) findViewById(R.id.btnPublicar);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);

        vidPreview = (VideoView) findViewById(R.id.videoPreview);



        btnTakePhoto.setOnClickListener(this);
        btnSelectPhoto.setOnClickListener(this);
        btnTakeVideo.setOnClickListener(this);
        btnSelectVideo.setOnClickListener(this);
        btnPublicar.setOnClickListener(this);

        MiHelper miHelper = new MiHelper();

        if( Integer.parseInt(_type ) == 1 )
        {
            btnTakePhoto.setVisibility(View.VISIBLE);
            btnSelectPhoto.setVisibility(View.VISIBLE);
            imgPreview.setVisibility(View.VISIBLE);

            btnTakeVideo.setVisibility(View.GONE);
            btnSelectVideo.setVisibility(View.GONE);
            vidPreview.setVisibility(View.GONE);
            type = String.valueOf(miHelper.contentImage);
        }
        else
        {
            btnTakePhoto.setVisibility(View.GONE);
            btnSelectPhoto.setVisibility(View.GONE);
            imgPreview.setVisibility(View.GONE);

            btnTakeVideo.setVisibility(View.VISIBLE);
            btnSelectVideo.setVisibility(View.VISIBLE);
            vidPreview.setVisibility(View.VISIBLE);
            type = String.valueOf(miHelper.contentVideo);
        }
    }
    private void captureImage()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    public void selectImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), CAMERA_SELECT_IMAGE_REQUEST_CODE);
    }
    private void recordVideo()
    {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(android.provider.MediaStore.EXTRA_SIZE_LIMIT, 5242880);
        intent.putExtra("android.intent.extra.durationLimit", 20 );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }
    public void selectVideo()
    {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione un video"), CAMERA_SELECT_VIDEO_REQUEST_CODE);
    }
    public Uri getOutputMediaFileUri(int type)
    {
        this.file = getOutputMediaFile(type);
        return Uri.fromFile( this.file );
    }
    private static File getOutputMediaFile(int type)
    {
        MiHelper miHelper = new MiHelper();
        File mediaStorageDir = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),miHelper.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                Log.i("Error", "Oops! Failed create " + miHelper.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        }
        else if (type == MEDIA_TYPE_VIDEO)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        }
        else
        {
            return null;
        }
        return mediaFile;
    }
    @Override
    public void onClick(View view)
    {
        progressBar.setProgress(0);
        if( view.getId() == R.id.btnTakePhoto )
        {
            captureImage();
        }
        else if( view.getId() == R.id.btnSelectPhoto )
        {
            selectImage();
        }
        else if( view.getId() == R.id.btnTakeVideo )
        {
            recordVideo();
        }
        else if( view.getId() == R.id.btnSelectVideo )
        {
            selectVideo();
        }
        else if( view.getId() == R.id.btnPublicar )
        {
            /*
            if( fileUri != null )
              file = new File( fileUri.getPath() );
              */

            layoutPercent.setVisibility(View.VISIBLE);
            new UploadFileToServer().execute();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", fileUri);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }
    public Bitmap Redimensionar(Bitmap mBitmap, int newWidth)
    {
        float aspectRatio = mBitmap.getWidth() / (float) mBitmap.getHeight();
        int width = newWidth;
        int height = Math.round(width / aspectRatio);

        mBitmap = Bitmap.createScaledBitmap( mBitmap, width, height, false);

        return  mBitmap;
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private File SaveBitmap(String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;

    }
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation)
    {
        Matrix matrix = new Matrix();
        switch (orientation)
        {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try
        {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                path = null;
                btnPublicar.setEnabled(true);
                btnPublicar.setBackgroundResource(R.drawable.double_blue);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), null);



                ExifInterface ei = null;
                try
                {
                    ei = new ExifInterface( fileUri.getPath() );
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap b = rotateBitmap( bitmap , orientation );
                    imgPreview.setImageBitmap(b);

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    String filename = "IMG_" + timeStamp + ".jpg";

                    File sd = Environment.getExternalStorageDirectory();
                    File dest = new File(sd, filename);
                    file = dest;


                    FileOutputStream out = new FileOutputStream(dest);
                    b.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();


                }
                catch (IOException e)
                {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( fileUri == null )
                {
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( fileUri == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la captura de imagen", Toast.LENGTH_SHORT).show();
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if( requestCode == CAMERA_SELECT_IMAGE_REQUEST_CODE )
        {
            if (resultCode == RESULT_OK)
            {
                fileUri = null;
                btnPublicar.setEnabled(true);
                btnPublicar.setBackgroundResource(R.drawable.double_blue);

                Uri selectedFileUri = data.getData();
                imgPreview.setImageURI(selectedFileUri);


                String path = getRealPathFromURI( selectedFileUri );
                file = new File(path);
                /*
                file = SaveBitmap( path );
                Toast.makeText(this, "Path " + path , Toast.LENGTH_SHORT).show();


                Log.i("File" , file.toString() );
                Log.i("File" , file.getPath() );
                */

                try
                {

                   Bitmap mbitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedFileUri);
                   //Bitmap b = Redimensionar(mbitmap, 800);
                   BitmapFactory.Options options = new BitmapFactory.Options();
                   options.inSampleSize = 8;
                   //Bitmap bitmap = BitmapFactory.decodeFile(selectedFileUri.getPath(), null);
                    /*


                   String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                   String filename = "IMG_" + timeStamp + ".jpg";

                   File sd = Environment.getExternalStorageDirectory();
                   File dest = new File(sd, filename);
                   file = dest;
                   */
                }
                catch (IOException e)
                {
                    Toast.makeText(this, "Error " + e.getMessage() , Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( path == null )
                {
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( path == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la selección de imagen", Toast.LENGTH_SHORT).show();
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                path = null;
                btnPublicar.setEnabled(true);
                btnPublicar.setBackgroundResource(R.drawable.double_blue);

                vidPreview.setBackground(new ColorDrawable(Color.TRANSPARENT));
                vidPreview.setVisibility(View.VISIBLE);
                vidPreview.setVideoPath( fileUri.getPath() );
                //vidPreview.seekTo(100);
                vidPreview.start();
            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( fileUri == null )
                {
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( fileUri == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la captura de video", Toast.LENGTH_SHORT).show();
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
        else if( requestCode == CAMERA_SELECT_VIDEO_REQUEST_CODE )
        {
            if (resultCode == RESULT_OK)
            {
                fileUri = null;
                btnPublicar.setEnabled(true);
                btnPublicar.setBackgroundResource(R.drawable.double_blue);

                Uri selectedFileUri = data.getData();
                path = FilePath.getPath(this, selectedFileUri);

                vidPreview.setBackground(new ColorDrawable(Color.TRANSPARENT));
                vidPreview.setVisibility(View.VISIBLE);
                vidPreview.setVideoURI( selectedFileUri );
                vidPreview.start();
            }
            else if (resultCode == RESULT_CANCELED)
            {
                if( path == null )
                {
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
            else
            {
                if( path == null )
                {
                    Toast.makeText(getApplicationContext(), "Fallo la selección de video", Toast.LENGTH_SHORT).show();
                    btnPublicar.setEnabled(false);
                    btnPublicar.setBackgroundResource(R.drawable.btn_disabled);
                }
            }
        }
    }
    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:

                    Intent intent = new Intent(MediaWallActivity.this, ProfileActivity.class);

                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);

                    break;

            }
        }
        return(super.onOptionsItemSelected(item));
    }
    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected void onPreExecute()
        {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }
        @Override
        protected void onProgressUpdate(Integer... progress)
        {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);
            // updating progress bar value
            progressBar.setProgress(progress[0]);
            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String getB64Auth (String login, String pass)
        {
            String source=login+":"+pass;
            String ret="Basic "+ Base64.encodeToString(source.getBytes(),Base64.URL_SAFE|Base64.NO_WRAP);
            return ret;
        }

        @SuppressWarnings("deprecation")
        private String uploadFile()
        {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://test.multistrategy.co/BigSocialNetwork/Service/api/MultimediaAPI/PostUserImage");
            //HttpPost httppost = new HttpPost("http://www.anasoft.com.co/upload.php");

            String authorizationString = "Basic " + Base64.encodeToString( ("PruebaAPI123" + ":" + "PruebaAPI123").getBytes(), Base64.NO_WRAP);
            httppost.setHeader("authorization", authorizationString);

            try
            {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener()
                {
                    @Override
                    public void transferred(long num)
                    {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });

                FileBody bin = new FileBody(file);
                entity.addPart("file", bin);


                //entity.addPart("file", new FileBody( file ));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();


                responseString = EntityUtils.toString(r_entity);
                Log.i("Response : " , responseString );


                try
                {
                    JSONObject jsonObject = new JSONObject(responseString);
                    Log.i("Json" ,jsonObject.toString() );
                    responseString = jsonObject.getString("Message");
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                int statusCode = response.getStatusLine().getStatusCode();

            }
            catch (ClientProtocolException e)
            {
                responseString = e.toString();
            }
            catch (IOException e)
            {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.i("TAG", "Response from server: " + result);
            //showAlert(result);
            EditText txtComment = (EditText) findViewById(R.id.txtComment);
            Log.i("TAG", "Ccccc: " + txtComment.getText() );

            SaveUserContentMedia( result , txtComment.getText().toString() , type );
            super.onPostExecute(result);
        }

    }
    public void SaveUserContentMedia( String filePath , String txtComment , String type )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(MediaWallActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", "2" );
        params.put("File", filePath );
        params.put("Content", txtComment );
        params.put("IsActive", "true" );
        params.put("ContentParent", "" );
        params.put("ContentTypeId", type );

        Log.i("Parametros :" , params.toString() );



        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveContent"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Log.i("SaveUserContentMedia", response.toString());
                    Intent intent = new Intent(MediaWallActivity.this, WallActivity.class);
                    startActivity(intent);
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MediaWallActivity.this);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
}
