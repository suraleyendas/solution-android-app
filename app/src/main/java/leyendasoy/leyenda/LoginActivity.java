package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import clases.Singleton;
import clases.Util;
import clases.DbHelper;
import clases.MiHelper;
import clases.PrefManager;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText textLogin , textPassword , recordar_clave;
    Button btnAccept;
    final Socket socket = null;

    Map<String,String> params;
    RequestQueue queue;
    JSONObject jsonObj;
    JsonObjectRequest request;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    DbHelper dbHelper;
    MiHelper miHelper;
    Util util;
    Context miContex;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        miContex = this;


        TextView titulo1 = (TextView) findViewById(R.id.titulo1);
        TextView titulo2 = (TextView) findViewById(R.id.titulo2);
        TextView recordar_clave = (TextView) findViewById(R.id.recordar_clave);

        recordar_clave.setPaintFlags(recordar_clave.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Typeface dinoLight = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Light 2.otf");
        Typeface dinoBold = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");

        titulo1.setTypeface(dinoLight);
        titulo2.setTypeface(dinoBold);

        util = new Util();

        Typeface customFontArial = Typeface.createFromAsset( getAssets() , "fonts/Arial.ttf");
        Typeface customFontArialBold = Typeface.createFromAsset( getAssets() , "fonts/Arial Bold.ttf");

        dbHelper = new DbHelper(this);


        prefManager = new PrefManager(this);
        miHelper = new MiHelper();


        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);


        btnAccept = ( Button ) findViewById( R.id.btnAccept );
        textLogin  = ( EditText ) findViewById(R.id.textLogin);
        textPassword  = ( EditText ) findViewById(R.id.textPassword);

        btnAccept.setOnClickListener(this);
        recordar_clave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
            if (v.getId() == R.id.btnAccept)
            {
                if( util.compruebaConexion(this) )
                {
                    String username = textLogin.getText().toString();
                    String password = textPassword.getText().toString();

                    Boolean valid = true;

                    if (username.isEmpty())
                    {
                        textLogin.setError("Ingrese el usuario.");
                        valid = false;
                    }
                    else
                    {
                        textLogin.setError(null);
                    }

                    if (password.isEmpty())
                    {
                        textPassword.setError("Ingrese la contraseña.");
                        valid = false;
                    }
                    else
                    {
                        textPassword.setError(null);
                    }
                    if (valid == true)
                    {
                        Login();
                    }
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder( this );
                    builder.setTitle("Error en la conexión");
                    builder.setMessage("Error no tienes conexión a internet.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            Login();
                        }
                    });
                    builder.show();
                }
            }
            if (v.getId() == R.id.recordar_clave)
            {
                Uri uri = Uri.parse("https://login.sura.com/forgotpassword.aspx?ReturnUrl=https://www.somossura.com/_layouts/15/seguridadsura/formslogin.aspx?ReturnUrl=/_layouts/15/Authenticate.aspx?Source=%2F&Source=/&# <https://login.sura.com/forgotpassword.aspx?ReturnUrl=https://www.somossura.com/_layouts/15/seguridadsura/formslogin.aspx?ReturnUrl=/_layouts/15/Authenticate.aspx?Source=/&Source=/&>");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
    }

    @Override
    public void onBackPressed() {
    }

    public String getLocalIpAddress()
    {
        try
        {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());

                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("Tacg", ex.toString());
        }
        return null;
    }
    public void Login()
    {
        String ip =  getLocalIpAddress();

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("userName", textLogin.getText().toString());
        params.put("password", textPassword.getText().toString());
        params.put("ipAddress", ip);
        params.put("programId", "1");
        params.put("userTypeId", "1");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/Login" , jsonObj , new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {

                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if( result == true )
                    {
                        RadioGroup rg = (RadioGroup) findViewById(R.id.radioButtonGroup1);
                        String rol = ((RadioButton)findViewById(rg.getCheckedRadioButtonId() )).getText().toString();


                        Singleton.getInstance().setUserID( response.getInt("UserId") );
                        Intent intent = new Intent(LoginActivity.this, EditprofileActivity.class);
                        intent.putExtra("role", rol );
                        intent.putExtra("textoBtn", "Siguiente" );
                        startActivity(intent);
                    }
                    else
                    {

                        AlertDialog.Builder builder = new AlertDialog.Builder( LoginActivity.this );
                        builder.setMessage("Error datos incorrectos por favor vuelva a intentarlo.").setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {

                            }
                        });
                        builder.show();
                    }

                } catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContex );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                Login();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

}
