package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.BoringLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.ReportAdapter;
import adapters.SocialAdapter;
import adapters.WallAdapter;
import adparter_class.Comment;
import adparter_class.Report;
import adparter_class.Wall;
import clases.MiHelper;
import clases.Singleton;

public class ReportActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog progressDialog = null;
    private RecyclerView recycler;
    Context miContex;
    ReportActivity ref;
    private String[] data;
    EditText txt;
    Button btn;
    int ReportId;
    int _iduser;
    int ContentID;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        txt = ( EditText ) findViewById(R.id.txt);
        btn = ( Button ) findViewById(R.id.btn);

        miContex = this;
        ref = this;
        ContentReportTypeAPI();
        recycler = (RecyclerView) findViewById(R.id.lista);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        ContentID = extras.getInt("ContentID");
        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _iduser = Singleton.getInstance().getUserID();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        btn.setOnClickListener(this);
        btn.setPadding(60,0,60,0);
    }

    public void ContentReportTypeAPI() {
        final MiHelper miHelper = new MiHelper();

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ReportActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "ContentReportTypeAPI", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");
                    Log.i("Resultado", response.toString());

                    if (result == true)
                    {
                        JSONArray contents = response.getJSONArray("arrayContentReportType");
                        List<Report> items = new ArrayList<>();

                        for (int i = 0; i < contents.length(); i++)
                        {
                            JSONObject content = contents.getJSONObject(i);
                            items.add( new Report(  content.getInt("Id") , content.getString("Description") , false ));
                        }
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ref);
                        recycler.setLayoutManager(linearLayoutManager);
                        final ReportAdapter listAdapter = new ReportAdapter(items,ref);
                        recycler.setAdapter(listAdapter);
                    }

                } catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //GetWallByUser();
                            }
                        }).show();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }

    public void ChangeStatusViews( Boolean enableText ,  Boolean enableButton , int ReportId )
    {

        this.ReportId = ReportId;
        if( ReportId == 12 )
        {
            txt.setBackgroundResource(R.drawable.input_enable);
        }
        else
        {
            txt.setBackgroundResource(R.drawable.input_disabled);
        }

        txt.setEnabled(enableText);
        btn.setEnabled(enableButton);
        if( enableButton )
        {
             btn.setBackgroundResource(R.drawable.double_blue);

             btn.setPadding(60,0,60,0);
        }
        else
        {
            btn.setBackgroundResource(R.drawable.btn_disabled);
            btn.setPadding(60,0,60,0);

        }

    }
    @Override
    public void onClick(View view)
    {
       if( view.getId() == R.id.btn )
       {
           txt.setError(null);


           if( ReportId == 12 )
           {
               String text = txt.getText().toString().trim();
               if (text.trim().length() > 0)
               {
                   SaveUserContent(text);
               }
               else
               {
                   txt.setError("Ingrese el motivo");
               }
           }
           else
           {
               SaveUserContent("");
           }
       }
    }
    public void SaveUserContent( String description )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ReportActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentID) );
        params.put("UserShareId", "" );
        params.put("UserContentTypeId", String.valueOf(miHelper.userContentTypeReport)  );
        params.put("ReportTypeId", String.valueOf( ReportId ) );
        params.put("ReportDescription", description  );
        params.put("Viewed", ""  );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveUserContent"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if( result == true )
                    {
                        Toast.makeText(miContex, "Publicación reportada exitosamente.", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                    else
                    {
                        Toast.makeText(miContex, "No se pudo reportar la publicación.", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //getFollowed();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(ReportActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(ReportActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

                case R.id.notification:
                    intent = new Intent(ReportActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    break;

            }
        }
        return(super.onOptionsItemSelected(item));
    }
}
