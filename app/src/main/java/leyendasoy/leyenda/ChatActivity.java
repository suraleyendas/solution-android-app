package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.ChatAdapter;
import adapters.MessageAdapter;
import adapters.WallAdapter;
import adparter_class.Chat;
import adparter_class.Comment;
import adparter_class.Message;
import adparter_class.Wall;
import clases.MiHelper;
import clases.Singleton;
import io.socket.engineio.client.Socket;

import static android.app.PendingIntent.getActivity;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout tab_inicio , tab_leyenda;
    public String _nombre , _foto  , _email , _city , _company ,  _userphoto , myPhoto;
    public int _iduser , _userto;
    ImageView send;
    Context miContext;

    List<Chat> list;
    RecyclerView recycler;
    ChatAdapter chatAdapter;
    TextView txtMsg;
    ProgressDialog progressDialog = null;
    MiHelper miHelper;
    public Boolean StatusActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        StatusActivity = true;

        miHelper = new MiHelper();
        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            Singleton.getInstance().setChatActivity( this );
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        miContext = this;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        _userto = extras.getInt("UserTo") ;
        _userphoto = extras.getString("PhotoUser");

        try
        {
            _iduser = Singleton.getInstance().getUserID();
            _nombre = Singleton.getInstance().getName();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }




        GetUser( _iduser );

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        txtMsg = ( TextView ) findViewById(R.id.txtMsg);

        send = ( ImageView ) findViewById(R.id.send);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );
        send.setOnClickListener(this);
        list = new ArrayList<>();

        recycler = ( RecyclerView ) findViewById(R.id.recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        chatAdapter = new ChatAdapter(list,this);
        recycler.setAdapter(chatAdapter);

        txtMsg.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if( s.toString().trim().length() > 0  )
                {
                    send.setBackgroundResource(R.drawable.send);
                }
                else
                {
                    send.setBackgroundResource(R.drawable.send_gray);
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
    }
    @Override
    public void onPause()
    {
        super.onPause();
        StatusActivity = false;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        StatusActivity = true;
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        StatusActivity = false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            Intent intent;
            switch (item.getItemId())
            {

                case R.id.user:
                    intent = new Intent(ChatActivity.this, ProfileActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(ChatActivity.this, MessageActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(ChatActivity.this, WallActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(ChatActivity.this, LegendActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if ( view.getId() == R.id.send )
        {
            try
            {
                String texto = txtMsg.getText().toString().trim();
                if( texto.length() > 0  )
                {
                    txtMsg.setText("");
                    AddChatMessageTo( texto , _iduser );
                    JSONObject obj = new JSONObject();
                    obj.put("to_user", _userto);
                    obj.put("from_user",_iduser);
                    obj.put("message",texto);
                    obj.put("content_id",0);
                    obj.put("content_type_id",0);
                    obj.put("description_content", "");

                    Singleton.getInstance().NewMessage( obj );
                }
                else
                   txtMsg.setError("Este campo es obligatorio.");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }
    }
    public void refreshMyRecyclerView( final int items )
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                ChatActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        chatAdapter.notifyDataSetChanged();
                        ChatActivity.this.updateScroll( items );
                    }
                });

            }
        }).start();
    }
    public void AddChatMessageTo( String text , int user_id )
    {
        int items = chatAdapter.addItem(text, user_id , "" ,  1 , _userphoto );
        refreshMyRecyclerView(items);
    }
    public void AddChatMessageFrom( String text , int from_user )
    {
        int items = chatAdapter.addItem(text, from_user, "", 2 , _foto );
        refreshMyRecyclerView(items);
    }
    public void AddAllChatMessageFrom(JSONArray messages )
    {
          for (int i = 0 ;  i < messages.length() ; i++ )
          {
              try
              {
                  JSONObject message = messages.getJSONObject(i);
                  if( message.getInt("from_user") == _iduser )
                     chatAdapter.addItem( message.getString("text"), message.getInt("from_user"), "", 1, _userphoto);
                  else
                     chatAdapter.addItem( message.getString("text"), message.getInt("from_user"), "", 2, _userphoto);
              }
              catch (JSONException e)
              {
                  e.printStackTrace();
              }
          }
         refreshMyRecyclerView(  messages.length() );
    }
    public void GetUserById()
    {

        String url = miHelper.getURL() + "/UsersAPI/GetUser/" + _iduser ;
        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                Log.i("Login:",response.toString() );

                try
                {
                    if( response.getBoolean("Result") == true )
                    {

                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                try
                {
                    if( response.getString("Picture").length() > 0 )
                    {
                        myPhoto = response.getString("Picture");
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.toString() );
                        progressDialog.dismiss();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        queue.add(getRequest);
    }
    public void updateScroll( int size )
    {
        recycler.scrollToPosition( size - 1 );
    }
    public void GetUser( final int UserId )
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ChatActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(UserId) );
        params.put("Width", "50");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.i("Resrrr : " , response.toString() );
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        _foto = response.getString("Picture");
                        try
                        {
                            JSONObject obj = new JSONObject();
                            obj.put("to_user", _userto);
                            obj.put("from_user",_iduser);
                            Singleton.getInstance().GetMessages( obj );
                        }
                        catch (URISyntaxException e)
                        {
                            e.printStackTrace();
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContext );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetUser( UserId );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }

}
