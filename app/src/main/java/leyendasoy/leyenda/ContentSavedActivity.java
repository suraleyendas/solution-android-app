package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.SavedAdapter;
import adapters.WallAdapter;
import adparter_class.Comment;
import adparter_class.Wall;
import clases.MiHelper;
import clases.Singleton;
import leyendasoy.leyenda.R;

public class ContentSavedActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout tab_inicio , tab_leyenda ,  tab_retos;
    String _nombre , _foto  , _email , _city , _company;
    int _iduser;

    ProgressDialog progressDialog = null;
    Context miContex;
    ContentSavedActivity ref;

    List<Wall> items;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;
    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    SavedAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_saved);

        miContex = this;
        ref = this;

        TextView titulo1 = ( TextView ) findViewById(R.id.titulo1);
        Typeface dinoBold = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");
        titulo1.setTypeface(dinoBold);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        try
        {
            _nombre = Singleton.getInstance().getName();
            _foto =  Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email =  Singleton.getInstance().getEmail();
            _city =  Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        GetMyPublish( page  , false );

        recycler = (RecyclerView) findViewById(R.id.recycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    GetMyPublish( page , false );
            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                GetMyPublish( page , true );
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(ContentSavedActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(ContentSavedActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

                case R.id.notification:
                    intent = new Intent(ContentSavedActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(ContentSavedActivity.this, WallActivity.class);

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(ContentSavedActivity.this, LegendActivity.class);

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(ContentSavedActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
    }

    public void GetMyPublish( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ContentSavedActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("TopComment", "2");
        params.put("UserContentTypeId", String.valueOf(miHelper.userContentTypeSave));
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", miHelper.paginationContentSaved );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetMyPublish"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.i("respon " , response.toString() );
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }

                        JSONArray contents = response.getJSONArray("arrayContent");
                        for (int i = 0; i < contents.length(); i++)
                        {
                            List<Comment> commentList = new ArrayList<>();
                            JSONObject content = contents.getJSONObject(i);

                            JSONArray ListComment = content.getJSONArray("ListComment");
                            if( ListComment.length() > 0 )
                            {
                                for (int j = 0 ; j < ListComment.length() ; j++ )
                                {
                                    JSONObject comment = ListComment.getJSONObject(j);
                                    Comment c = new Comment( comment.getString("Picture") , comment.getString("FullName") , comment.getString("StrComment") );
                                    commentList.add(c);
                                }
                            }
                            items.add(new Wall( content.getString("Name") , content.getString("ContentDescription").trim() , content.getString("Picture") , content.getString("ContentId") , content.getString("UserId") , content.getString("FilePath") , content.getString("strContent") , content.getBoolean("IsActive") , content.getInt("ContentTypeId") , content.getString("ContentType") ,  content.getBoolean("IsLike") , content.getInt("CountLike") , content.getInt("CountComment") , commentList , content.getInt("Event_Type") , content.getBoolean("IsChallenge") , content.getString("ChallengeTitle") , content.getString("ChallengeDescription") , content.getInt("ChallengePoint") , content.getBoolean("IsParticipated") , content.getString("LimitDateChallenge") ,  content.getInt("ChallengeTypeId") ,  content.getInt("ChallengeId")  ) );
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new SavedAdapter(items, ref);
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Wall> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }
                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetMyPublish( page , false );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }


}
