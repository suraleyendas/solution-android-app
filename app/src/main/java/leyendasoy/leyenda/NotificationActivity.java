package leyendasoy.leyenda;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.NotificationAdapter;
import adapters.WallAdapter;
import adparter_class.Comment;
import adparter_class.Notification;
import adparter_class.Wall;
import clases.MiHelper;
import clases.Singleton;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recycler;
    List<Notification> items;
    ProgressDialog progressDialog = null;
    Context miContex;
    NotificationActivity ref;
    LinearLayout tab_inicio , tab_leyenda , tab_retos ,  tab_premios;

    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    NotificationAdapter listAdapter;
    int _iduser;
    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationIcon(R.drawable.ic_menu_blue);

        miContex = this;
        ref = this;

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        tab_premios = (LinearLayout) findViewById(R.id.tab_premios);
        tab_premios.setOnClickListener( this );

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );

             _iduser = Singleton.getInstance().getUserID();

            Singleton.getInstance().setContext( this );
            NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            Singleton.getInstance().setNotificationManager( nm );
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        recycler = (RecyclerView) findViewById(R.id.recycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    GetNotificationByUserId( page , false );

            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                GetNotificationByUserId( page , true );
                        }
                    }
                }
            }
        });

        GetNotificationByUserId( page , false );
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        this.menu = menu;
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(NotificationActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(NotificationActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

                case R.id.notification:
                    intent = new Intent(NotificationActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));

    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.tab_inicio )
        {
            Intent intent = new Intent(NotificationActivity.this, WallActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(NotificationActivity.this, LegendActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(NotificationActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_premios )
        {
            Intent intent = new Intent(NotificationActivity.this, AwardsActivity.class);
            startActivity(intent);
        }
    }
    public void GetNotificationByUserId( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(NotificationActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", miHelper.paginationNotification );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "NotificationAPI/GetNotificationByUserId"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.i("Response : " , response.toString() );
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }

                        JSONArray contents = response.getJSONArray("ArrayNotificationByuser");
                        if( contents.length() > 0 )
                        {
                            MenuItem item = menu.findItem(R.id.notification);
                            if (item != null)
                            {
                                item.setIcon(R.drawable.ic_bell_menu_active);
                            }
                        }
                        for (int i = 0; i < contents.length(); i++)
                        {
                            List<Comment> commentList = new ArrayList<>();
                            JSONObject content = contents.getJSONObject(i);

                            items.add(new Notification( content.getInt("NotificationId") , content.getString("Message") , content.getInt("UserNotificaterId") , content.getString("UserName") , content.getString("Picture") , content.getBoolean("IsViewed") , content.getInt("ContentId") , content.getInt("NotificationTypeId") ) );
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new NotificationAdapter(items, miContex );
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Notification> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetNotificationByUserId( page , false );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
}
