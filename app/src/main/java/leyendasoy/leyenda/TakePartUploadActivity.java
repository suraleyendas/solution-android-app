package leyendasoy.leyenda;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import clases.AndroidMultiPartEntity;
import clases.MiHelper;
import clases.Singleton;

public class TakePartUploadActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar;
    ProgressDialog progressDialog = null;

    String _nombre , _foto  , _email , _city , _company ;
    int _iduser;
    int _type ,  ChallengeID , ContentID ,  ChallengeTypeID;
    Button btnCargar , finishChallege;
    TextView txt_point , txt_title , txtPercentage;

    File file = null;
    long totalSize = 0;

    RelativeLayout play , pause;
    SeekBar seekbar;
    ImageView imgPreview;
    VideoView videoView;
    RelativeLayout  layoutImage , layoutVideo;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_part_upload);

        txt_point = ( TextView ) findViewById(R.id.txt_point);
        txt_title = ( TextView ) findViewById(R.id.txt_title);

        imgPreview = ( ImageView ) findViewById(R.id.imgPreview);
        videoView = (VideoView) findViewById(R.id.videoPreview);

        layoutImage = ( RelativeLayout ) findViewById(R.id.layoutImage );
        layoutVideo = ( RelativeLayout ) findViewById(R.id.layoutVideo );
        layoutImage.setVisibility(View.GONE);
        layoutVideo.setVisibility(View.GONE);

        btnCargar = ( Button ) findViewById(R.id.btnCargar);
        btnCargar.setOnClickListener(this);

        finishChallege = ( Button ) findViewById(R.id.finishChallege);
        finishChallege.setOnClickListener(this);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        _type = extras.getInt("type");
        ChallengeID = extras.getInt("ChallengeID");
        ContentID = extras.getInt("ContentID");
        ChallengeTypeID = extras.getInt("ChallengeTypeID");

        txt_point.setText( extras.getString("txtPoints"));
        txt_title.setText( extras.getString("txtTitle"));

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );

            _nombre = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _foto = Singleton.getInstance().getPhoto();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        if (extras.containsKey("hasFile"))
        {
            try
            {
                if( Singleton.getInstance().getFile() != null )
                {
                    file = Singleton.getInstance().getFile();
                    finishChallege.setEnabled(true);
                    finishChallege.setBackgroundResource( R.drawable.double_blue );
                }

            } catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
            if(  _type == 1 )
            {
                try
                {

                    imgPreview.setImageBitmap(Singleton.getInstance().getBitmap());
                    imgPreview.setVisibility(View.VISIBLE);
                }
                catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                try
                {
                    videoView.setBackground(new ColorDrawable(Color.TRANSPARENT));
                    videoView.setVisibility(View.VISIBLE);
                    videoView.setVideoPath( Singleton.getInstance().getVideoPath() );
                    videoView.seekTo(100);
                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                    {
                        @Override
                        public void onPrepared(MediaPlayer mp)
                        {
                            seekbar.setVisibility(View.VISIBLE);
                            play.setVisibility(View.VISIBLE);
                        }
                    });
                }
                catch (URISyntaxException e)
                {
                    e.printStackTrace();
                }
            }
            /*
            profile_guess = true;
            userIdFollow = extras.getString("userIdFollow");
            GetProfile( userIdFollow );
            GetProfileInfoByUserId( userIdFollow );
            */
        }
        if( _type == 1 )
        {
            imgPreview.setVisibility(View.VISIBLE);
            layoutImage.setVisibility(View.VISIBLE);
        }
        else
        {
            videoView.setVisibility(View.VISIBLE);
            layoutVideo.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onClick(View v)
    {
        progressBar.setProgress(0);
        if( v.getId() == R.id.btnCargar )
        {
            finishChallege.setEnabled(false);
            finishChallege.setBackgroundResource( R.drawable.btn_disabled );
            goToMediaChallenge( ChallengeID , ContentID , _type );
        }
        else if( v.getId() == R.id.finishChallege )
        {
            new UploadFileToServer().execute();
        }
        else if( v.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        if( item.getTitle() == null )
        {
            Intent intent = new Intent(TakePartUploadActivity.this, ChallegeDetailActivity.class);
            intent.putExtra("ChallengeID", ChallengeID );
            intent.putExtra("ContentID", ContentID );
            intent.putExtra("ChallengeTypeID", ChallengeTypeID );
            startActivity(intent);
        }
        return (super.onOptionsItemSelected(item));

    }
    public void goToMediaChallenge( int ChallengeID , int ContentID , int type )
    {
        Intent intent = new Intent(TakePartUploadActivity.this, MediaChallengeActivity.class);
        intent.putExtra("ChallengeID", ChallengeID );
        intent.putExtra("ContentID", ContentID );
        intent.putExtra("type", type );
        startActivity(intent);
    }
    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected void onPreExecute()
        {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }
        @Override
        protected void onProgressUpdate(Integer... progress)
        {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);
            // updating progress bar value
            progressBar.setProgress(progress[0]);
            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String getB64Auth (String login, String pass)
        {
            String source=login+":"+pass;
            String ret="Basic "+ Base64.encodeToString(source.getBytes(),Base64.URL_SAFE|Base64.NO_WRAP);
            return ret;
        }

        @SuppressWarnings("deprecation")
        private String uploadFile()
        {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://test.multistrategy.co/BigSocialNetwork/Service/api/MultimediaAPI/PostUserImage");

            String authorizationString = "Basic " + Base64.encodeToString( ("PruebaAPI123" + ":" + "PruebaAPI123").getBytes(), Base64.NO_WRAP);
            httppost.setHeader("authorization", authorizationString);

            try
            {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener()
                {
                    @Override
                    public void transferred(long num)
                    {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });

                FileBody bin = new FileBody(file);
                entity.addPart("file", bin);


                //entity.addPart("file", new FileBody( file ));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();


                responseString = EntityUtils.toString(r_entity);
                Log.i("Response : " , responseString );


                try
                {
                    JSONObject jsonObject = new JSONObject(responseString);
                    Log.i("Json" ,jsonObject.toString() );
                    responseString = jsonObject.getString("Message");
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                int statusCode = response.getStatusLine().getStatusCode();

            }
            catch (ClientProtocolException e)
            {
                responseString = e.toString();
            }
            catch (IOException e)
            {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result)
        {

            SaveChallengeUploadFileByUser( result );
            super.onPostExecute(result);
        }

    }
    public void SaveChallengeUploadFileByUser( String filePath  )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(TakePartUploadActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("ChallengeId", "" + ChallengeID );
        params.put("UserId", String.valueOf(_iduser) );
        params.put("FileDescription", "" );
        params.put("FilePath", filePath );

        //params.put("ContentTypeId", type );

        Log.i("Parametros :" , params.toString() );



        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ChallengeAPI/SaveChallengeUploadFileByUser"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Log.i("UploadFileByUser  :::: ", response.toString());
                    Intent intent = new Intent(TakePartUploadActivity.this, FinishChallengeVideoImageActivity.class);
                    intent.putExtra("ContentID", ContentID);
                    intent.putExtra("ChallengeID", ChallengeID);
                    intent.putExtra("ChallengeTypeID", ChallengeTypeID);
                    intent.putExtra("type", _type);
                    startActivity(intent);

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TakePartUploadActivity.this);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
}
