package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.CommentAdapter;
import adapters.EventLegendAdapter;
import adparter_class.Comment;
import adparter_class.EventLegend;
import clases.MiHelper;
import clases.Singleton;


public class EventActivity extends AppCompatActivity  {

    String _nombre , _foto  , _email , _city , _company ;
    int _iduser;
    ProgressDialog progressDialog = null;
    RecyclerView recycler;
    Context miContex;
    EventActivity ref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        miContex = this;
        ref = this;
        recycler = (RecyclerView)  findViewById(R.id.recycler);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getName();
            _city = Singleton.getInstance().getName();
            _company = Singleton.getInstance().getName();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        GetEventsByUsuer();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:

                    Intent intent = new Intent(EventActivity.this, ProfileActivity.class);

                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);

                    break;

            }
        }
        return(super.onOptionsItemSelected(item));
    }
    public  void GetEventsByUsuer()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(EventActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetEventsByUser"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        JSONArray ListEvents = response.getJSONArray("arrayEvents");
                        if( ListEvents.length() > 0 )
                        {
                            List<EventLegend> items = new ArrayList<>();
                            for (int j = 0 ; j < ListEvents.length() ; j++ )
                            {
                                JSONObject event = ListEvents.getJSONObject(j);
                                EventLegend e = new EventLegend( event.getString("Picture") , event.getString("Name") , event.getString("strContent") ,  event.getString("ContentId") );

                                items.add(e);
                            }
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
                            recycler.setLayoutManager(linearLayoutManager);

                            final EventLegendAdapter listAdapter = new EventLegendAdapter(items , ref  );
                            recycler.setAdapter(listAdapter);
                        }

                    }

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //GetPublishByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        GetEventsByUsuer();
    }
    public  void goToComment( String contentID )
    {
        Intent intent = new Intent(EventActivity.this, CommentActivity.class);
        intent.putExtra("contentID", contentID );
        intent.putExtra("userID", _iduser );
        startActivity(intent);
    }

}
