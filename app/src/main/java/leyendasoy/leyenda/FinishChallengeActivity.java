package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.CommentAdapter;
import adparter_class.Answer;
import adparter_class.Comment;
import clases.MiHelper;
import clases.Question;
import clases.Singleton;

public class FinishChallengeActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog progressDialog = null;
    Context miContex;
    FinishChallengeActivity ref;
    List<Comment> items;
    TextView contentText;
    ImageView imageView;
    VideoView videoView;
    RelativeLayout play , pause;
    SeekBar seekbar;
    String _nombre , _foto  , _email , _city , _company ;
    int _iduser;
    int PointObtained = 0 , QuestionCorrect = 0,  ChallengeID , ContentID;
    TextView title ;
    Button shareResult ,  btnPoints ,  btnCorrect;

    LinearLayout tab_inicio , tab_leyenda ,  tab_retos;

    RelativeLayout r1 , r2 , r3;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_challenge);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        PointObtained = extras.getInt("PointsObtained");
        ChallengeID = extras.getInt("ChallengeID");
        ContentID = extras.getInt("ContentId");
        PointObtained = extras.getInt("PointObtained");
        QuestionCorrect = extras.getInt("QuestionCorrect");

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getName();
            _city = Singleton.getInstance().getName();
            _company = Singleton.getInstance().getName();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        shareResult = ( Button ) findViewById(R.id.shareResult);
        btnPoints = ( Button ) findViewById(R.id.btnPoints);
        btnCorrect = ( Button ) findViewById(R.id.btnCorrect);

        shareResult.setOnClickListener(this);

        r1 = ( RelativeLayout ) findViewById(R.id.r1);
        r2 = ( RelativeLayout ) findViewById(R.id.r2);
        r3 = ( RelativeLayout ) findViewById(R.id.r3);

        r1.setVisibility(View.GONE);
        r2.setVisibility(View.GONE);
        r3.setVisibility(View.GONE);

        title = ( TextView ) findViewById(R.id.title);
        contentText = ( TextView ) findViewById(R.id.contentText);
        imageView = ( ImageView ) findViewById(R.id.imageView);
        videoView = (VideoView) findViewById(R.id.videoView);

        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        contentText.setVisibility(View.GONE);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        miContex = this;
        ref = this;

        play.setOnClickListener(this);
        pause.setOnClickListener(this);

        intent.putExtra("PointObtained", PointObtained );
        intent.putExtra("QuestionCorrect", QuestionCorrect );

        GetChallengerByContentId();

        btnPoints.setText( btnPoints.getText() + " " + PointObtained );
        btnCorrect.setText( btnCorrect.getText() + " " + QuestionCorrect );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(Gravity.LEFT);
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(FinishChallengeActivity.this, ProfileActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(FinishChallengeActivity.this, MessageActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;
            }
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(FinishChallengeActivity.this, WallActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(FinishChallengeActivity.this, LegendActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(FinishChallengeActivity.this, ChallengeActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );
            startActivity(intent);
        }
        else if( v.getId() == R.id.shareResult )
        {
            LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popUpView = li.inflate(R.layout.poput_share_result,null);
            PopupWindow mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAsDropDown(v , 0, 0);

        }
        else if( v.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
    }

    public void GetChallengerByContentId()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(FinishChallengeActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentID) );

        Log.i("Parametros : " , params.toString() );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ChallengeAPI/GetChallengerByContentId"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        items = new ArrayList<>();

                        JSONArray contents = response.getJSONArray("ArrayChallenger");
                        JSONObject content = contents.getJSONObject(0);

                        title.setText( content.getString("ChallegeTitle") );
                        ChallengeID = content.getInt("ChallengeId");


                        if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentText )
                        {
                            r1.setVisibility(View.VISIBLE);
                            contentText.setVisibility(View.VISIBLE);
                            contentText.setText( content.getString("strContent") );
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentVideo )
                        {
                            r3.setVisibility(View.VISIBLE);
                            videoView.setVisibility(View.VISIBLE);
                            Uri uri = Uri.parse(  content.getString("FilePath") );
                            videoView.setVideoURI( uri );

                            videoView.seekTo(100);
                            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                            {
                                @Override
                                public void onPrepared(MediaPlayer mp)
                                {
                                    seekbar.setVisibility(View.VISIBLE);
                                    play.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentImage )
                        {
                            imageView.setVisibility(View.VISIBLE);
                            r2.setVisibility(View.VISIBLE);

                        }

                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetChallengerByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
}
