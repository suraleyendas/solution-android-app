package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import clases.MiHelper;
import clases.Singleton;

public class ExpressYourselfActivity extends AppCompatActivity
{

    String _nombre , _foto  , _email , _city , _company;
    int _iduser;
    EditText txtExpresate;
    ProgressDialog progressDialog = null;
    Context miContex;
    Boolean enable = false;
    Menu menu = null;
    ImageView photoProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_express_yourself);

        miContex = this;

        txtExpresate = (EditText ) findViewById(R.id.txtExpresate);

        txtExpresate.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if( menu != null )
                {
                    if( s.toString().trim().length() <= 0 )
                    {
                        menu.findItem(R.id.expresate).setEnabled(false);
                        enable = false;
                    }
                    else
                    {
                        menu.findItem(R.id.expresate).setEnabled(true);
                        enable = true;
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        photoProfile = ( ImageView ) findViewById(R.id.photoProfile);
        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getName();
            _city = Singleton.getInstance().getName();
            _company = Singleton.getInstance().getName();


        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        GetProfile( _iduser );

        TextView txtNombre = ( TextView ) findViewById(R.id.txtNombre);
        txtNombre.setText( _nombre );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_express_yourself, menu);
        this.menu = menu;
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else if( item.getTitle().toString().compareToIgnoreCase("Publicar") == 0 )
        {
            if( txtExpresate.getText().toString().length() > 0 )
            {
                menu.findItem(R.id.expresate).setEnabled(false);
                SaveContent( txtExpresate.getText().toString() );
            }
            else
            {
                menu.findItem(R.id.expresate).setEnabled(true);
                txtExpresate.setError("Este campo no puede estar vacio");
            }
        }
        return (super.onOptionsItemSelected(item));
    }
    public void  SaveContent( String text )
    {
        final MiHelper miHelper = new MiHelper();
        progressDialog = new ProgressDialog(ExpressYourselfActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("File", "");
        params.put("Content", text );
        params.put("IsActive", "true");
        params.put("ContentParent", "");
        params.put("ContentTypeId", "1");
        JSONObject jsonObj = new JSONObject(params);


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveContent/"  , jsonObj , new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                progressDialog.dismiss();
                try
                {
                    menu.findItem(R.id.expresate).setEnabled(true);



                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        Toast.makeText(miContex, "Publicado exitosamente", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(ExpressYourselfActivity.this, WallActivity.class);
                        intent.putExtra("userId", _iduser );
                        intent.putExtra("urlFoto", _foto );
                        intent.putExtra("txtNombre", _nombre );
                        intent.putExtra("txtEmail", _email );
                        intent.putExtra("company", _company );
                        intent.putExtra("city", _city );

                        startActivity(intent);
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error");
                        builder.setMessage("No se pudo obtener datos del usuario.").setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                            }
                        }).show();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //GoToProfile( userIdFollow ,isFollow );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        queue.add(getRequest);

    }
    public void GetProfile( final int iduser )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ExpressYourselfActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(iduser) );
        params.put("Width", "800" );
        JSONObject jsonObj = new JSONObject(params);


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser/", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Log.i("Profile " , response.toString() );
                    boolean result = (boolean) response.getBoolean("Result");
                    if( result == true )
                    {
                        byte[] decodedString = Base64.decode(response.getString("Picture"), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        photoProfile.setImageBitmap(decodedByte);

                    }


                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetProfile( iduser );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
}
