package leyendasoy.leyenda;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import clases.DbHelper;
import clases.PrefManager;
import clases.Singleton;

public class SplashActivity extends Activity
{
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;
    TextView count;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PrefManager prefManager = new PrefManager(this);
        if ( prefManager.isFirstTimeLaunch() == false )
        {
            Intent intent = new Intent(SplashActivity.this, WallActivity.class);
            startActivity(intent);
        }
        else
            init();
        /*
        try
        {
            Singleton.getInstance().setContextDB( this );

            if(  Singleton.getInstance().getUserID() > 0 )
            {
                Intent intent = new Intent(SplashActivity.this, WallActivity.class);
                startActivity(intent);
            }
            else
            {


            }
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        */

    }
    public  void  init()
    {
        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                Intent mainIntent = new Intent().setClass( SplashActivity.this , SliderActivity.class);
                startActivity(mainIntent);
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, 1500 );
    }
    /*
    @Override

    public void onResume()
    {
        super.onResume();
        init();
    }
    */
}
