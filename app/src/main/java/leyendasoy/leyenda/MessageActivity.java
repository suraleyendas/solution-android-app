package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.FollowedAdapter;
import adapters.MessageAdapter;
import adparter_class.City;
import adparter_class.Followed;
import adparter_class.Message;
import clases.MiHelper;
import clases.Singleton;
import clases.Util;

public class MessageActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog progressDialog = null;
    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    MessageAdapter listAdapter = null;
    public int SelectedUser = 0;


    List<Message> items;

    LinearLayout tab_inicio , tab_leyenda;
    List<Message> list;
    RecyclerView recycler;
    EditText txtSearch;
    Util util;

    Context miContex;
    MessageActivity ref;

    private View view;
    private boolean add = false;
    private Paint p = new Paint();

    String  _nombre ,_foto ,_email,_city,_company ;
    int _iduser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            Singleton.getInstance().setMessageActivity(this);
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        miContex = this;
        ref = this;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getName();
            _city = Singleton.getInstance().getName();
            _company = Singleton.getInstance().getName();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        txtSearch = ( EditText ) findViewById(R.id.txtSearch );


        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        util = new Util();
        if (util.compruebaConexion(this))
        {
            GetMessages( page , false ) ;
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error en la conexión");
            builder.setMessage("Error no tienes conexión a internet.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    GetMessages( page , false );
                }
            });
            builder.show();
        }

        recycler = (RecyclerView) findViewById(R.id.recycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources( R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    GetMessages( page , false );
                /*
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       // setupAdapter();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
                */
            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                GetMessages( page , true );
                        }
                    }
                }
            }
        });

        /*

        list = new ArrayList<>();
        list.add( new Message("1","photo" , "Ángela Rodriguez" , "Hola Sofi, estaba navegando y vi estos tips, míralos!"));
        list.add( new Message("2","photo" , "Anónimo" , "Hola, me gustaría sugerirte que…"));
        list.add( new Message("3","photo" , "Grupo SURA ARL" , "Chicos Hola, ayer compartieron esta actividad de 5K leyendas! ¿Vamos?"));

        recycler = ( RecyclerView ) findViewById(R.id.recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        listAdapter = new MessageAdapter(list,this);
        recycler.setAdapter(listAdapter);
        */

        txtSearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
        initSwipe();
    }


    public void GetMessages( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(MessageActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser));
        params.put("Width", "50");
        params.put("Height", "50");
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", "5");
        params.put("SearchQuery", "");

        Log.i("Params : " , params.toString() );


        JSONObject jsonObj = new JSONObject(params);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUserFollow", jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.i("Response : " , response.toString() );

                    loading = false;
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {

                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }
                        JSONArray users = response.getJSONArray("UserArray");


                        JSONObject obj = new JSONObject();

                        String ids = "";

                        for (int i = 0; i < users.length(); i++)
                        {
                            JSONObject user = users.getJSONObject(i);
                            items.add( new Message( user.getInt("UserId") , user.getString("Picture")  , user.getString("Name") , ""));
                            ids += user.getInt("UserId") + ",";
                        }
                        ids += _iduser;
                        obj.put("ListIds" , ids );
                        obj.put("UserId" , _iduser  );



                        Singleton.getInstance().GetLastMessages( obj );

                        if( listAdapter == null )
                        {
                            listAdapter = new MessageAdapter(items, miContex  );
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Message> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }

                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);



                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetMessages( page , add );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            Intent intent;
            switch (item.getItemId())
            {

                case R.id.user:
                    intent = new Intent(MessageActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false  );
                    intent.putExtra("isFollow", false  );

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(MessageActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;
                case R.id.notification:
                    intent = new Intent(MessageActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    break;

            }
        }

        return (super.onOptionsItemSelected(item));
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(MessageActivity.this, WallActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(MessageActivity.this, LegendActivity.class);
            intent.putExtra("userId", _iduser );
            intent.putExtra("urlFoto", _foto );
            intent.putExtra("txtNombre", _nombre );
            intent.putExtra("txtEmail", _email );
            intent.putExtra("company", _company );
            intent.putExtra("city", _city );

            startActivity(intent);
        }
    }
    private void initSwipe()
    {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
            {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
            {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT)
                {
                    listAdapter.removeItem(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
            {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE)
                {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0)
                    {
                       /*
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_edit_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                        */

                    }
                    else
                    {
                        p.setColor(Color.parseColor("#D32F2F"));

                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_forever_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recycler);
    }
    public void AddChatLastMessageFrom( JSONArray messages ) throws JSONException
    {
            int items = listAdapter.UpdateLastMessage( messages );
            refreshMyRecyclerView();
    }
    public void refreshMyRecyclerView()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                MessageActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listAdapter.notifyDataSetChanged();
                    }
                });

            }
        }).start();
    }
}
