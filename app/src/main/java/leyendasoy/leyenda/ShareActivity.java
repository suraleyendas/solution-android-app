package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.CommentAdapter;
import adparter_class.Comment;
import clases.CustomVolleyRequest;
import clases.MiHelper;
import clases.Singleton;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener
{

    Menu menu = null;
    ProgressDialog progressDialog = null;
    Context miContex;
    CommentActivity ref;
    ImageView photoProfile;
    List<Comment> items;
    TextView txtNombre , contentText ,  txtComment;
    NetworkImageView imageView;
    VideoView videoView;
    LinearLayout linearComment;
    RecyclerView recycler;
    int ContentID;
    RelativeLayout play , pause;
    SeekBar seekbar;
    int _iduser , Redirect ;
    private ImageLoader imageLoader;
    int ContentTypeId;
    String FilePath;
    Boolean enable;
    String ContentParent = "";
    String ContentDescription = "";
    int UserTo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        photoProfile = (ImageView) findViewById(R.id.photoProfile);
        txtComment = ( TextView ) findViewById(R.id.txtComment);
        txtNombre = ( TextView ) findViewById(R.id.txtNombre);
        contentText = ( TextView ) findViewById(R.id.contentText);
        imageView = (NetworkImageView) findViewById(R.id.imageView);
        videoView = ( VideoView ) findViewById(R.id.videoView);
        linearComment = ( LinearLayout ) findViewById(R.id.linearComment);
        recycler = (RecyclerView) findViewById(R.id.recycler);

        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        contentText.setVisibility(View.GONE);

        play = ( RelativeLayout ) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = ( SeekBar ) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        miContex = this;

        play.setOnClickListener(this);
        pause.setOnClickListener(this);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        ContentID = extras.getInt("ContentID");
        Redirect = extras.getInt("Redirect");
        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _iduser = Singleton.getInstance().getUserID();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        GetPublishByContentId();

    }
    public void GetPublishByContentId()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ShareActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentID) );
        params.put("Top", "100");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetPublishByContentId"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        items = new ArrayList<>();

                        JSONArray contents = response.getJSONArray("arrayContent");
                        JSONObject content = contents.getJSONObject(0);
                        ContentTypeId = content.getInt("ContentTypeId");
                        UserTo = content.getInt("UserId");

                        FilePath = content.getString("FilePath");
                        if( content.getBoolean("IsChallenge") )
                        {
                            recycler.setVisibility(View.GONE);
                            txtComment.setVisibility(View.GONE);

                            if (content.getInt("ContentParentId") == 0)
                                ContentParent = String.valueOf( content.getInt("ContentId") );
                            else
                                ContentParent =  String.valueOf( content.getInt("ContentParent") );
                        }

                        byte[] decodedString = Base64.decode( content.getString("Picture").toString() , Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        photoProfile.setImageBitmap(decodedByte);

                        txtNombre.setText( content.getString("Name") );

                        if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentText )
                        {
                            contentText.setVisibility(View.VISIBLE);
                            contentText.setText( content.getString("strContent") );
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentVideo )
                        {
                            videoView.setVisibility(View.VISIBLE);
                            Uri uri = Uri.parse(  content.getString("FilePath") );
                            videoView.setVideoURI( uri );

                            videoView.seekTo(100);
                            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                            {
                                @Override
                                public void onPrepared(MediaPlayer mp)
                                {
                                    seekbar.setVisibility(View.VISIBLE);
                                    play.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentEvent )
                        {

                            if( content.getInt("Event_Type") == miHelper.contentText )
                            {
                                contentText.setVisibility(View.VISIBLE);
                                contentText.setText( content.getString("strContent") );
                            }
                            else if( content.getInt("Event_Type") == miHelper.contentVideo )
                            {
                                videoView.setVisibility(View.VISIBLE);
                                Uri uri = Uri.parse(  content.getString("FilePath") );
                                videoView.setVideoURI( uri );
                                videoView.seekTo(100);

                                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                                {
                                    @Override
                                    public void onPrepared(MediaPlayer mp)
                                    {
                                        seekbar.setVisibility(View.VISIBLE);
                                        play.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                            else if( content.getInt("Event_Type") == miHelper.contentImage )
                            {
                                imageView.setVisibility(View.VISIBLE);
                                imageView.setBackground(null);
                                imageLoader = CustomVolleyRequest.getInstance(miContex).getImageLoader();
                                imageLoader.get( content.getString("FilePath") , ImageLoader.getImageListener(imageView, R.drawable.sin_imagen , R.drawable.photo_error));
                                imageView.setImageUrl(content.getString("FilePath"), imageLoader);
                            }
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentImage )
                        {
                            imageView.setVisibility(View.VISIBLE);
                            imageView.setBackground(null);
                            imageLoader = CustomVolleyRequest.getInstance(miContex).getImageLoader();
                            imageLoader.get( content.getString("FilePath") , ImageLoader.getImageListener(imageView, R.drawable.sin_imagen , R.drawable.photo_error));
                            imageView.setImageUrl(content.getString("FilePath"), imageLoader);

                        }

                        if( content.getBoolean("IsChallenge") )
                        {
                            JSONArray ListComment = content.getJSONArray("ListComment");
                            if (ListComment.length() > 0) {
                                List<Comment> commentList = new ArrayList<>();
                                for (int j = 0; j < ListComment.length(); j++) {
                                    JSONObject comment = ListComment.getJSONObject(j);
                                    Comment c = new Comment(comment.getString("Picture"), comment.getString("FullName"), comment.getString("StrComment"));
                                    commentList.add(c);
                                }
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
                                recycler.setLayoutManager(linearLayoutManager);

                                final CommentAdapter listAdapter = new CommentAdapter(commentList);
                                recycler.setAdapter(listAdapter);
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetPublishByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void SharePublish( String contentID , String ContentType )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ShareActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", contentID );
        params.put("UserShareId", "" );
        params.put("UserContentTypeId", ContentType  );
        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SharePublish"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {

                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if( result == true )
                    {
                        if( Redirect == 1 )
                        {
                            Intent intent = new Intent(ShareActivity.this, WallActivity.class);
                            intent.putExtra("userId", _iduser);
                            startActivity(intent);
                        }
                        else if( Redirect == 2 )
                        {
                            Intent intent = new Intent(ShareActivity.this, ChallengeActivity.class);
                            intent.putExtra("userId", _iduser);
                            startActivity(intent);
                        }
                        Toast.makeText(miContex, "Publicación compartida exitosamente.", Toast.LENGTH_SHORT).show();

                        int _iduser = Singleton.getInstance().getUserID();
                        String _name = Singleton.getInstance().getName();
                        Singleton.getInstance().SaveUserNotification( UserTo , _iduser ,  "A " + _name +  " le ha gustado tu publicación" , true , miHelper.NotificationContentShare , ContentID );

                    }
                    else
                    {
                        Toast.makeText(miContex, "No se pudo compartir la publicación.", Toast.LENGTH_SHORT).show();

                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //getFollowed();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void SaveContent( String texto )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ShareActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("File", FilePath );
        params.put("Content", texto );
        params.put("IsActive", "true" );
        params.put("ContentParent", "" + ContentParent  );
        params.put("ContentTypeId",  String.valueOf( ContentTypeId ) );
        params.put("ContentDescription",  ContentDescription );


        Log.i("Save content " , params.toString() );

        JSONObject jsonObj = new JSONObject(params);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveContent"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Log.i("Result " , response.toString() );

                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        String ContentId = response.getString("ContentId");
                        SharePublish( ContentId , String.valueOf( ContentTypeId ) );
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //getFollowed();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_shared, menu);
        this.menu = menu;

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else if( item.getTitle().toString().compareToIgnoreCase("Compartir") == 0 )
        {
            MiHelper miHelper = new MiHelper();
            String texto;

            if( ContentTypeId == miHelper.contentText )
            {
                ContentDescription = txtComment.getText().toString().trim();
                texto = contentText.getText().toString().trim();
            }
            else
            {
                texto = txtComment.getText().toString().trim();
            }
            if( texto.length() > 0 )
            {
                SaveContent( texto );

                if( Redirect == 1 )
                {
                    Intent intent = new Intent(ShareActivity.this, WallActivity.class);
                    intent.putExtra("userId", _iduser);
                    startActivity(intent);
                }
                else if( Redirect == 2 )
                {
                    Intent intent = new Intent(ShareActivity.this, ChallengeActivity.class);
                    intent.putExtra("userId", _iduser);
                    startActivity(intent);
                }

            }
            else
            {
                txtComment.setText("Esta campo es obligatorio");
            }
        }
        return (super.onOptionsItemSelected(item));
    }
    @Override
    public void onClick(View view)
    {

         if( view.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
    }
}
