package leyendasoy.leyenda;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import clases.PrefManager;

public class SliderActivity extends Activity
{
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip;
    private Button btnNext , btn_begin;
    private PrefManager prefManager;

    int MY_PERMISSIONS_CAMERA = 1;
    int MY_PERMISSIONS_READ_EXTERNAL_EXTORAGE = 1;

    private void verifyPermissionCamera()
    {
        //WRITE_EXTERNAL_STORAGE tiene implícito READ_EXTERNAL_STORAGE
        //porque pertenecen al mismo grupo de permisos
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)  != PackageManager.PERMISSION_GRANTED)
        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA))
            {
            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA , Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.RECORD_AUDIO }, MY_PERMISSIONS_CAMERA);


            }
        }
    }
    private void verifyPermissionReadExternalStorage()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)  != PackageManager.PERMISSION_GRANTED)
        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE))
            {

            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_READ_EXTERNAL_EXTORAGE);

            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == MY_PERMISSIONS_CAMERA )
        {

            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {

            }
            else
            {

            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        verifyPermissionCamera();
        launchHomeScreen();



        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        dotsLayout.setGravity(Gravity.CENTER);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btn_begin = (Button) findViewById(R.id.btn_begin);
        btn_begin.setVisibility(View.GONE);


        layouts = new int[]
        {
                R.layout.welcome_side1,
                R.layout.welcome_side2,
                R.layout.welcome_side3,
                R.layout.welcome_side4,
                R.layout.welcome_side5
        };

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                int current = getItem(+1);
                if (current < layouts.length)
                {
                    viewPager.setCurrentItem(current);

                }
            }
        });
        btn_begin.setOnClickListener( new View.OnClickListener()
        {
           @Override
           public  void onClick( View v )
           {
               launchHomeScreen();

           }
        });

    }

    private void addBottomDots(int currentPage)
    {
        dots = new TextView[layouts.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++)
        {

            dots[i] = new TextView(this);
            dots[i].setHeight(24);
            dots[i].setWidth(24);
            LinearLayout.LayoutParams layoutMargins = new LinearLayout.LayoutParams(ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.WRAP_CONTENT);
            layoutMargins.setMargins(5, 0, 5, 0);
            dots[i].setLayoutParams(layoutMargins);
            dots[i].setBackgroundResource(R.drawable.border_item_slide_inactive);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setBackgroundResource(R.drawable.border_item_slide_active);
    }

    private int getItem(int i)
    {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen()
    {
        startActivity(new Intent(SliderActivity.this, WelcomeActivity.class));

    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position)
        {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1)
            {

                btn_begin.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.GONE);
                btnSkip.setVisibility(View.GONE);

            }
            else
            {
                btn_begin.setVisibility(View.GONE);
                btnSkip.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.VISIBLE);

            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2)
        {

        }

        @Override
        public void onPageScrollStateChanged(int arg0)
        {

        }
    };
    private void changeStatusBarColor()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
    public class MyViewPagerAdapter extends PagerAdapter
    {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter()
        {
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            Typeface customFontArial = Typeface.createFromAsset( getAssets() , "fonts/Arial.ttf");

            if( position == 0 )
            {
                TextView txt1 = (TextView) view.findViewById(R.id.titulo1);
                TextView txt2 = (TextView) view.findViewById(R.id.titulo2);
                TextView txt3 = (TextView) view.findViewById(R.id.titulo3);

                txt1.setTypeface( customFontArial );
                txt2.setTypeface( customFontArial );
                txt3.setTypeface( customFontArial );
            }
            if( position == 1 )
            {
                TextView txt1 = (TextView) view.findViewById(R.id.titulo1);
                txt1.setTypeface( customFontArial );

            }
            if( position == 2 )
            {
                TextView txt1 = (TextView) view.findViewById(R.id.titulo1);
                txt1.setTypeface( customFontArial );
            }
            if( position == 3 )
            {
                TextView txt1 = (TextView) view.findViewById(R.id.titulo1);
                txt1.setTypeface( customFontArial );
            }
            if( position == 4 )
            {
                TextView txt1 = (TextView) view.findViewById(R.id.titulo1);
                TextView txt2 = (TextView) view.findViewById(R.id.titulo2);

                txt1.setTypeface( customFontArial );
                txt2.setTypeface( customFontArial );
            }
            container.addView(view);

            return view;
        }

        @Override
        public int getCount()
        {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }
    }


}
