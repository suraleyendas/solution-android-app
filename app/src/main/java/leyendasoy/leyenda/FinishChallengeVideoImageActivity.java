package leyendasoy.leyenda;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.VideoView;

import java.net.URISyntaxException;

import clases.Singleton;


public class FinishChallengeVideoImageActivity extends AppCompatActivity implements View.OnClickListener {

    String _nombre , _foto  , _email , _city , _company ;
    int _iduser;
    int  ContentID,  ChallengeID , _type , ChallengeTypeID;

    RelativeLayout play , pause;
    SeekBar seekbar;
    ImageView imgPreview;
    VideoView videoView;
    RelativeLayout layoutImage , layoutVideo;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_challenge_video_image);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        ChallengeID = extras.getInt("ChallengeID");
        ChallengeTypeID = extras.getInt("ChallengeTypeID");
        ContentID = extras.getInt("ContentID");
        _type = extras.getInt("type");

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }


        layoutImage = ( RelativeLayout ) findViewById(R.id.layoutImage );
        layoutVideo = ( RelativeLayout ) findViewById(R.id.layoutVideo );
        layoutImage.setVisibility(View.GONE);
        layoutVideo.setVisibility(View.GONE);

        videoView = ( VideoView ) findViewById(R.id.videoView);
        imgPreview = ( ImageView ) findViewById(R.id.imageView);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);


        if(  _type == 1 )
        {
            try
            {
                imgPreview.setImageBitmap(Singleton.getInstance().getBitmap());
                imgPreview.setVisibility(View.VISIBLE);
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            try
            {
                videoView.setBackground(null);
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoPath( Singleton.getInstance().getVideoPath() );
                videoView.seekTo(100);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                {
                    @Override
                    public void onPrepared(MediaPlayer mp)
                    {
                        seekbar.setVisibility(View.VISIBLE);
                        play.setVisibility(View.VISIBLE);
                    }
                });
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
            intent = new Intent(FinishChallengeVideoImageActivity.this, ChallegeDetailActivity.class);

            intent.putExtra("ChallengeTypeID", ChallengeTypeID);
            intent.putExtra("ContentID", ContentID);
            intent.putExtra("ChallengeID", ChallengeID);

            startActivity(intent);
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(FinishChallengeVideoImageActivity.this, ProfileActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(FinishChallengeVideoImageActivity.this, MessageActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;
            }
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
    }
}
