package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.BoringLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.AnswerAdapter;
import adparter_class.Comment;
import adparter_class.Answer;
import clases.CustomVolleyRequest;
import clases.MiHelper;
import clases.NonSwipeableViewPager;
import clases.Question;
import clases.Singleton;

public class ChallengeQuestionActivity extends AppCompatActivity implements View.OnClickListener
{
    ProgressDialog progressDialog = null;
    Context miContex;
    ChallengeQuestionActivity ref;
    List<Comment> items;
    TextView contentText;
    NetworkImageView imageView;
    VideoView videoView;
    RelativeLayout play , pause;
    SeekBar seekbar;
    String _nombre , _foto  , _email , _city , _company;
    int _iduser;
    int ChallengeID ,  ContentID;
    List<Question> QuestionsList = null;
    int QuestionSelected = 0;
    int QuestionCorrect = 0;
    int PointObtained = 0;

    TextView txtPoints , txtTitle;



    LinearLayout expand_collapse ;
    ImageView icon_expand_collapse;
    Boolean IsParticipated;
    Button BtnIsParticipated;

    private int[] layouts;
    private TextView[] dots;
    private LinearLayout dotsLayout;
    private NonSwipeableViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    RelativeLayout r1 , r2 , r3;
    LinearLayout layoutNext;
    int position = 0;
    List<Boolean> listResponse;
    TextView txtStatusResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_question);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        listResponse = new ArrayList<>();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        ContentID = extras.getInt("ContentID");

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getPhoto();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getEmail();
            _city = Singleton.getInstance().getCity();
            _company = Singleton.getInstance().getCompany();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        txtStatusResponse = ( TextView ) findViewById( R.id.txtStatusResponse );
        txtStatusResponse.setText("");

        txtPoints = (TextView) findViewById(R.id.txtPoints);
        txtTitle = (TextView) findViewById(R.id.txtTitle);

        r1 = ( RelativeLayout ) findViewById(R.id.r1);
        r2 = ( RelativeLayout ) findViewById(R.id.r2);
        r3 = ( RelativeLayout ) findViewById(R.id.r3);

        r1.setVisibility(View.GONE);
        r2.setVisibility(View.GONE);
        r3.setVisibility(View.GONE);



        layoutNext = ( LinearLayout ) findViewById(R.id.layoutNext);
        layoutNext.setOnClickListener(this);

        viewPager = (NonSwipeableViewPager) findViewById(R.id.view_pager);

        viewPager.beginFakeDrag();

        contentText = ( TextView ) findViewById(R.id.contentText);
        imageView = ( NetworkImageView ) findViewById(R.id.imageView);
        videoView = (VideoView) findViewById(R.id.videoView);

        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        contentText.setVisibility(View.GONE);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        miContex = this;
        ref = this;

        play.setOnClickListener(this);
        pause.setOnClickListener(this);

        GetChallengerByContentId();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
             onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(ChallengeQuestionActivity.this, ProfileActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(ChallengeQuestionActivity.this, MessageActivity.class);
                    intent.putExtra("userId", _iduser);
                    intent.putExtra("urlFoto", _foto);
                    intent.putExtra("txtEmail", _email);
                    intent.putExtra("txtNombre", _nombre);
                    intent.putExtra("city", _city);
                    intent.putExtra("company", _company);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;
            }
        }
        return (super.onOptionsItemSelected(item));
    }
    public void SaveAnswer( int AnswerId ,  Boolean IsCorrect )
    {
        QuestionsList.get(position).setSelected(true);
        QuestionSelected++;
        Boolean IsLastQuestion = false;

        if( IsCorrect )
            QuestionCorrect++;

        if( IsCorrect )
        {
            txtStatusResponse.setText("¡Acertaste, felicitaciones!");
            txtStatusResponse.setTextColor(Color.parseColor("#056499") );
        }
        else
        {
            txtStatusResponse.setText("La respuesta es incorrecta");
            txtStatusResponse.setTextColor(Color.parseColor("#f00033") );
        }

        if( QuestionSelected == QuestionsList.size() )
        {
            IsLastQuestion = true;

        }
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ChallengeQuestionActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("ChallengeId", String.valueOf(ChallengeID) );
        params.put("AnswerId", String.valueOf(AnswerId) );
        params.put("UserId", String.valueOf(_iduser) );
        params.put("IsCorrect", String.valueOf(IsCorrect) );
        params.put("IsLastQuestion", String.valueOf(IsLastQuestion) );
        JSONObject jsonObj = new JSONObject(params);

        final Boolean finalIsLastQuestion = IsLastQuestion;
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ChallengeAPI/SaveChallengeQueryByUser"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    Log.i("Response Question : " , response.toString() );

                    if (result == true)
                    {
                        if( finalIsLastQuestion )
                        {
                            PointObtained = response.getInt("PointsObtained");
                            GoToFinishChallenge();
                        }
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetChallengerByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        queue.add(getRequest);
    }
    public void GetChallengerByContentId()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ChallengeQuestionActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentID) );

        Log.i("Parametros : " , params.toString() );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ChallengeAPI/GetChallengerByContentId"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        items = new ArrayList<>();

                        JSONArray contents = response.getJSONArray("ArrayChallenger");
                        JSONObject content = contents.getJSONObject(0);

                        txtTitle.setText( content.getString("ChallegeTitle").toUpperCase() );
                        txtPoints.setText( content.getString("Points") + " Pts.");

                        ChallengeID = content.getInt("ChallengeId");

                        JSONArray jsonQuestions  = content.getJSONArray("ArrayQuestion");
                        if( jsonQuestions.length() > 0 )
                        {
                            QuestionsList = new ArrayList<>();

                            for ( int i = 0 ; i < jsonQuestions.length() ; i++ )
                            {
                                 JSONObject jsonQuestion = jsonQuestions.getJSONObject(i);

                                 JSONArray jsonAnswers = jsonQuestion.getJSONArray("ListAnswer");
                                 List<Answer> listAnswers = new ArrayList<>();
                                 if( jsonAnswers.length() > 0 )
                                 {
                                     for ( int j = 0 ; j < jsonAnswers.length() ; j++ )
                                     {
                                         JSONObject jsonAnswer = jsonAnswers.getJSONObject(j);
                                         listAnswers.add( new Answer( jsonAnswer.getInt("AnswerId") , jsonAnswer.getString("StrAnswer") , jsonAnswer.getBoolean("IsCorrect"), "#ffffff", false , jsonQuestion.getInt("QuestionId") , 0 )  );
                                     }
                                 }
                                 QuestionsList.add( new Question( jsonQuestion.getInt("QuestionId") , jsonQuestion.getString("StrQuestion") , false , listAnswers ));

                                 layouts = new int[QuestionsList.size()];

                                 for ( int x = 0 ; x < QuestionsList.size() ; x++ )
                                 {
                                    layouts[x] = R.layout.layout_list_answer;
                                 }

                                 dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
                                 dotsLayout.setGravity(Gravity.CENTER);
                                 addBottomDots(0);

                                 myViewPagerAdapter = new MyViewPagerAdapter( ref );
                                 viewPager.setAdapter(myViewPagerAdapter);
                                 viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
                            }
                        }
                        if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentText )
                        {
                            r1.setVisibility(View.VISIBLE);
                            contentText.setVisibility(View.VISIBLE);
                            contentText.setText( content.getString("strContent") );
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentVideo )
                        {
                            r3.setVisibility(View.VISIBLE);
                            videoView.setVisibility(View.VISIBLE);
                            Uri uri = Uri.parse(  content.getString("FilePath") );
                            videoView.setVideoURI( uri );

                            videoView.seekTo(100);
                            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                            {
                                @Override
                                public void onPrepared(MediaPlayer mp)
                                {
                                    seekbar.setVisibility(View.VISIBLE);
                                    play.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentImage )
                        {
                            imageView.setVisibility(View.VISIBLE);
                            r2.setVisibility(View.VISIBLE);
                            ImageLoader imageLoader = CustomVolleyRequest.getInstance(miContex).getImageLoader();
                            imageLoader.get( content.getString("FilePath") , ImageLoader.getImageListener( imageView , R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
                            imageView.setImageUrl( content.getString("FilePath") , imageLoader);

                            if( QuestionsList == null )
                            {
                                layoutNext.setVisibility(View.GONE);
                                Toast.makeText(miContex, "No se encontraron preguntas", Toast.LENGTH_SHORT).show();
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetChallengerByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public class MyViewPagerAdapter extends PagerAdapter implements View.OnClickListener
    {
        private LayoutInflater layoutInflater;
        private ChallengeQuestionActivity challengeQuestionActivity;

        public MyViewPagerAdapter( ChallengeQuestionActivity challengeQuestionActivity )
        {
            this.challengeQuestionActivity = challengeQuestionActivity;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);

            TextView StrQuestion = ( TextView ) view.findViewById(R.id.StrQuestion);
            StrQuestion.setText( QuestionsList.get(position).getStrQuestion() );


            RecyclerView recycler = (RecyclerView) view.findViewById(R.id.listQuestions);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
            recycler.setLayoutManager(linearLayoutManager);

            AnswerAdapter answerAdapter = new AnswerAdapter( QuestionsList.get(position).getListAnswer() , challengeQuestionActivity );
            recycler.setAdapter(answerAdapter);

            container.addView(view);


            return view;
        }
        @Override
        public int getCount()
        {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }
        @Override
        public void onClick(View v)
        {
            /*
            if( v.getId() == R.id.linearVote )
            {
                if( sliderLegends.get(currentPosition).getVoted() )
                {
                    v.setBackgroundResource(R.drawable.border_white);
                    sliderLegends.get(currentPosition).setVoted( false );
                    SaveUserFollow( sliderLegends.get(currentPosition).getUserId() , false );
                }
                else
                {
                    v.setBackgroundResource(R.drawable.bg_green);
                    SaveUserFollow( sliderLegends.get(currentPosition).getUserId() , true );
                    sliderLegends.get(currentPosition).setVoted( true );
                }
            }
            */
        }
    }
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageSelected(int position)
        {
            addBottomDots(position);
        }
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageScrollStateChanged(int arg0)
        {

        }
    };
    private void addBottomDots(int currentPage)
    {
        dots = new TextView[layouts.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++)
        {
            dots[i] = new TextView(this);
            dots[i].setHeight(24);
            dots[i].setWidth(24);
            LinearLayout.LayoutParams layoutMargins = new LinearLayout.LayoutParams(ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.WRAP_CONTENT);
            layoutMargins.setMargins(5, 0, 5, 0);
            dots[i].setLayoutParams(layoutMargins);
            dots[i].setBackgroundResource(R.drawable.border_item_slide_question_inactive);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setBackgroundResource(R.drawable.border_item_slide_question_active);
    }
    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.layoutNext )
        {
            if( position < QuestionsList.size() )
            {
                if(  QuestionsList.get(position).getSelected() == true )
                {
                    txtStatusResponse.setText("");
                    if( position < layouts.length )
                    {
                        position++;
                        viewPager.setCurrentItem(position, true);
                    }
                }
             }
        }
        else if( v.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }

    }
    public void GoToFinishChallenge()
    {
        Intent intent = new Intent(ChallengeQuestionActivity.this, FinishChallengeActivity.class);
        intent.putExtra("ChallengeID", ChallengeID );
        intent.putExtra("ContentId", ContentID );
        intent.putExtra("PointObtained", PointObtained );
        intent.putExtra("QuestionCorrect", QuestionCorrect );

        startActivity(intent);
    }
}
