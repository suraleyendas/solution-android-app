package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.FaqAdapter;
import adapters.ReportAdapter;
import adparter_class.Faq;
import adparter_class.Report;
import clases.MiHelper;

public class FaqActivity extends AppCompatActivity
{
    Context context;
    ProgressDialog progressDialog = null;
    private RecyclerView recycler;
    FaqActivity ref;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        context = this;
        ref = this;

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        recycler = (RecyclerView) findViewById(R.id.lista);

        GetFAQS();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(FaqActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(FaqActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

                case R.id.notification:
                    intent = new Intent(FaqActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));
    }
    public void GetFAQS() {
        final MiHelper miHelper = new MiHelper();

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(FaqActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "FaqAPI/GetFAQS/1", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("Result");
                    Log.i("Resultado", response.toString());

                    if (result == true)
                    {
                        JSONArray contents = response.getJSONArray("ArrayFAQ");
                        List<Faq> items = new ArrayList<>();

                        for (int i = 0; i < contents.length(); i++)
                        {
                            JSONObject content = contents.getJSONObject(i);
                            items.add(  new Faq("", content.getInt("Id") , content.getString("Question") ,  content.getString("Answer") , false ));
                        }
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ref);
                        recycler.setLayoutManager(linearLayoutManager);
                        final FaqAdapter listAdapter = new FaqAdapter(items,ref);
                        recycler.setAdapter(listAdapter);
                    }

                } catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetFAQS();
                            }
                        }).show();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
}
