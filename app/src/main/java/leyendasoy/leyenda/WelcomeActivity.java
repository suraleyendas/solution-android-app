package leyendasoy.leyenda;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import clases.Util;
import clases.MiHelper;

public class WelcomeActivity extends Activity implements  /*Response.Listener<JSONObject>, Response.ErrorListener,*/ CompoundButton.OnCheckedChangeListener ,View.OnClickListener {
    TextView textView1 , textViewIntro1 , textViewIntro2 , textViewTitleTerm ;
    CheckBox checkBox;
    Button btnAccept;
    ProgressDialog progressDialog;
    Util util;
    Context miContext;
    WebView web_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        miContext = this;

        setContentView(R.layout.activity_welcome);
        textView1 = (TextView) findViewById(R.id.textView2);
        Typeface customFont1 = Typeface.createFromAsset(getAssets(), "fonts/DINPro-Bold.otf");
        textView1.setTypeface(customFont1);

        checkBox = (CheckBox) findViewById(R.id.check_box);
        checkBox.setOnCheckedChangeListener(this);

        Typeface customFontArial = Typeface.createFromAsset(getAssets(), "fonts/Arial.ttf");
        Typeface customFontArialBold = Typeface.createFromAsset(getAssets(), "fonts/Arial Bold.ttf");

        web_view = ( WebView ) findViewById(R.id.web_view);

        textViewIntro1 = (TextView) findViewById(R.id.textViewIntro1);
        textViewIntro1.setTypeface(customFontArial);

        textViewIntro2 = (TextView) findViewById(R.id.textViewIntro2);
        textViewIntro2.setTypeface(customFontArial);

        textViewTitleTerm = (TextView) findViewById(R.id.textViewTitleTerm);

        btnAccept = (Button) findViewById(R.id.btnAccept);

        btnAccept.setOnClickListener(this);

        btnAccept.setEnabled(false);
        checkBox.setEnabled(false);


        if( util.compruebaConexion(this) )
        {
            GetTermAndCondition();
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            builder.setTitle("Error en la conexión");
            builder.setMessage("Error no tienes conexión a internet.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    GetTermAndCondition();
                }
            });
            builder.show();
        }

    }

    public void GetTermAndCondition()
    {
        final MiHelper miHelper = new MiHelper();

        progressDialog = new ProgressDialog(WelcomeActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage( miHelper.loadinMsg );
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "https://api.twitter.com/oauth2/token";
        final String requestBody = "grant_type=client_credentials";

        Map<String, String> params = new HashMap<String, String>();
        params.put("ParameterId", "1");
        params.put("ProgramId", "1");



        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ParameterAPI/TermsAndConditions" , jsonObj , new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    textViewTitleTerm.setText( response.getString("Name") );
                    web_view.loadDataWithBaseURL(null, response.getString("Value"), "text/html", "UTF-8", null);
                    checkBox.setEnabled(true);

                } catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContext );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetTermAndCondition();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", String.format("Basic %s", Base64.encodeToString( String.format("%s:%s", miHelper.getUsername() , miHelper.getPassword() ).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        queue.add(getRequest);
    }
    @Override
    public void onClick(View v)
    {
       if( v.getId() == R.id.btnAccept )
       {
           if( btnAccept.isEnabled() )
             startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
       }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        int id = buttonView.getId();
        if( id == R.id.check_box )
        {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if( isChecked )
            {
                btnAccept.setEnabled(true);
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
                    btnAccept.setBackgroundResource(R.drawable.double_blue);
                else
                    btnAccept.setBackgroundResource(R.drawable.double_blue);
            }
            else
            {
                btnAccept.setEnabled(false);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
                    btnAccept.setBackgroundResource(R.drawable.btn_disabled);
                else
                    btnAccept.setBackgroundResource(R.drawable.btn_disabled);
            }
        }
    }
}
