package leyendasoy.leyenda;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.CommentAdapter;
import adparter_class.Comment;
import clases.CustomVolleyRequest;
import clases.MiHelper;
import clases.Singleton;

public class ChallegeDetailActivity extends AppCompatActivity implements View.OnClickListener {
    ProgressDialog progressDialog = null;
    Context miContex;
    ChallegeDetailActivity ref;
    List<Comment> items;
    TextView  contentText ,  txtComment;
    NetworkImageView imageView;
    VideoView videoView;
    RecyclerView recycler;
    String contentParent;
    RelativeLayout play , pause;
    SeekBar seekbar;
    String _nombre , _foto ,  _email , _city , _company ;
    int _iduser;
    TextView title ;
    TextView description;
    Boolean isLike = false;

    LinearLayout tab_inicio , tab_leyenda , tab_retos;
    LinearLayout  layoutLike , layoutComment , layoutShare , layoutReport;

    Boolean IsParticipated;
    Button BtnIsParticipated;
    MiHelper miHelper;

    TextView countLike ,  countComment;

    int ChallengeTypeID , ChallengeID , ContentID;
    TextView txtPoints , txtTitle;

    RelativeLayout r1 , r2 , r3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challege_detail);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        ContentID = extras.getInt("ContentID");
        ChallengeID = extras.getInt("ChallengeID");
        ChallengeTypeID = extras.getInt("ChallengeTypeID");


        countLike = ( TextView ) findViewById(R.id.countLike);
        countComment = ( TextView ) findViewById(R.id.countComment);

        txtPoints = (TextView) findViewById(R.id.txtPoints);
        txtTitle = (TextView) findViewById(R.id.txtTitle);


        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _nombre = Singleton.getInstance().getName();
            _foto = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();
            _email = Singleton.getInstance().getName();
            _city = Singleton.getInstance().getName();
            _company = Singleton.getInstance().getName();

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        miHelper = new MiHelper();

        r1 = (RelativeLayout) findViewById(R.id.r1);
        r2 = (RelativeLayout) findViewById(R.id.r2);
        r3 = (RelativeLayout) findViewById(R.id.r3);


        layoutLike = (LinearLayout) findViewById(R.id.layoutLike);
        layoutLike.setOnClickListener( this );

        layoutComment = (LinearLayout) findViewById(R.id.layoutComment);
        layoutComment.setOnClickListener( this );

        layoutShare = (LinearLayout) findViewById(R.id.layoutShare);
        layoutShare.setOnClickListener( this );

        layoutReport = (LinearLayout) findViewById(R.id.layoutReport);
        layoutReport.setOnClickListener( this );


        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );


        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);

        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        title = ( TextView ) findViewById(R.id.title);
        description = ( TextView ) findViewById(R.id.description);

        BtnIsParticipated = ( Button ) findViewById(R.id.BtnIsParticipated);

        contentText = ( TextView ) findViewById(R.id.contentText);
        imageView = ( NetworkImageView ) findViewById(R.id.contentImg);
        videoView = (VideoView) findViewById(R.id.videoView);
        recycler = (RecyclerView) findViewById(R.id.recycler);

        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        contentText.setVisibility(View.GONE);

        play = (RelativeLayout) findViewById(R.id.play);
        pause = ( RelativeLayout ) findViewById(R.id.pause);
        seekbar = (SeekBar) findViewById(R.id.seekBarVideo);

        play.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);

        miContex = this;
        ref = this;

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        BtnIsParticipated.setOnClickListener(this);

        GetPublishByContentId();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        Intent intent;
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(ChallegeDetailActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(ChallegeDetailActivity.this, MessageActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));

    }
    public void GetPublishByContentId()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ChallegeDetailActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentID) );
        params.put("Top", "100");
        JSONObject jsonObj = new JSONObject(params);

        Log.i("Params " , params.toString() );

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetPublishByContentId"  , jsonObj, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Log.i("Response " , response.toString() );

                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {

                        items = new ArrayList<>();

                        JSONArray contents = response.getJSONArray("arrayContent");
                        JSONObject content = contents.getJSONObject(0);

                        IsParticipated = content.getBoolean("IsParticipated");
                        IsParticipated = false;


                        countLike.setText( String.valueOf(content.getInt("CountLike")) );
                        countComment.setText( String.valueOf(content.getInt("CountComment")) );

                        txtTitle.setText( content.getString("ChallengeTitle").toUpperCase() );
                        txtPoints.setText( content.getString("ChallengePoint") + " Pts.");

                        isLike = content.getBoolean("IsLike");
                        if( IsParticipated )
                        {
                            BtnIsParticipated.setBackgroundResource(R.drawable.double_blue_participated_xs);
                            BtnIsParticipated.setText("Ya has participado");
                        }
                        else
                        {
                            BtnIsParticipated.setBackgroundResource(R.drawable.double_blue_xs);
                            BtnIsParticipated.setText("Participar");
                        }


                        title.setText( content.getString("ChallengeTitle") );
                        description.setText( content.getString("ChallengeDescription") );

                        if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentText )
                        {
                            contentText.setVisibility(View.VISIBLE);
                            contentText.setText( content.getString("strContent") );
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentVideo )
                        {
                            if( content.getString("FilePath").length() > 0 )
                            {
                                r3.setVisibility(View.VISIBLE);
                                videoView.setBackground(new ColorDrawable(Color.TRANSPARENT));
                                videoView.setVisibility(View.VISIBLE);
                                Uri uri = Uri.parse(content.getString("FilePath"));
                                videoView.setVideoURI(uri);

                                videoView.seekTo(100);
                                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {

                                        seekbar.setVisibility(View.VISIBLE);
                                        play.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                            else
                            {

                                imageView.setBackgroundResource(R.drawable.video_no_disponible);
                                imageView.setVisibility(View.VISIBLE);

                            }
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentEvent )
                        {

                            if( content.getInt("Event_Type") == miHelper.contentText )
                            {
                                contentText.setVisibility(View.VISIBLE);
                                contentText.setText( content.getString("strContent") );
                            }
                            else if( content.getInt("Event_Type") == miHelper.contentVideo )
                            {
                                videoView.setVisibility(View.VISIBLE);
                                Uri uri = Uri.parse(  content.getString("FilePath") );
                                videoView.setVideoURI( uri );
                                videoView.seekTo(100);

                                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                                {
                                    @Override
                                    public void onPrepared(MediaPlayer mp)
                                    {
                                        seekbar.setVisibility(View.VISIBLE);
                                        play.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                            else if( content.getInt("Event_Type") == miHelper.contentImage )
                            {
                                ImageLoader imageLoader = CustomVolleyRequest.getInstance(miContex).getImageLoader();
                                imageLoader.get( content.getString("FilePath") , ImageLoader.getImageListener(imageView, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
                                imageView.setImageUrl( content.getString("FilePath") , imageLoader);
                                imageView.setVisibility(View.VISIBLE);
                                //new CommentActivity.DownloadImageTask((ImageView) imageView ).execute( content.getString("FilePath")  );
                            }
                        }
                        else if( Integer.parseInt(content.getString("ContentTypeId")) == miHelper.contentImage )
                        {
                            ImageLoader imageLoader = CustomVolleyRequest.getInstance(miContex).getImageLoader();
                            imageLoader.get( content.getString("FilePath") , ImageLoader.getImageListener(imageView, R.drawable.sin_imagen , R.drawable.imagen_no_disponible));
                            imageView.setImageUrl( content.getString("FilePath") , imageLoader);
                            imageView.setVisibility(View.VISIBLE);
                        }
                        JSONArray ListComment = content.getJSONArray("ListComment");
                        if( ListComment.length() > 0 )
                        {
                            List<Comment> commentList = new ArrayList<>();
                            for (int j = 0 ; j < ListComment.length() ; j++ )
                            {
                                JSONObject comment = ListComment.getJSONObject(j);
                                Comment c = new Comment( comment.getString("Picture") , comment.getString("FullName") , comment.getString("StrComment") );
                                commentList.add(c);
                            }
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
                            recycler.setLayoutManager(linearLayoutManager);

                            final CommentAdapter listAdapter = new CommentAdapter(commentList);
                            recycler.setAdapter(listAdapter);
                        }

                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetPublishByContentId();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.tab_inicio )
        {
            Intent intent = new Intent(ChallegeDetailActivity.this, WallActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(ChallegeDetailActivity.this, LegendActivity.class);

            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(ChallegeDetailActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }

        else if( v.getId() == R.id.BtnIsParticipated )
        {
            switch ( ChallengeTypeID )
            {
                case 1:
                    goToTakePartUpload( ChallengeID ,  ContentID  , 1 );
                break;
                case 2:
                    goToTakePartUpload( ChallengeID , ContentID  , 2 );
                break;
                case 3:
                    if( IsParticipated == false )
                    {
                        Intent intent = new Intent(ChallegeDetailActivity.this, ChallengeQuestionActivity.class);
                        intent.putExtra("ContentID", ContentID );

                        startActivity(intent);
                    }
                break;
            }


        }
        else if( v.getId() == R.id.play )
        {
            play.setVisibility(View.GONE);

            videoView.start();
            seekbar.setMax( videoView.getDuration() );

            final boolean[] mVideoCompleted = {false};

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer)
                {
                    mVideoCompleted[0] = true;
                    videoView.seekTo(100);
                    play.setVisibility(View.VISIBLE);
                }
            });
            videoView.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (mVideoCompleted[0])
                    {
                        play.setVisibility(View.VISIBLE);
                        videoView.seekTo(100);
                    }
                    if ( videoView.isPlaying())
                    {
                        videoView.pause();
                        play.setVisibility(View.GONE);
                        pause.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                play.setVisibility(View.VISIBLE);
                                pause.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                    return true;
                }
            });
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                int toque = 0;
                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 0;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                    // TODO Auto-generated method stub
                    toque = 1;
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
                {
                    // TODO Auto-generated method stub
                    if( toque == 1 )
                    {
                        videoView.seekTo( progress );
                    }
                }
            });

            Runnable onEverySecond=new Runnable()
            {
                public void run()
                {
                    if(seekbar != null)
                    {
                        seekbar.setProgress(videoView.getCurrentPosition());
                    }
                    if(videoView.isPlaying())
                    {
                        seekbar.postDelayed(this, 10);
                    }
                }
            };
            seekbar.postDelayed(onEverySecond, 10);
        }
        if( v.getId() == R.id.layoutLike )
        {
            View parent = (View) v.getParent();
            TextView countLike = ( TextView ) parent.findViewById(R.id.countLike);

            ImageView like = ( ImageView ) v.findViewById(R.id.like);

            if( isLike == false )
            {
                countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) + 1 ) );
                isLike = true;
                like.setBackgroundResource(R.mipmap.like_blue);
                SaveUserContent( ContentID , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");
            }
            else
            {
                countLike.setText( String.valueOf( Integer.parseInt( countLike.getText().toString()) - 1 ) );
                isLike = false;
                like.setBackgroundResource(R.mipmap.like);
                SaveUserContent( ContentID , "" , String.valueOf(  miHelper.userContentTypeLike ) , "" , "");
            }
        }
        else if( v.getId() == R.id.layoutComment )
        {
            goToComment( ContentID );
        }
        else if( v.getId() == R.id.layoutReport )
        {
            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View popUpView = li.inflate(R.layout.poput_report,null);
            PopupWindow mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAsDropDown(v , 0, 0);

            TextView reportContent = ( TextView ) popUpView.findViewById(R.id.ReportContent);
            reportContent.setOnClickListener(this);
            TextView saveContent = ( TextView ) popUpView.findViewById(R.id.SaveContent);
            saveContent.setOnClickListener(this);

        }
        else if( v.getId() == R.id.ReportContent)
        {
            goToReport( ContentID );
        }
        else if( v.getId() == R.id.SaveContent)
        {
            SaveUserContent( ContentID , "" , String.valueOf(  miHelper.userContentTypeSave ) , "" , "");
            Toast.makeText(this, "Contenido guardado", Toast.LENGTH_SHORT).show();
        }
        else if( v.getId() == R.id.layoutShare )
        {

            LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popUpView = li.inflate(R.layout.poput_share,null);
            PopupWindow mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAsDropDown(v , 0, 0);

            TextView shareWithFriend = ( TextView ) popUpView.findViewById(R.id.ShareWithFriends);
            shareWithFriend.setOnClickListener(this);

            TextView sendAsPrivateMsg = ( TextView ) popUpView.findViewById(R.id.SendAsPrivateMsg);
        }
        else if( v.getId() == R.id.ShareWithFriends)
        {
            goToShare(  String.valueOf( ContentID ) );
        }
        else if( v.getId() == R.id.SendAsPrivateMsg)
        {
            ///Toast.makeText(wallActivity, "SendAsPrivateMsg", Toast.LENGTH_SHORT).show();
        }

    }
    public void SaveUserContent( int ContentId , String UserShareId , String UserContentTypeId , String ReportTypeId , String ReporDescription )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog( miContex );
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(miContex);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("ContentId", String.valueOf(ContentId) );
        params.put("UserShareId", UserShareId );
        params.put("UserContentTypeId", UserContentTypeId );
        params.put("ReportTypeId", ReportTypeId );
        params.put("ReportDescription", ReporDescription );
        params.put("Viewed", ReporDescription );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/SaveUserContent"  , jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.i( "Rs " , response.toString() );
                    progressDialog.dismiss();
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //getFollowed();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public  void goToComment( int ContentID )
    {
        Intent intent = new Intent(ChallegeDetailActivity.this, CommentActivity.class);
        intent.putExtra("ContentID", ContentID );
        startActivity(intent);
    }
    public  void goToReport( int ContentID )
    {

        Intent intent = new Intent(ChallegeDetailActivity.this, ReportActivity.class);
        intent.putExtra("ContentID", ContentID );
        startActivity(intent);
    }
    public  void goToShare(  String contentId )
    {
        Intent intent = new Intent(ChallegeDetailActivity.this, ShareActivity.class);
        intent.putExtra("ContentID", ContentID );
        startActivity(intent);
    }
    public void goToTakePartUpload( int ChallengeID , int ContentID , int type )
    {
        Intent intent = new Intent(ChallegeDetailActivity.this, TakePartUploadActivity.class);
        intent.putExtra("ChallengeID", ChallengeID );
        intent.putExtra("ContentID", ContentID );
        intent.putExtra("ChallengeTypeID", ChallengeTypeID );
        intent.putExtra("txtTitle", txtTitle.getText() );
        intent.putExtra("txtPoints", txtPoints.getText() );
        intent.putExtra("type", type );
        startActivity(intent);
    }

}
