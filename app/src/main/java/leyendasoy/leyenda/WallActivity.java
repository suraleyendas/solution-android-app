package leyendasoy.leyenda;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.NotificationAdapter;
import adapters.WallAdapter;
import adparter_class.Comment;
import adparter_class.Notification;
import adparter_class.Wall;
import clases.DbHelper;
import clases.MiHelper;
import clases.PrefManager;
import clases.Singleton;

public class WallActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private RecyclerView recycler;
    List<Wall> items;
    ProgressDialog progressDialog = null;
    Context miContex;
    WallActivity ref;
    LinearLayout tab_inicio , tab_leyenda , tab_retos ,  tab_premios;

    LinearLayout layoutPhoto ,  layoutVideo , layoutExpress , layoutEvent ;
    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    WallAdapter listAdapter;

    int _iduser;

    ImageView photoProfile;
    String _nombre;
    Menu menu = null;


    private NotificationManager notifyMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationIcon(R.drawable.ic_menu_blue);

        miContex = this;
        ref = this;

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        tab_premios = (LinearLayout) findViewById(R.id.tab_premios);
        tab_premios.setOnClickListener( this );

        photoProfile = ( ImageView ) findViewById(R.id.photoProfile);



        if( menu != null )
        {
            menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.ic_menu_blue));
        }
        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );

            _nombre = Singleton.getInstance().getName();
            _iduser = Singleton.getInstance().getUserID();

            Singleton.getInstance().setContext( this );
            NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            Singleton.getInstance().setNotificationManager( nm );
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        GetUser();
        GetNotificationByUserId();

        layoutPhoto = (LinearLayout) findViewById(R.id.layoutPhoto);
        layoutPhoto.setOnClickListener(this);

        layoutVideo = (LinearLayout) findViewById(R.id.layoutVideo);
        layoutVideo.setOnClickListener(this);

        layoutEvent = (LinearLayout) findViewById(R.id.layoutEvent);
        layoutEvent.setOnClickListener(this);

        layoutExpress = (LinearLayout) findViewById(R.id.layoutExpress);
        layoutExpress.setOnClickListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        GetWallByUser( page , false );

        TextView txtNombre = ( TextView ) findViewById(R.id.txtNombre);
        txtNombre.setText(_nombre);

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(miContex);
        recycler.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    GetWallByUser( page , false );

            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                GetWallByUser( page , true );
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.navigation, menu);
        this.menu = menu;
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        if( item.getTitle() == null )
        {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(Gravity.LEFT);
        }
        else
        {
            switch (item.getItemId())
            {
                case R.id.user:
                    intent = new Intent(WallActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);

                    startActivity(intent);
                    break;

                case R.id.message:
                    intent = new Intent(WallActivity.this, MessageActivity.class);
                    startActivity(intent);
                    break;

                case R.id.notification:
                    intent = new Intent(WallActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    break;

            }
        }
        return (super.onOptionsItemSelected(item));
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.content_saved)
        {
            Intent intent = new Intent(WallActivity.this, ContentSavedActivity.class);

            intent.putExtra("userId", _iduser );
            startActivity(intent);
        }
        else if (id == R.id.liked_content)
        {
            Intent intent = new Intent(WallActivity.this, ContentLikeActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.termandcondition)
        {
            Intent intent = new Intent(WallActivity.this, TermAndConditionActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.faq)
        {
            Intent intent = new Intent(WallActivity.this, FaqActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.profile)
        {
            Intent intent = new Intent(WallActivity.this, ProfileActivity.class);
            intent.putExtra("showButtons", false);
            intent.putExtra("isFollow", false);
            startActivity(intent);
        }
        else if ( id == R.id.close_session )
        {
            DbHelper dbHelper = new DbHelper(this);
            dbHelper.deleteUsers();
            getApplicationContext().deleteDatabase( dbHelper.DATABASE_NAME );
            PrefManager prefManager = new PrefManager(this);
            prefManager.setFirstTimeLaunch(true);

            Intent intent = new Intent(WallActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
    @Override
    public void onClick(View view)
    {
        if( view.getId() == R.id.layoutExpress )
        {
            Intent intent = new Intent(WallActivity.this, ExpressYourselfActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutPhoto )
        {
            Intent intent = new Intent(WallActivity.this, MediaWallActivity.class);
            intent.putExtra("type", "1" );
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutVideo )
        {
            Intent intent = new Intent(WallActivity.this, MediaWallActivity.class);
            intent.putExtra("type", "2" );
            startActivity(intent);
        }
        else if( view.getId() == R.id.layoutEvent )
        {
            Intent intent = new Intent(WallActivity.this, EventActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_inicio )
        {

            Intent intent = new Intent(WallActivity.this, WallActivity.class);

            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(WallActivity.this, LegendActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(WallActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
        else if( view.getId() == R.id.tab_premios )
        {
            Intent intent = new Intent(WallActivity.this, AwardsActivity.class);
            startActivity(intent);
        }
    }
    public void GetWallByUser( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(WallActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("TopComment", "2");
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", miHelper.paginationWall );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetWallByUser"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }

                        JSONArray contents = response.getJSONArray("arrayContent");
                        for (int i = 0; i < contents.length(); i++)
                        {
                            List<Comment> commentList = new ArrayList<>();
                            JSONObject content = contents.getJSONObject(i);

                            JSONArray ListComment = content.getJSONArray("ListComment");
                            if( ListComment.length() > 0 )
                            {
                                for (int j = 0 ; j < ListComment.length() ; j++ )
                                {
                                    JSONObject comment = ListComment.getJSONObject(j);
                                    Comment c = new Comment( comment.getString("Picture") , comment.getString("FullName") , comment.getString("StrComment") );
                                    commentList.add(c);
                                }
                            }
                            items.add(new Wall( content.getString("Name").trim() , content.getString("ContentDescription").trim() , content.getString("Picture") , content.getString("ContentId").trim() , content.getString("UserId").trim() , content.getString("FilePath") , content.getString("strContent").trim() , content.getBoolean("IsActive") , content.getInt("ContentTypeId") , content.getString("ContentType").trim() ,  content.getBoolean("IsLike") , content.getInt("CountLike") , content.getInt("CountComment") , commentList , content.getInt("Event_Type") , content.getBoolean("IsChallenge") , content.getString("ChallengeTitle").trim() , content.getString("ChallengeDescription").trim() , content.getInt("ChallengePoint") , content.getBoolean("IsParticipated") , content.getString("LimitDateChallenge").trim() , content.getInt("ChallengeTypeId") ,  content.getInt("ChallengeId")  ) );
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new WallAdapter(items, miContex ,  1);
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Wall> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }
                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetWallByUser( page , false );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
    public void GetUser()
    {
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(WallActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("Width", "50");

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");
                    if (result == true)
                    {
                        byte[] decodedString = Base64.decode(response.getString("Picture"), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        photoProfile.setImageBitmap(decodedByte);
                        Singleton.getInstance().InitChat( _iduser , response.getString("Picture") , _nombre );
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder( miContex );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetUser();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
    public void GetNotificationByUserId()
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(WallActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("PageIndex", "0" );
        params.put("PageSize", "10" );
        JSONObject jsonObj = new JSONObject(params);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "NotificationAPI/GetNotificationByUserId"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    Log.i("Notificacion " , " " + result.toString() );
                    if (result == true)
                    {
                        JSONArray contents = response.getJSONArray("ArrayNotificationByuser");
                        if( contents.length() > 0 )
                        {
                            MenuItem item = menu.findItem(R.id.notification);
                            if (item != null)
                            {
                                item.setIcon(R.drawable.ic_bell_menu_active);
                            }
                        }
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContex);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetNotificationByUserId(  );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }


}
