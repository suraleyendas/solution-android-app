package leyendasoy.leyenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.WallAdapter;
import adparter_class.Comment;
import adparter_class.Wall;
import clases.ModalVote;
import clases.MiHelper;
import clases.Singleton;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private RecyclerView recycler;
    List<Wall> items;
    ProgressDialog progressDialog = null;

    Button btnEditProfile ,  btn_follow ,  btn_vote;
    String  urlFoto;
    int _iduser , userIdFollow = 0;
    boolean isFollow , showButtons , isVoted = false;

    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount , page = 0;
    SwipeRefreshLayout swipeRefreshLayout;
    WallAdapter listAdapter;


    Context miContext;

    TextView textNombre , textCorreo , textEmpresa;
    TextView txtPoint , txtFollows , txtFollowers , txtVotes ;

    LinearLayout tab_inicio , tab_retos , tab_leyenda , tab_premios, linearComment;
    boolean statusBtnEdit  = false;
    TextView txtComments;
    Boolean profile_guess = false;
    ImageView photoProfile;

    ProfileActivity ref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }
    public void init()
    {
        txtPoint = (TextView) findViewById(R.id.txtPoint);
        txtFollows = (TextView) findViewById(R.id.txtFollows);
        txtFollowers = (TextView) findViewById(R.id.txtFollowers);
        txtVotes = (TextView) findViewById(R.id.txtVotes);

        ref = this;

        txtComments = ( TextView ) findViewById(R.id.txtComments);
        photoProfile = ( ImageView ) findViewById(R.id.photoProfile);

        linearComment = ( LinearLayout ) findViewById(R.id.linearComment);
        linearComment.setOnClickListener(this);

        miContext = this;

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        textNombre = ( TextView ) findViewById(R.id.txtNombre );
        textCorreo = ( TextView ) findViewById(R.id.txtCorreo );
        textEmpresa = ( TextView ) findViewById(R.id.txtEmpresa );


        btnEditProfile = ( Button ) findViewById(R.id.btnEditProfile);
        btn_follow = ( Button ) findViewById(R.id.btn_follow);
        btn_vote = ( Button ) findViewById(R.id.btn_vote);

        btnEditProfile.setOnClickListener(this);
        btn_follow.setOnClickListener(this);
        btn_vote.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        try
        {
            Singleton.getInstance().setContextDB( getApplicationContext() );
            _iduser = Singleton.getInstance().getUserID();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        if (extras != null)
        {
            if (extras.containsKey("userIdFollow"))
            {
                profile_guess = true;
                userIdFollow = extras.getInt("userIdFollow");
                GetProfile( userIdFollow );
                GetProfileInfoByUserId( userIdFollow );
            }
            else
            {
                profile_guess = false;
                GetProfile( _iduser );
                GetProfileInfoByUserId( _iduser );
            }
        }

        tab_inicio = (LinearLayout) findViewById(R.id.tab_inicio);
        tab_inicio.setOnClickListener( this );

        tab_leyenda = (LinearLayout) findViewById(R.id.tab_leyenda);
        tab_leyenda.setOnClickListener( this );

        tab_retos = (LinearLayout) findViewById(R.id.tab_retos);
        tab_retos.setOnClickListener( this );

        tab_premios = (LinearLayout) findViewById(R.id.tab_premios);
        tab_premios.setOnClickListener( this );

        showButtons = (boolean) extras.get("showButtons");
        isFollow = (boolean) extras.get("isFollow");

        LinearLayout linear3 = ( LinearLayout ) findViewById(R.id.linear3);
        btn_follow = (Button) findViewById(R.id.btn_follow);

        if( showButtons == true )
        {
            UserIsVoted( _iduser , userIdFollow );

            btnEditProfile.setVisibility(View.GONE);
            linear3.setVisibility(View.VISIBLE);
            if( isFollow )
            {
                btn_follow.setBackgroundResource(R.drawable.btn_following);
                btn_follow.setText("Siguiendo");
                btn_follow.setPadding(20,0,30,0);
                btn_follow.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done_white_24dp, 0);
                btn_follow.setTextColor(Color.WHITE);

            }
            else
            {
                btn_follow.setBackgroundResource(R.drawable.btn_follow);
                btn_follow.setText("Seguir");
                btn_follow.setPadding(0,0,0,0);
                btn_follow.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_follow.setTextColor( Color.parseColor("#6CA6B2") );
            }
        }
        else
        {
            btnEditProfile.setVisibility(View.VISIBLE);
            linear3.setVisibility(View.GONE);
        }
        LinearLayout layoutSeguidos = (LinearLayout) findViewById(R.id.layoutSeguidos);
        layoutSeguidos.setOnClickListener(this);
        LinearLayout layoutSeguidores = (LinearLayout) findViewById(R.id.layoutSeguidores);
        layoutSeguidores.setOnClickListener(this);
        LinearLayout layoutVotos = (LinearLayout) findViewById(R.id.layoutVotos);
        layoutVotos.setOnClickListener(this);
        LinearLayout layoutPuntos = (LinearLayout) findViewById(R.id.layoutPuntos);
        layoutPuntos.setOnClickListener(this);

        recycler = (RecyclerView) findViewById(R.id.recycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);

        GetMyPublish( page , false );

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.loading);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                listAdapter.clear();
                page = 0;
                if( loading == false )
                    GetMyPublish( page , false );

            }
        });

        recycler.addOnScrollListener( new RecyclerView.OnScrollListener()
        {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if ( dy > 0 )
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if ( loading == false )
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++;
                            swipeRefreshLayout.setRefreshing(true);
                            if( loading == false )
                                GetMyPublish( page , true );
                        }
                    }
                }
            }
        });


    }
    @Override
    public void onResume()
    {
        super.onResume();
        init();
    }

    public void GetProfileInfoByUserId( final int user )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "UsersAPI/GetProfileInfoByUserId/"+ user, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {

                        txtPoint.setText( "" + response.getInt("Points") );
                        txtFollows.setText( "" + ( response.getInt("Follows") + 1 ) );
                        txtFollowers.setText( "" +response.getInt("Followers") );
                        txtVotes.setText( "" + response.getInt("Votes") );
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetProfileInfoByUserId( user );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void YaHasVotado( boolean status )
    {
        if( status == true )
        {
            isVoted = true;
            Toast.makeText(this, "Voto guardado exitosamente", Toast.LENGTH_SHORT).show();

            btn_vote.setText("Ya has votado");
            btn_vote.setTag(R.id.tag_vote, "yes");
            btn_vote.setBackgroundResource(R.drawable.btn_voted);
            btn_vote.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done_white_24dp, 0);
            btn_vote.setTextColor(Color.WHITE);
        }
    }
    public  void SaveUserFollow(final String userIdFollow , final boolean follow )
    {
        final MiHelper miHelper = new MiHelper();
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser));
        params.put("UserFollowId", userIdFollow );
        params.put("IsFollow", String.valueOf(follow) );




        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/SaveUserFollow", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("result");

                    if (result == true)
                    {
                        if( follow == false )
                        {
                            btn_follow.setBackgroundResource(R.drawable.btn_follow);
                            btn_follow.setText("Seguir");
                            btn_follow.setPadding(0,0,0,0);
                            btn_follow.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            btn_follow.setTextColor( Color.parseColor("#6CA6B2") );
                        }
                        else
                        {
                            btn_follow.setBackgroundResource(R.drawable.btn_following);
                            btn_follow.setText("Siguiendo");
                            btn_follow.setPadding(20, 0, 30, 0);
                            btn_follow.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done_white_24dp, 0);
                            btn_follow.setTextColor(Color.WHITE);
                        }
                    }


                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                SaveUserFollow(  userIdFollow ,  follow );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public  void UserIsVoted( final int userId , final int userIdFollow  )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(userId) );
        params.put("UserVotedId", String.valueOf(userIdFollow) );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/UserIsVoted", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Log.i("User is voted:" + userIdFollow , response.toString());
                    Boolean result = response.getBoolean("Result");

                    if (result == true)
                    {
                        isVoted = true;
                        btn_vote.setText("Ya has votado");
                        btn_vote.setBackgroundResource(R.drawable.btn_voted);
                        btn_vote.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done_white_24dp, 0);
                        btn_vote.setTextColor(Color.WHITE);
                        btn_vote.setTag(R.id.tag_vote , "yes");
                    }
                    else
                    {
                        btn_vote.setTag(R.id.tag_vote , "no");
                    }

                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                UserIsVoted(  userId , userIdFollow  );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if( item.getTitle() == null )
        {
            onBackPressed();
        }
        else
        {
           switch (item.getItemId())
           {
                case R.id.user:

                    Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                    intent.putExtra("showButtons", false);
                    intent.putExtra("isFollow", false);
                    startActivity(intent);
                break;
               case R.id.message:
                   intent = new Intent(ProfileActivity.this, MessageActivity.class);
                   startActivity(intent);
                   break;
               case R.id.notification:
                   intent = new Intent(ProfileActivity.this, NotificationActivity.class);
                   startActivity(intent);
                   break;

            }
        }
        return(super.onOptionsItemSelected(item));
    }
    public  void SaveUserVote(int userId, int userIdVote, String description, final boolean isActive )
    {
        final MiHelper miHelper = new MiHelper();
        progressDialog = new ProgressDialog( this );
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue( miContext );
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(userId) );
        params.put("UserIdVote", String.valueOf(userIdVote) );
        params.put("Description", ""+description );
        params.put("IsActive", String.valueOf(isActive) );

        Log.i("Params" , params.toString() );


        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/SaveUserVote", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();

                    Boolean result = response.getBoolean("result");
                    progressDialog.dismiss();

                    if (result == true)
                    {
                        progressDialog.dismiss();

                        if( isActive == true )
                        {
                            isVoted = true;
                        }
                        else
                        {
                            isVoted = false;
                            Toast.makeText( miContext, "Tu voto ha sido retirado.", Toast.LENGTH_SHORT).show();
                            btn_vote.setBackgroundResource(R.drawable.btn_vote);
                            btn_vote.setText("Vota por Leyenda");
                            btn_vote.setPadding(0,0,0,0);
                            btn_vote.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            btn_vote.setTextColor( Color.parseColor("#256289") );
                        }



                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder( miContext );
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //SaveUserFollow(  userIdFollow ,  isFollow );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        queue.add(getRequest);
    }
    @Override
    public void onClick(View v)
    {
        if( v.getId() == R.id.btnEditProfile )
        {
            Intent intent = new Intent(ProfileActivity.this, EditprofileActivity.class);
            intent.putExtra("textoBtn", "Actualizar" );
            intent.putExtra("role", "role" );
            startActivity(intent);
        }

        else if( v.getId() == R.id.layoutSeguidos )
        {
            Intent intent = new Intent(ProfileActivity.this, FollowedActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.layoutSeguidores )
        {
            Intent intent = new Intent(ProfileActivity.this, FollowersActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.layoutVotos )
        {
            Intent intent = new Intent(ProfileActivity.this, VotersByUserActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.layoutPuntos )
        {
            if( profile_guess == false )
            {
                Intent intent = new Intent(ProfileActivity.this, AwardsActivity.class);
                startActivity(intent);
            }
        }
        else if( v.getId() == R.id.btn_follow )
        {
            if( showButtons == true )
            {
                    if( userIdFollow > 0 )
                    {
                        if( isFollow == true )
                        {
                            SaveUserFollow(String.valueOf(this.userIdFollow), false);
                            isFollow = false;
                        }
                        else
                        {
                            SaveUserFollow(String.valueOf(this.userIdFollow), true);
                            isFollow = true;
                        }
                    }
            }
        }
        else if( v.getId() == R.id.btn_vote )
        {

            if( isVoted == true )
                SaveUserVote( _iduser , userIdFollow , "" , false );
            else
                GetAvailableUserVote();

        }
        else if( v.getId() == R.id.tab_inicio )
        {
            Intent intent = new Intent(ProfileActivity.this, WallActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_leyenda )
        {
            Intent intent = new Intent(ProfileActivity.this, LegendActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_retos )
        {
            Intent intent = new Intent(ProfileActivity.this, ChallengeActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.tab_premios )
        {
            Intent intent = new Intent(ProfileActivity.this, AwardsActivity.class);
            startActivity(intent);
        }
        else if( v.getId() == R.id.linearComment )
        {
            if( userIdFollow == 0 )
            {
                Intent intent = new Intent(ProfileActivity.this, SummaryActivity.class);
                intent.putExtra("summary", txtComments.getText());
                startActivity(intent);
            }
        }
    }
    public void GetProfile( final int iduser )
    {
        final MiHelper miHelper = new MiHelper();
        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(miHelper.loadinMsg);
            progressDialog.show();
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(iduser) );
        params.put("Width", "800" );
        JSONObject jsonObj = new JSONObject(params);


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "UsersAPI/GetUser/", jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Log.i("Profile " , response.toString() );
                    boolean result = (boolean) response.getBoolean("Result");
                    if( result == true )
                    {

                        txtComments.setText( response.getString("Summary") );
                        byte[] decodedString = Base64.decode(response.getString("Picture"), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        photoProfile.setImageBitmap(decodedByte);

                        textNombre.setText( response.getString("Name") );
                        textCorreo.setText( response.getString("Email") );
                        textEmpresa.setText( response.getString("Company") + " - " + response.getString("City") );

                        if( profile_guess == false )
                        {
                            Singleton.getInstance().setUserID(response.getInt("UserId"));
                            Singleton.getInstance().setName(response.getString("Name"));
                            Singleton.getInstance().setPhoto(response.getString("Picture"));
                            Singleton.getInstance().setEmail(response.getString("Email"));
                            Singleton.getInstance().setCity(response.getString("City"));
                            Singleton.getInstance().setCompany(response.getString("Company"));
                            Singleton.getInstance().setCityID(response.getInt("CityId"));
                            Singleton.getInstance().setCompanyID(response.getInt("CompanyId"));
                        }

                    }


                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetProfile( iduser );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetAvailableUserVote()
    {
        final MiHelper miHelper = new MiHelper();
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(miHelper.loadinMsg);
        progressDialog.show();

        final RequestQueue queue = Volley.newRequestQueue(this);



        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, miHelper.getURL() + "UsersAPI/GetAvailableUserVote/" + _iduser , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    Log.i("GetAvailableUserVote" ,  response.toString() );


                    if( result )
                    {
                        if( isVoted == false )
                        {
                            ModalVote.newInstance( ref, textNombre.getText().toString() , textCorreo.getText().toString() , textEmpresa.getText().toString() , urlFoto , _iduser , userIdFollow , true ).show(getFragmentManager(), null);
                        }
                        else
                        {
                            SaveUserVote( _iduser , userIdFollow , "" , false );
                        }
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Advertencia");
                        builder.setMessage("No es posible votar por cuarta vez.").setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                            }
                        }).show();
                    }
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetAvailableUserVote();
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };

        queue.add(getRequest);
    }
    public void GetMyPublish( final int page , final  Boolean add )
    {
        loading = true;
        final MiHelper miHelper = new MiHelper();

        if( progressDialog == null )
        {
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage( miHelper.loadinMsg );
            progressDialog.show();
        }
        final RequestQueue queue = Volley.newRequestQueue(this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", String.valueOf(_iduser) );
        params.put("TopComment", "2");
        params.put("UserContentTypeId", "0");
        params.put("PageIndex", String.valueOf(page) );
        params.put("PageSize", miHelper.paginationWall );

        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, miHelper.getURL() + "ContentAPI/GetMyPublish"  , jsonObj, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    loading = false;
                    progressDialog.dismiss();
                    Boolean result = response.getBoolean("Result");

                    Log.i("Wall" , response.toString()  );

                    if (result == true)
                    {
                        if( listAdapter == null || add == false )
                        {
                            items = new ArrayList<>();
                        }
                        JSONArray contents = response.getJSONArray("arrayContent");
                        for (int i = 0; i < contents.length(); i++)
                        {
                            List<Comment> commentList = new ArrayList<>();
                            JSONObject content = contents.getJSONObject(i);

                            JSONArray ListComment = content.getJSONArray("ListComment");
                            if( ListComment.length() > 0 )
                            {
                                for (int j = 0 ; j < ListComment.length() ; j++ )
                                {
                                    JSONObject comment = ListComment.getJSONObject(j);
                                    Comment c = new Comment( comment.getString("Picture") , comment.getString("FullName") , comment.getString("StrComment") );
                                    commentList.add(c);
                                }
                            }
                            items.add(new Wall( content.getString("Name").trim() , content.getString("ContentDescription").trim() , content.getString("Picture") , content.getString("ContentId").trim() , content.getString("UserId").trim() , content.getString("FilePath") , content.getString("strContent").trim() , content.getBoolean("IsActive") , content.getInt("ContentTypeId") , content.getString("ContentType").trim() ,  content.getBoolean("IsLike") , content.getInt("CountLike") , content.getInt("CountComment") , commentList , content.getInt("Event_Type") , content.getBoolean("IsChallenge") , content.getString("ChallengeTitle").trim() , content.getString("ChallengeDescription").trim() , content.getInt("ChallengePoint") , content.getBoolean("IsParticipated") , content.getString("LimitDateChallenge").trim() , content.getInt("ChallengeTypeId") ,  content.getInt("ChallengeId")  ) );
                        }

                        if( listAdapter == null )
                        {
                            listAdapter = new WallAdapter(items, miContext ,  1);
                            recycler.setAdapter(listAdapter);
                        }
                        else if( add == false )
                        {
                            listAdapter.clear();
                            listAdapter.addAll(items);
                        }
                        else if( add == true )
                        {
                            List<Wall> newList = new ArrayList<>();
                            for ( int i = 0 ; i < items.size() ; i++ )
                                newList.add(  items.get(i) );
                            listAdapter.clear();
                            listAdapter.addAll( newList );
                        }
                    }
                    else
                    {
                        //Toast.makeText(miContex, miHelper.WithOutResult, Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
                catch (Exception e)
                {
                    swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                if (error.networkResponse == null)
                {
                    if (error.getClass().equals(TimeoutError.class))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(miContext);
                        builder.setTitle("Error en la conexión");
                        builder.setMessage("Vuelva a intentarlo.").setNegativeButton("Reintentar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                GetMyPublish( page , false );
                            }
                        }).show();
                    }
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", miHelper.getUsername(), miHelper.getPassword()).getBytes(), Base64.DEFAULT)));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);

    }
}
